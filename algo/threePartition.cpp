#include <stdio.h>

/*
	3 partition problem:
	put every number into one of 3 buckets and find a combination
	for which the sum of numbers in each of the 3 buckets is equal.
	(either tell the combination or tell if there is one)

	example:
	int vals[] = {3,2,1,3};
	bucket-assignment 0,1,1,2 means
	bucket 0 : 3
	bucket 1 : 2+1 = 3
	bucket 2 : 3

	this would be a valid solution.
*/
int main()
{
	//int vals[] = {3,2,1,3};	// viable test
	int vals[] = {6, 27, 42, 42, 7, 28, 18, 24, 33, 38, 33, 19, 26, 9, 24, 20}; // finishes fast (found solution)
	//int vals[] = {6, 27, 42, 42, 7, 28, 18, 24, 33, 38, 33, 19, 26, 9, 24, 20, 47, 4, 6}; // takes forever
	int valsLen = sizeof vals/sizeof vals[0];
	int number=0;
	int placesCounter=0;	// number of dec-places of current number

	while(placesCounter<=valsLen) {

		int buckets[3] = {0};

		int q=number++;
		placesCounter=0;
		// convert number to base-3 (place for place)
		do{
			int r = q%3;
			q = q/3;
			if(placesCounter<valsLen) {
				buckets[r] += vals[placesCounter];
			}
			placesCounter++;
		}while(q!=0);
		bool solution=buckets[0]==buckets[1]&&buckets[1]==buckets[2]?1:0;
		bool valid=placesCounter==valsLen;
		//printf("sum1=%d sum2=%d sum3=%d valid=%d solution=%d\n", buckets[0], buckets[1], buckets[2], valid, solution);
		if(valid && solution) {
			printf("ok");
			return 1;
		}
	}

	return 0;
}