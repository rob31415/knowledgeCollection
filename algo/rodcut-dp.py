#!/usr/bin/env python

#            k
# best(k) = max { p(k) + best(k-n) }
#           n=1


# this is the width of a slab
# 0 1 2 3 ... n

# best means "highest value"
# best of 0 is 0 ... best of 3 cuts = max( value of cutting width 1 + best way of cutting 2, cutting width 2 + best of 1 or 3 + 0 )
# b[0]=0   b[1]=v[1]+b[0]   b[2]=max(v[1]+b[1], v[2]+b[0])   b[3]=max(v[1]+b[2], v[2]+b[1], v[3]+b[0]) ... n


# value of width 1 slab = 4; value of width 2 = 2; etc.
v = [0,4,2,5,2,5,2,1,1,4]

# best
b={}
b[0]=v[0]

# what was chosen
c={}

# bottom up; iteratively

for k in range(1,len(v)):
    for n in range(1,k+1):
        value = v[n]+b[k-n]
        if len(b)<k+1 or value > b[k]:
            c[k] = n
            b[k] = value

for i in c:
    print(c[i])

print(b[len(v)-1])
