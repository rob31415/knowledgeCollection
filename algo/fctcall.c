#include <stdlib.h>     // size_t
#include <inttypes.h>
#include <stdio.h>


void myNaiveImplOfAFctCall()
{
    // data needed by caller and callee
    int stack[5];
    unsigned stackIdx=0;

    // callee (i.e. a function)

    // goto jumpOverMyFunc;
    {
        //printf("%p", &&jumpOverMyFunc);
        void *ptr = &&jumpOverMyFunc;   // gcc specific: get addr of label
        goto *ptr;  // go there
    }
    myFunc:
    {
        int sum = 0;
        while(stackIdx>0) {
            sum += stack[--stackIdx];
        }
        printf("%d\n", sum);
        goto retAddr;
    }
    jumpOverMyFunc:


    stack[stackIdx++] = 1;
    stack[stackIdx++] = 2;
    stack[stackIdx++] = 3;

    goto myFunc;
    retAddr:    // in assembler, the ret addr would go on the stack

    printf("fin\n");
}



unsigned someTailRecursiveFct(unsigned n)
{
    if(n>0) return n+someTailRecursiveFct(n-1);
}


void myNaiveImplOfATailRecursiveFctCall()
{
    int stack[5];
    unsigned stackIdx=0;

    stack[stackIdx++] = 3;

    goto myFunc;
    retAddr:
    printf("fin\n");

    goto jumpOverMyFunc;
    myFunc:
    {
        int a = stack[--stackIdx];
        if(a==0) {
            int sum = 0;
            while(stackIdx>0) {
                sum += stack[--stackIdx];
            }
            printf("%d\n", sum);
            goto retAddr;
        } else {
            stack[stackIdx++] = a;
            int b=a-1;
            stack[stackIdx++] = b;
            goto myFunc;
        }
    }
    jumpOverMyFunc:

    end:
    return;
}


// puts return address on stack and jumps there (i.e. can be called from different locations)
void myImplOfATailRecursiveFctCallGeneric()
{
    // data needed by caller and callee
    long stack[6];
    unsigned stackIdx=0;


    // callee (i.e. a function)
    goto jumpOverMyFunc;
    myFunc:
    {
        int a = stack[--stackIdx];
        if(a==0) {
            int sum = 0;
            while(stackIdx>1) {
                sum += stack[--stackIdx];
            }
            printf("%d\n", sum);
            void* ptr=(void*)stack[--stackIdx];
            goto *ptr;
        } else {
            stack[stackIdx++] = a;
            int b=a-1;
            stack[stackIdx++] = b;
            goto myFunc;
        }
    }
    jumpOverMyFunc:


    // call the function
    stack[stackIdx++] = (long)&&retAddr;
    stack[stackIdx++] = 3;
    goto myFunc;
    retAddr:

    // call the function once more from a different location
    stack[stackIdx++] = (long)&&retAddr2;
    stack[stackIdx++] = 4;
    goto myFunc;
    retAddr2:

    printf("fin\n");
}


void myImplOfATailRecursiveFctCallGenericWithRetval()
{
    // TODO
}


int main()
{
    myNaiveImplOfAFctCall();
    printf("%d\nfin\n", someTailRecursiveFct(3));
    myNaiveImplOfATailRecursiveFctCall();
    myImplOfATailRecursiveFctCallGeneric();
    return 0;
}
