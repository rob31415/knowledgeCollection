#!/usr/bin/env python

# property of optimal substructure (principle of optimality)

# fibonacci naive; iterative; top-down; runtime O(n^2)

k0=0
k1=1

print(k0)
print(k1)

for i in range(2,10):
    k=k0+k1
    k0=k1
    k1=k
    print(k)

print("")

# fibo memoized; recursive; bottom-up; runtime O(n) (disregarding access time of python dict)

m={}
m[0]=0
m[1]=1

def fib(n):
    global m
    if len(m)<n+1:
        m[n]=fib(n-1)+fib(n-2)
        print(m[n])
    return m[n]

print(m[0])
print(m[1])
fib(9)
print("")

# fibo iteratively; bottom-up

m={}
m[0]=0
m[1]=1

for i in range(2,10):
    m[i]=m[i-1]+m[i-2]

print(m[9])
