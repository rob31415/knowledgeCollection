/*
                     20
            10                45
        40       5        24       18
      12  19   33 25    27  30  15   23
*/
int h[] = {20, 10, 45, 40, 5, 24, 18, 12, 19, 33, 25, 27, 30, 15, 23};
int h_count = 15;   // note this starts at 1


void reorg(int* a, int parent_idx, int count)
{
    int left_child_idx = parent_idx*2;
    int right_child_idx = (parent_idx*2)+1;
    // TODO: compare and swap
    // also: call reorg(a, biggest_child_idx, count), because modifying a triad can disturb lower triads:
    // e.g.: 10,40,5 becomes 40,10,5 and then 40,12,19 becomes 10,12,19 and must again be reorganized.
}

void max_heapify(int* a, int count)
{
    int last_parent_idx = count/2;  // starting with "18"

    // left of count/2 are all parents. right of count/2 are all leaves.
    // begin with last parent (18) then go backwards: 25, 5, 40, 45, 10 and 20.
    // for all those triads, make parent the biggest (compare and swap).
    for(int current_parent_idx=last_parent_idx; i>0; i--) {
        reorg(a, current_parent_idx, count);
    }
}

int main() {
    max_heapify(&h[0], h_count);
    return 0;
}
