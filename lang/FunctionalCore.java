/*

C function pointer

    typedef void (*myFPtr)(int);
    myFPtr fp = &someExistingFunction;

    It's a pointer to an instruction (not to data).

java method reference (function reference) vs lambda

    String::startsWith
    (a, b) -> a.startsWith(b)

    non-capturing (the lambda doesn’t access any variables defined outside its body) 
    capturing (the lambda accesses variables defined outside its body)
    https://www.infoq.com/articles/Invokedynamic-Javas-secret-weapon/

*/

interface FunctionalInterface<T, U> {
    Boolean test(T a, U b);
}

public class FunctionalCore {
    public static void main() {
        // error: cannot infer type for local variable noGood
        //var noGood = (String a, String b) -> {return a.startsWith(b);};
        FunctionalInterface<String,String> functionObject = String::startsWith;
        functionObject.test("Hi","There");
    }
}

/*

    Ok, so functionObject is like a "proxy" or a "wrapper" or a "pointer" or sth. like that.
    It represents something you can call.
    
    BTW: A constraint is, that it's a type which is a "functional interface".
    And what makes it functional is that it respects the SAM-rule (single abstract method).
    If you put @FunctionalInterface on your interface, the compiler complains if it doesn't.

    When calling the functionObject, the concrete implementation is called which is assigned to it.
    If that happens to be a lambda, it's trivial to see how it works.

    If it's a method reference it's not trivial because one needs to understand how a method reference
    is being turned into a lamba.

    In this example, this is what we're finally calling:
    public boolean startsWith(String prefix); (of a String object.)
    But we do NOT call via a String-object directly.
    We say "String::startsWith", not "someStringObject::startsWith".
    The latter we could call "Object-Instance Method Reference".
    The first we could call "Parameter Method Reference".

    For a "Parameter Method Reference",
    "ClassName::instanceMethodName" becomes "(ClassName e, Object o) -> e.instanceMethodName(o)".
    The compiler handles the parameters automatically.
    Note: this is a BiConsumer.

    So, "String::startsWith" in this example means "The method startsWith of some object of type String".

    There are 4 types of method references. 2 are described above.
    There's also "ClassName::new" to reference a constructor 
    and "ContainingClass::staticMethodName" refering to a static method.

*/


/*
a method reference can resolve into any functional interface.
it depends on the context, which IF that happens to be.
e.g.

String::isEmpty  can resolve into
    Predicate<String>
    Function<String, Boolean>
String::startsWith  can resolve into
    BiConsumer
    because it becomes (ClassName e, Object o) -> e.instanceMethodName(o)
Class X { void bla() {} }       X::bla
    Consumer
    (ClassName e) -> e.instanceMethodName()

remember, 1st arg is classname; so, fct() is Consumer; fct(someParam) is BiConsumer
*/

