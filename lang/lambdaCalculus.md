  λx.             a function taking x as argument (w/o definition yet)
  λx. x+1         definition: the function returns x+1

  (λx. x+1)   5   applying the function to the number 5
  5+1             substitution...
  6

  encoding true and false:

  T=λx.λy.x   chooses the first arg
  F=λx.λy.y   chooses the second arg

  NOT=λb.b F T
  https://www.youtube.com/watch?v=eis11j_iGMs

church touring hypothesis: any sequential program can be encoded in lambda calculus. same for touring machines.
so, these 2 systems are formally equivalent and can be transformed into one another.
