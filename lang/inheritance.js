// "concatenation" merges a block with another block.
// this is inheritance (or more specific, mixin)
// done on object level rather than class level.

const stackable = base => {
    const items = []    // fully encapsulated (i.e. private)
    return Object.assign(base, {
        depth: () => items.length,
        top: () => items[items.length-1],
        pop: () => {items.pop()},
        push: newTop => {items.push(newTop)},
    })
}

const aStack = () => stackable({})

const clearable = base => {
    return Object.assign(base, {
        clear: () => {
            while (base.depth())
                base.pop()
        },
    })
}

const anotherStack = () => clearable(stackable({}))
