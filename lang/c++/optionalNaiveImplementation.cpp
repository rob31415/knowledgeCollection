#include <stdio.h>

template <typename TypeForValue, typename TypeForReturncode>
class Optional
{
private:
    TypeForValue value;
    bool hasValue;
    TypeForReturncode returnCode;

public:
    Optional() : value(), hasValue(false), returnCode(TypeForReturncode()) {}
    Optional(TypeForValue value) : value(value), hasValue(true), returnCode(TypeForReturncode()) {}
    Optional(TypeForReturncode returnCode) : value(), hasValue(false), returnCode(returnCode) {}
    Optional(TypeForValue value, TypeForReturncode returnCode) : value(value), hasValue(true), returnCode(returnCode) {}

    operator bool() {return hasValue;}
    TypeForValue operator*() {if(hasValue) return value; else return TypeForValue();}
    TypeForReturncode operator()() {return returnCode;}

};

// assign and test. (if you're fond of creating a syntax of your own.)
#define when(f,x) {auto _value = f; if(_value) x;}

// you can't use native datatypes for this because it potentially interferes with constructors for those types
//typedef unsinged int ReturnCode;
enum class ReturnCode {undefined, good, bad, somethingHappened};    // but this works

// works, because explicit is missing in TypeForValue Constructor of class Optional
Optional<int, ReturnCode> bla1() {return 10;}

// use this to say "function doesn't return a value"
Optional<int, ReturnCode> bla2() {return Optional<int, ReturnCode>();}

// works with more complex data-types as long as they have a standard constructor
struct SomeData {int a; double b;};
Optional<SomeData, ReturnCode> bla3() {return Optional<SomeData, ReturnCode>();}

// works, because explicit is missing in ReturnCode Constructor of class Optional
Optional<double, ReturnCode> bla4() {return ReturnCode::bad;}


int main()
{
    // either do assignment and test in two steps

    auto b1 = bla1();
    auto b2 = bla2();
    auto b3 = bla3();
    auto b4 = bla4();
    auto b5 = Optional<int*, ReturnCode>();

    printf("%d returnCode=%d\n", b1 ? *b1 : -1, b1());
    printf("%d returnCode=%d\n", b2 ? *b2 : -1, b2());
    printf("%d returnCode=%d\n", b3 ? (*b3).a : -1, b3());
    printf("returnCode=%d\n", b4());
    printf("%p returnCode=%d\n", b5 ? *b5 : 0, b5());
    printf("\n");

    // if you prefer own syntax, do both in one step

    when(bla1(),
    {
        printf("b1=%d returnCode=%d\n", *_value, _value());
    } 
    else
    {
        printf("b1 has no value\n");
    });

    when(bla2(),
    {
        printf("b2=%d\n", *_value);
    } 
    else
    {
        printf("b2 has no value\n");
    });

    when(bla3(),
    {
        printf("b3=%d\n", (*_value).a);
    } 
    else
    {
        printf("b3 has no value\n");
    });

    when(bla4(),
    {
        printf("OK\n");
    }
    else
    {
        printf("b4 returnCode=%d\n", _value());
    });

    when(b5,
    {
        printf("b5=%p\n", *_value);
    } 
    else
    {
        printf("b5 has no value\n");
    });


    return 0;
}


/*
Program output:

10 returnCode=0
-1 returnCode=0
-1 returnCode=0
returnCode=2
(nil) returnCode=0

b1=10 returnCode=0
b2 has no value
b3 has no value
b4 returnCode=2
b5 has no value
*/