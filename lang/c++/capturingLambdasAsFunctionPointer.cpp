/*
Capturing lambdas as function pointer (callback) is not possible in C++. Only non-capturing lambdas are.

Naive workarounds include:
using a global variable,
passing generic user-data either with typed lambda (generic lambda expression since c++14) or void*.
*/

using Callback = void(*)(int);

void invoke(Callback c) {
    c(4);
}

int main()
{
    int a;
    auto nonCapturingLambda = [](int x) -> void {x;};
    auto capturingLambda = [=](int x) -> void {x; a;};
    invoke(nonCapturingLambda);
    invoke(capturingLambda);    // compiler error
    return 0;
}