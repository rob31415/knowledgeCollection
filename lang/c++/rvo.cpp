// rvo is enabled by dafault:
// g++ rvo.cpp && ./a.out

// disable rvo (see "man gcc"):
// g++ -fno-elide-constructors rvo.cpp && ./a.out

#include <string>
#include <stdio.h>

#define nullptr 0

/////////////// RVO

struct C {
  static int counter;
  C() {counter++;}
  C(const C&) { counter++; }
};
int C::counter = 0;

C f() {
  return C();
}

bool check() {
    const C obj = f();    // no intermediate/temp/anonymous object was constructed
    return C::counter==1;
}

/////////////// NRVO

void* addr = nullptr;

std::string st()
{
   std::string s("nrvo!");  // rvo works also for non-anonymous object
   addr = (void *)(&s);
   return s;
}

bool checkString()  // "named rvo"
{
    std::string s = st();
    return addr != nullptr && addr == &s;
}

int main()
{
  printf("compiler does RVO=%d\ncompiler does NRVO=%d\n", check(), checkString());
  return 0;
}