//g++ functionComposition.cpp && ./a.out || echo $?

#include <functional>

// this is like typedef, but using can be templated
template<typename T>
using SomeFct = std::function<T(int)>;


// returns a value
template<typename T>
T getValue(SomeFct<T> f1, SomeFct<T> f2, T x) {
    return f1(f2(x));
}


// returns a function
template<typename T>
std::function<T()> getFunction(SomeFct<T> f1, SomeFct<T> f2, T x) {
    return [f1,f2,x](){return f1(f2(x));};
}


int main() {
    getValue<int>([](int x){return x+1;}, [](int x){return x+1;}, 2);   // 4
    return getFunction<int>([](int x){return x+1;}, [](int x){return x+1;}, 40)();  // 42
}