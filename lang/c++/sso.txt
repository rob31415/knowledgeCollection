class String {
    union {
        char* _beg;
        char str[16];
    };
    // in constructor etc., if len<16 use str, otherwise _beg=new ...
}

is faster when (de)allocating
moving small strings is slower, because moving big strings is just pointer copying
