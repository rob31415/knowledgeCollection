// https://learnxinyminutes.com/docs/typescript/


# up and runnig

- install npm with OS's package manager
  - sudo npm -g install typescript
- "tsc --out o.js *.ts"
- "tsc --noEmitOnError hello.ts"
  - ts is compiled to js
  - without that switch, .js file is created even if ts has an error.
- "--target es2015"
  - by default "downleveling" to ES3

## tsconfig.json

The presence of a tsconfig.json file in a directory indicates that the directory is the root of a TypeScript project. 

# type system

It only applies at compile time!

## configuration

- type checking: "more loose opt-in experience" by default; you can "strict: true"  in config.json
- "noImplicitAny"   ts does type inference; by default, inference can always use "any" type, then it's plain JS behaviour
- "strictNullChecks"    null and undefined are assignable to any other type; not with this

## primitives 

note: always use lowercase

	let s:string;
	let n:number;
	let b:boolean;
	const oneHundred: bigint = BigInt(100);
	const s1 = Symbol("name");
	const s2 = Symbol("name");

	let arr1:number[];

s1 != s2 always

## type inference

	let msg = "hello there!";

## functions

	function add(left: number, right: number): number {
		return left + right;
	}

## types as sets (or: "wtf is | ?")

it's a "union".

- you have to think of it like this:
  - string type is a set of strings.
  - int type is a set of integers.
  - union of string type and int type == union of string values and int values

### what for?

	type ID = number | string;

	function union(x: number | string) {
		// "narrowing"
		if (typeof id === "string") {;} else {;}
	}

## type alias

think C "typedef"...

	type Point = {
		x: number;
		y: number;
	};

### type alias w/ union

	type ID = number | string;

## interfaces

	interface Point {
		x: number;
		y: number;
	}

## type vs interface

	interface A {x:number}   
	interface B extends A {y:number}

is similar to

	type TA = {x:number}    
	type B = A & {y:number}     // called "intersection"

but

	interface A {z:string}	

just "reopens" A and add some more. that's *not* possible w/ type.

## type assertion

"as" or "<>" does "type assertion", for when you know what it's gonna be. think "typecast"...

	const myCanvas = document.getElementById("main_canvas") as HTMLCanvasElement;

is equivalent to

	const myCanvas = <HTMLCanvasElement>document.getElementById("main_canvas");

this works only within inheritance line.

note: if that don't work for you, just cast to "any" and from there to anything you want...

## nominal vs structural type system

- Suppose there's an object O satisfying an interface I.
- Additionally, they do **not** have any declarative relationship (like inheritance).
- Within a *structural type system*, O can be used wherever I is expected.
- A *nominal type system* does not behave this way.

### ramifications

	class A {bla() {;}}
	class B {bla() {;}}
	let a:A = new B();	// no prob, identical types.

### more ramifications

	class Empty {}

Any object can be used where "Empty" is expected. Why? Because Empty has no props, so it expects at least "nothing".

Additional properties are irrelevant in this scenario.

## type erasure

typescript is fully erased - the types are not available at runtime.
Exception: for instance, typeof or instanceof work with general JS types.

## literal types

in the following "auto" is a literal type.

	interface Options {
		width: number;
	}
	function configure(x: Options | "auto") {
		// ...
	}

## literal inference

	const x: string = "hi"
	function f(a:"hi") {}
	f(x)

Yields this errormessage: Argument of type 'string' is not assignable to parameter of type '"hi"'.

## as const

	const bah = { grxl: "G" } as const;

as const makes grxl be type *"G"*, without it, the type would be *string*

## value space and type space

- There's value space and type space (erased at runtime).
- value space = Value literals, variables, constants, parameters, functions, class declarations, enums
- type space = Any definition with a type keyword, interfaces, class declarations, enums

Names in type space and value space don't collide, this is why we can define both a type and a variable with the same name:

	type Foo = { type: true }
	var Foo = { value : true }

## Non-null Assertion Operator !

    function liveDangerously(x?: number | null) {
        // No error
        console.log(x!.toFixed());
        // Error: Object is possibly 'null' or 'undefined'.
        console.log(x.toFixed());
    }

# Data types

## Record

A Record<K, T> is an object type whose property keys are of type K and whose property values are of type T.

## Tuple

	var employee: [number, string] = [1, "Steve"];

	employee[0]; // returns 1
	employee[1]; // returns "Steve"

	employee.push(2, "Bill"); 

implements array methods (pop() etc.)

### destructuring

	function print(): [number, string] {  return [1,'my']  }
	const [num, txt] = print();


# ?

What is the use case of assigning an object to an instance-less, typed variable?

	let x = {one:"bla", two:2}
	class X {
		get one(): string {
			return "bla2"
		}
		f(): void {console.log("f")}
	}
	let y: X = x            // Property 'f' is missing in type '{ one: string; two: number; }' but required in type 'X'.
	console.log(y.one)      // "bla"
	console.log(y.two)      // Property 'two' does not exist on type 'X'.  but still executes and prints 2
	y.f()                   // exec fails with y.f is not a function
