# debug

What happens if a browser encounters CSS it doesn't understand?

It does nothing and just moves on to the next bit of CSS.

So,
- CSS doesn't have any error messages in console (or devTools).
- You can basically write total garbage without any notification whatsoever.

# basic stuff

    #idselector
    .classselector
    h1                  element selector
    img[src]            elements with specified attribute

- selector
- declaration (= property + prop value)
- values have many units (see link below)

# more selecting

## and

    class1.class2 {}

## or

    class1, class2 {}

### descendant combinator (aka get children)

    <main> <img>A</img> </main>
    <aside> <img>B</img> </aside>

    main img {...}
    aside img {...}

separated w/ a space.

## child combinator

    main > img {...}
    aside > img {...}

matches only direct children - opposed to descendant, which matches children's children as well.

## sibling combinators

    main + img      the first sibling
    main ~ img      all siblings

## units: em vs rem

- 2em means, 2* size declared in parent
- 2rem means, 2* size declared in <html> (r like in "root-parent" which is <html>)

# conflicts

The CSS language has rules to control which selector is stronger in the event of a conflict. These rules are called:

- cascade: (order) Later styles replace conflicting styles that appear earlier in the stylesheet
- specificity: e.g. class selector > element selector
- inheritance: some CSS property values set on parent elements are inherited by their child elements (color) and some aren't (width)

These three concepts (cascade, specificity, and inheritance) together control which CSS applies to what element.

## exception

!important. This is used to make a particular property and value the most specific thing, thus overriding the usual rules of the cascade.

# not so basic stuff

    :after
    ::after

## double-colon selectors are pseudo-elements

pseudo-element is a keyword added to a selector that lets you style a **specific part** of the selected element(s).

    ::before and ::after
    ::first-letter
    ::first-line
    ::backdrop
    ::marker
    ::selection
    ::placeholder
    ::cue

## single-colon selectors are (now) pseudo-classes

- pseudo-class is a keyword added to a selector that specifies a **special state** of the selected element(s).
- pseudo element/class distinction was not present in older versions of the W3C spec, so most browsers support both syntaxes for the original pseudo-elements.
- As a rule, double colons (::) should be used instead of a single colon (:).

### pseudo-classes

:active
:any-link
:autofill
:blank
:checked
:current
:default
:defined
:dir()
:disabled
:empty
:enabled
:first
:first-child
:first-of-type
:fullscreen
:future
:focus
:focus-visible
:focus-within
:has()
:host
:host()
:host-context()
:hover
:indeterminate
:in-range
:invalid
:is()
:lang()
:last-child
:last-of-type
:left
:link
:local-link
:modal
:not()
:nth-child()
:nth-col()
:nth-last-child()
:nth-last-col()
:nth-last-of-type()
:nth-of-type()
:only-child
:only-of-type
:optional
:out-of-range
:past
:picture-in-picture
:placeholder-shown
:paused
:playing
:read-only
:read-write
:required
:right
:root
:scope
:state()
:target
:target-within
:user-invalid
:valid
:visited
:where()

example

    a:hover             only the ones in state "hover"

# confusion of the highest orda

    A)
    [dropdown-item-checked]:after

    B)
    [dropdown-item-checked] ::after

what is selected? what's the difference?

...

# variables (custom properties)

## declare

    p { --my-var: #ff7a59; }
    or
    someElement.style.setProperty("--myvar", "green")

## use

    p { color: var(--myvar); }
    or
    someElement.style.getPropertyValue("--myvar")

## notes

- case sensitive!
- scope: Generally, a CSS variable is only visible to child elements of the parent element it is declared in.

# block vs inline

- block: takes all space available from containing element (dimension automatically calculated); linebrak afterwards
- inline: "in line with text", as wide as it's content (dimension auto calced; no manual width/padding etc possible)
- inline-block: inline but with manually specifiable dimensions

## nesting block and inline elements

which nesting is legal?

    into B I
    B    1 0
    I    1 1

- only block into inline is illegal (badly formed html).

# hiding

    display:none        // (like absolute positioning)
    visibility:hidden   // space is retained  (like relative positioning)

# layoutting

- layouts: from float to flexbox & grid
- display:flex changes child and parent.
- inner layout:
- outer layout: flex (block behaviour), inline-flex (inline behaviour)
- the next element upwards the chain that has "positioning" property set...
- each child of a flex container becomes a flex item

# pixel ratio

huawei lg30 lite: 1080x2312 /3 = 360x770.6

# height

- height: 100%  is relative to parent
- height: auto  is based on height of children
- CSS height does not work on inline elements, column groups, and table columns

Setting height:100% on an element with position:relative will have no effect unless the parent element has a defined heigh.

## height min/max

    min-height > max-height > height
        max-height overrides height
        min-height overrides both max-height and height

# stacking context

A stacking context is a group of elements that have a common parent and move up and down the z axis together.

Which element is being in front or behind of another element?

## created by specifying the following positions

- position: relative
- position: absolute
- position: fixed
- position: sticky

## created by z-index

- z-index: 0 (or any other numerical value; basically anything other that "auto")

## created by opacity

- any value <1

## a bunch of other reasons

https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_positioned_layout/Understanding_z-index/Stacking_context

# positioning

mostly set with top/right/bottom/left.

note: "page layout" here means "normal flow of the document"

- static
    - part of the page layout
    - no new stacking context
    - relative to position determined by page layout
    - this is the default
    - top/right/bottom/left not available
- relative
    - part of the page layout
    - creates new stacking context
        - within the page layout, the space for this element is retained
        - other elements' pos within the page layout **is not** affected
    - relative to position determined by page layout
    - similar to static, but it's moved relative to its determined position via top/right...
        - gives the visual impression that it's kind of "displaced"
- sticky
    - part of page layout
    - creates new stacking context
        - within the page layout, space for this element is created
        - other elements' pos within the page layout **is** affected
    - relative to nearest scrolling ancestor and containing block
        - meaning: scrolls along with its container, until it is at the top of the container or reaches the offset specified in top and will then stop scrolling, so it stays visible
        - https://developer.mozilla.org/en-US/docs/Web/CSS/Containing_block#identifying_the_containing_block
        - note: can move out of sight by scrolling up (it moves below the viewport bottom)
    - has a scrolling mechanism
- fixed
    - separated from page layout
    - creates stacking context
    - relative to the viewport
    - no space in document flow created for those elements
- absolute
    - separated from page layout
    - create stacking context if a z-index specified
    - relative to closest positioned ancestor (if any) or to the initial containing block
        - the containing block is formed by the edge of the padding box of the nearest ancestor element that has a position value other than static
    - no space in document flow created for those elements
    - similar to fixed if it's a 1st degree child of body (?)

# 2 box sizing modes

## box-sizing: content-box;   

- it's the default
- width and height properties do not include border, padding, or margins.
- Instead, they set the area's parameters inside the border, padding, and margin of the element.

## box-sizing: border-box;

- width/height specifications include borders

# media queries

- basis of "responsiveness"
- max-width is <
- min-width is >=

    @media all and (max-width: 768px) {}

    @media all and (min-width: 768px) {}

- there's no "else" like in imperative languages

## usual breakpoints

    none <576px
    sm   ≥576px
    md   ≥768px
    lg   ≥992px
    xl   ≥1200px
    xxl  ≥1400px

# links

https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/CSS_basics
https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/How_CSS_is_structured
https://cloudfour.com/thinks/modern-css-in-a-nutshell/
https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance
https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Values_and_Units
