# Table of contents

- [environment](#environment)
  - [executing a script in nodejs](#executing-a-script-in-nodejs)
- [!node --es_staging --harmony --use_strict myScript.js](#node---es_staging---harmony---use_strict-myscriptjs)
  - [linting](#linting)
  - [feature / compiler table](#feature--compiler-table)
- [types](#types)
- [scope](#scope)
  - [global](#global)
  - [hoisting](#hoisting)
  - [const](#const)
  - [scope pre ES6 (and eval)](#scope-pre-es6-and-eval)
- [object](#object)
  - [object properties](#object-properties)
  - [declaring an object](#declaring-an-object)
  - [removing a property](#removing-a-property)
  - [computed property names](#computed-property-names)
  - [prototype](#prototype)
- [this](#this)
  - [call / apply](#call--apply)
  - [bind](#bind)
  - [the "activation object"](#the-activation-object)
- [functions](#functions)
  - [function parameters](#function-parameters)
  - [function call behaviour](#function-call-behaviour)
- [constructors](#constructors)
- [array](#array)
  - [dense vs sparse](#dense-vs-sparse)
  - [types of keys() and values()](#types-of-keys-and-values)
  - [arrays can do lots of useful things](#arrays-can-do-lots-of-useful-things)
  - [copyWithin](#copywithin)
  - [2d array](#2d-array)
  - [3D array](#3d-array)
  - [slice](#slice)
  - [splice](#splice)
  - [join](#join)
  - [map/filter/lfold](#mapfilterlfold)
  - [array like object](#array-like-object)
  - [Typed arrays (aka ArrayBuffer)](#typed-arrays-aka-arraybuffer)
  - [More array madness: Object or Array?](#more-array-madness-object-or-array)
- [iterable](#iterable)
  - [implement iterator protocol](#implement-iterator-protocol)
- [quirk time](#quirk-time)
  - [semicolon or not?](#semicolon-or-not)
  - [strict in scope](#strict-in-scope)
  - [identity vs equality](#identity-vs-equality)
  - [parseint](#parseint)
  - [for loop through arrays](#for-loop-through-arrays)
  - [??](#)
- [Arrow Functions](#arrow-functions)
  - [namespacing](#namespacing)
  - [declaring dependencies](#declaring-dependencies)
  - [private, privileged, revealing private](#private-privileged-revealing-private)
  - [module pattern](#module-pattern)
  - [sandbox](#sandbox)
  - [object constants](#object-constants)
  - [chaining, method()-method](#chaining-method-method)
- [inheritance](#inheritance)
  - [prototypal](#prototypal)
  - [classes](#classes)
- [symbol](#symbol)
- [labels](#labels)
- [spread operator](#spread-operator)
- [Template Literals (template strings)](#template-literals-template-strings)
  - [interpolation](#interpolation)
  - [custom](#custom)
  - [raw string builtin](#raw-string-builtin)
- [Extended Literals](#extended-literals)
- [regex](#regex)
- [Destructuring Assignment](#destructuring-assignment)
- [Modules](#modules)
- [Generators](#generators)
- [weak ref](#weak-ref)
- [collections](#collections)
  - [set](#set)
  - [map](#map)
  - [weak map/set](#weak-mapset)
- [asynchronicity in JS](#asynchronicityhttpswwwyoutubecomwatchvsi5cspucdgy-in-js)
  - [Promises](#promises)
  - [async/await](#asyncawait)
  - [web workers](#web-workers)
- [Meta-Programming](#meta-programming)
  - [proxy](#proxy)
  - [reflection](#reflection)
- [Internationalization & Localization](#internationalization--localization)
  - [language-sensitive string comparison aka Collation](#language-sensitive-string-comparison-aka-collation)
  - [Number Formatting](#number-formatting)
  - [Currency Formatting](#currency-formatting)
  - [Date/Time Formatting](#datetime-formatting)
- [Built-In Methods](#built-in-methods)
  - [Object Property Assignment](#object-property-assignment)
  - [Array Element Finding](#array-element-finding)
  - [String Repeating](#string-repeating)
  - [String Searching](#string-searching)
  - [Number Type Checking](#number-type-checking)
  - [Number Safety Checking](#number-safety-checking)
  - [epsilon for Number Comparison](#epsilon-for-number-comparison)
  - [Number Truncation](#number-truncation)
  - [Number Sign Determination](#number-sign-determination)

# environment

an "environment" is a browser, any js engine, a database, etc.

## executing a script in nodejs

    #!node --es_staging --harmony --use_strict myScript.js

## linting

jslint ("Use spaces, not tabs") vs jshint ("--show-non-errors")

## feature / compiler table

    https://kangax.github.io/compat-table/es6/

# types

- there are 5 primitive types that are not an object: number, string, boolean, null, undefined
- they are immutable
- number, string, boolean have an "object representation" (wrapper)

## primitive values null vs undefined

- undefined means, a variable has been declared but has not yet been assigned a value
- null can be assigned to a variable as a representation of no value

# scope

- var       - function body scoped; is hoisted; can be used to declare global variable; redeclaration possible
- let/const - block scoped; not hoisted; can't be used to declare global variable; no redeclaration; good in closures
- omitted   - ?

## global

    var aGlobal = "x";
    function impliedGlobalF() {
        impliedGlobal = 0;  // not possible in strict mode; most likely accidentally; that's why nonstrict is discouraged
        var local;
    }
    //delete aGlobal;           // works, but only if "var" is omitted, which is only possible in non-strict mode
    //delete impliedGlobal; // works, but only in non-strict

## hoisting

- vars used somewhere in a fct behave like they're defined at the top (with "undefined" - maybe that's declaring?).
- var in the same scope are considered declared, even when used before their definition.

example

    function hoisting() {
        // uncommenting the next line is equivalent to leaving it commented
        //var someVar;  // equivalent to "var someVar=undefined;"
        console.log(someVar);   // "undefined"
        var someVar = 1;
        console.log(someVar);   // "1"
    }

therefore, it's best to always use the "single var pattern"

    function singleVarPattern() {
        var a=1,    // put it on top of the function - C style, not at all C++ RAII like.
            b=2,
            c=3;
        // ... some code
    };


### note

js executes 2 stages:
1. stage: parsing and entering context
2. stage: runtime code exec. 

## const

    const someConst = {x:10};   // the object's content is still mutable

## scope pre ES6 (and eval)

iife idiom ("immediately invoked function expression"): a block-scope emulating function

    // eval() has access to outer scope
    (function () {
        var local = 1;
        eval("local=3; console.log(local)");    // "3"
        console.log(local); // "3"
    }());

    // Function() doesn't (and btw, it can evaluate a string argument, too)
    (function () {
        var local = 1;
        Function("console.log(typeof local);")();   // "undefined"; new is optional
    }());

# object

two types of object:
- host (defined, implemented and provided by environment, "built-in")
- native (defined in user-land)

An object is an associative array who's keys and values can be of any type (see also: [array-like object](#array-like-object)).
keys are called "properties".

Everything is always mutable (except const) - "everything" = user-defined native objects and most props of built-in native objs.

## object properties

properties are elements of objects. accessing/creating them is done in three equivalent ways:

    object.property                 so called "dot notation"
    object['property']              "bracket notation"
    object = {property:"value"}     "object literal notation" (creation only of course)

### Practical difference between specifying object keys literally vs bracket-notation?

    class X {
        static x0 = 0
    }

    const Y_FAIL = {    // literal doesn't evaluate member access
        X.x0: "A"
    }

    const Y_WORKS = {}
    Y[X.x0] = "A"       // bracket does

    const someVar = 0
    const Y_WORKS_ALSO = {
        someVar: "A"    // although top-level vars do get evaluated
    }

## declaring an object

    // start with empty object...
    var anEmptyObject = {}; // equivalent to "new Object()"
    anEmptyObject.prop1 = "Bla";
    anEmptyObject.prop2 = 41;
    anEmptyObject.prop3 = function() {}

    // ...or better here, use object literal notation
    var anObjectIsAnAssociativeArray = {
        aProperty : "aValue",
        aFunctionProperty : function () {}
    };

note: an object literal is equivalent to "new Object()"

## removing a property

    anObjectIsAnAssociativeArray.addAnotherFunction = function () {}
    delete anObjectIsAnAssociativeArray.addAnotherFunction; // remove it again

## computed property names

    console.log({    [ "foo" + Math.random() ]: 42 })

## method property shorthand

instead of

    obj = {
        foo: function (a, b) {}
    }

you can write

    obj = {
        foo (a, b) {}
    }

## operators and properties

if you want to do this

    function $(elementId) {
        return document.getElementById(elementId).innerHTML
    }
    $("myElement") += "hello"

you're out of luck for 2 reasons:

- $ returns a string
- there's no way of returning a "reference" to a property, in a way that it's getter/setter - let alone operators like += can be invoked

## prototype

- Object is a built-in. prototype is a member of object.
- built-in prototype can be "augmented".
- changes are immediate/live for all sub-objects.

example

    var man = { hands: 2, legs: 2 };
    if(typeof Object.prototype.head === "undefined") {
        Object.prototype.head = function () {};
    }

so now, "man" has head() as well.

another example

    Number.prototype.addTo = function(value) {
        return this + value;
    }
    console.log((8).addTo(2))   // 10
    //console.log(8.addTo(2))   // doesn't work, for js tokenizer, "." is a decimal pt.
    console.log(8['addTo'](3)); // 11  showcasing "bracket notation"


### property flags

A property has three "flags":

- writable - propertie's value can be modified
- configurable - property may be deleted and flags may be changed
- enumerable - prop shows up during enumeration of it's object

### define a property programmatically

    Object.defineProperty(
        someObject,
        "someConstVariable",
        {value: 3.141593, 
        enumerable: true, 
        writable: false, 
        configurable: false });

the 3rd param is called "access descriptor".

### get property flags programatically

    Object.getOwnPropertyDescriptor(someObject, "somePropertyName");

### more on access descriptors (getter/setter) and method properties

    var o = {};
    Object.defineProperty(o, 'b', {
            // Using "method properties" (ES2015 feature).
            // It's equivalent to:
            // get: function() { return bValue; },
            // set: function(newValue) { bValue = newValue; },
        get() { return 0; },
        set(newValue) { bValue = newValue; },
        enumerable: true,
        configurable: true
    });
    o.b=10
    console.log(o.b)   // 0

note that besides "access descriptors", there's also "data descriptor" which defines the "value" - you can't mix both.

### object properties enumeration

here are 5 alternatives for doing this

    for (var i in myObject) {
        // only use what a myObject itself really has; omit super-object's properties.
        if(man.hasOwnProperty(i)) { /*...*/  }
        if(Object.prototype.hasOwnProperty.call(myObject,i)) {  }   // alternative
    }

    console.dir({a:1, b:"bla"});
    console.log(JSON.stringify(myObject))
    console.log( Object.fromEntries(myMapOrSet) )
    console.log( [...arr.keys()] )

but

    console.log(JSON.stringify( Object.fromEntries([1,2,3].keys())) );
    // error: Uncaught TypeError: Iterator value 0 is not an entry object
    what's an "entry object" ?

    let s = new Set()
    s.add({id:"bla"})
    console.log( [...s.entries()] )
    // [[{id: "bla"}, [circular object Object]]]

### hasOwnProperty vs "in" operator

Some parent of document.body has "innerHTML", but document.body itself doesn't.

    console.log(document.body.hasOwnProperty("innerHTML"))  // false
    console.log("innerHTML" in document.body)               // true

Unlike the in operator, hasOwnProperty does not check for the specified property in the object's prototype chain.

#### how to access a parent's property

idk..

    Object.getPrototypeOf(document.body)["innerHTML"] = "hi"

doesnt work :-(

### seal() vs freeze()

- seal() = prevents adding and/or removing properties
- makes every property non-configurable, and they cannot be converted from data accessors to properties
- freeze() = seal() + prevents changing any existing properties
- Neither one affects children objects

### deep copy (by ref and by val)

    var someObjectWithSubobjects = { b: {c:4} , d: { e: {f:1}} };

    var deepCopyByRef = Object.assign({}, someObjectWithSubobjects);
    someObjectWithSubobjects.d.e.f = 2;
    console.log("byRef", deepCopyByRef); // {f: 2}

    var deepCopyByVal = JSON.parse(JSON.stringify(someObjectWithSubobjects));
    someObjectWithSubobjects.d.e.f = 3;
    console.log("byVal", deepCopyByVal.d.e); // {f: 2}

### merging objects behaviour

    var o1 = { a: 1, b: 1, c: 1 };
    var o2 = { b: 2, c: 2 };
    var o3 = { c: 3 };
    var obj = Object.assign({}, o1, o2, o3);
    console.log(obj); // { a: 1, b: 2, c: 3 }

# this

what is 'this'?

    if(used in global context) {

        it's the global object

        console.log(this === window); // true when in browser
        console.log(this === global); // true in nodejs

    } else {

        if(function is bound to an object) {

            this is enclosing Object
            for nested objects, it's always the immediate/closest parent

        } else {

            if(strict mode) {
                undefined
            } else {
                the global object
            }

        }

    }


since this is an rvalue, there are two ways of implicitly setting it: call/apply and bind.

### lexical this

- it's a "this" in an arrow function.
- in arrow functions, there's no this, meaning that the arrow function does not define (or "bind") its own this.
- so, this in an arrow function refers to the this in the enclosing function - hence "lexical this" != "normal" this

## call / apply

    function add(c, d) { return this.a + this.b + c + d; }
    var o = {a: 1, b: 3};
    add.call(o, 5, 7);      //16
    add.apply(o, [10, 20]); // 34

## bind

creates a new function from the old and makes the given object it's this.

    function f(){ return this.a  }
    var g = f.bind({a: 'azerty'})
    console.log(g())     // azerty

### partial application

bind can also be used for partial application

    function f(a,b) {console.log(a,b)}
    const pa = f.bind(this, "a")    // preceeding any additionally provided args
    pa("b")                         // additionally provided arg

    output: "a b"

#### "full" application

    function f(someText) { console.log(someText)  }
    const a = f.bind(this, "a")
    const b = f.bind(this, "b")
    a()     // a
    b()     // b

## the "activation object"

- The term "Variable object" is a general or abstract term used to refer to any object that holds the properties that describe the environment and scope of the currently executing context.
- It's either "window" or "global" or an "activation object"
- The latter is basically a container for all the local stuff you can access by name inside a function, except for this - so arguments and declared variables.

# functions

    function bla() {}               // fct statement
    const bla = function() { }      // fct expression
    new Function('a', 'b', 'return a + b'); // fct constructor

## function parameters

    function multiply(a, b = 1)     // default param
    function multiply(multiplier, ...rest)

    function defaultArgsOldStyle(x, y) { if (y === undefined) y = 7;  };


## function call behaviour

- it's not by-ref and not by-val, it's "call by object (sharing)" (barbara liskov)
- the value of the argument is not the direct alias, but a copy of the address

how does that behave?

    var callByObject = {x: 10, y: 20}

    function change(obj) {
        obj.x = 100
        obj.y = 200
        obj.z = 1
        return callByObject
    }
    console.log("change", change(callByObject))     // { x: 100, y: 200, z: 1 }

    var callByObject2 = {x: 10, y: 20}

    function addNewProps(obj) {
        obj = {z: 1, q: 2}      // callByObject is not modified, obj is a copy of callByObject's address (hence, it's not call by-ref)
        delete obj.x
        return callByObject2
    }
    console.log("addNewProps", addNewProps(callByObject2));  // { x: 10, y: 20 }


# constructors


constructors always return an object.
either the "this"-object, when return is omitted or any object you specify.

    var Person = function(name) {
        // this is magically there. it's not empty really.
        // it's like: var this = Object.create(Person.prototype)
        this.name = name;
        return new Number(2);
    };
    var roger = new Person("roger");
    console.log(roger.name);    // "undefined"
    console.log(roger); "12"

the following doesn't work because "Cannot set property 'name' of undefined".
means: js wants to set name on global object and forgotNew is undefined.

    var forgotNew = Person("bla");    

amend forgotten new

    function Book(name, year) { 
        if (!(this instanceof Book)) {    // amend forgotten new
            return new Book(name, year);
        }
        this.name = name;
        this.year = year;
    }


# array

data can be stored at non-contiguous locations in the array,

    var fruits = [];
    fruits[5] = 'mango';
    console.log(fruits[0], fruits.length);  // "undefined 6"
    fruits.length = 5;  // deletes elements
    console.log(fruits[5]); // "undefined"

RegExp.exec, String.match, and String.replace, all return Arrays.

note

    console.log(Array(3), " is not ", Array.of(3));   // "[undefined, undefined, undefined] is not [3,3,3]"

## dense vs sparse

JavaScript arrays are not guaranteed to be dense. But it's keys are.

    var arr = ['a', , 'c'];
    var sparseKeys = Object.keys(arr);
    var denseKeys = [...arr.keys()];
    console.log(sparseKeys); // ['0', '2']
    console.log(denseKeys);  // [0, 1, 2]

## types of keys() and values()

Type difference between array and object

array  keys(): string (maybe number internally), values(): depends, entries(): [string,depends]

object keys(): depends, values(): depends, entries(): [depends,depends]

## arrays can do lots of useful things

- push/pop, shift/unshift, sort, fill, includes, lastIndexOf, concat, Array.isArray
- toSource() returns a literal
- every() ("for all" predicate) and some() ("exists" predicate)
- forEach()
- no "zip" though...

## copyWithin

copyWithin works like C and C++'s memmove, and is a high-performance method to shift the data of an Array

    console.log(["alpha", "bravo", "charlie", "delta"].copyWithin(2, 0)); // results in ["alpha", "bravo", "alpha", "bravo"]

copyWithin behaves python-slice-esque:

    console.log([1, 2, 3, 4, 5].copyWithin(-2)); // [1, 2, 3, 1, 2]

## 2d array

    [["a",2],["b",3]]

    (2) [(2) [...], (2) [...]]

## 3D array

    var a=[];
    a[0]=[];
    a[0][0]=[];

    (1) [(1) [(0) [] ] ]

## slice

slice returns a shallow copy of a portion of an array into a new array object

    console.log(['Banana', 'Orange', 'Lemon', 'Apple', 'Mango'].slice(1,3));
    // ["Orange", "Lemon"]

## splice

splice(start, deleteCount, item1, item2, ...) removes and adds elements IN PLACE

    var spliceExample = ["a", "b", "c", "d"];
    console.log(spliceExample.splice(2,1,"X")); // what's deleted ("c")
    console.log(spliceExample); // "a b X d"

## join

    console.log(["E", "W", "F"].join(" and ")); // a string "E and W and F"

## map/filter/lfold

    console.log([1,2,3,4,5].map(e=>e*2));   // 2, 4, 6, 8, 10  creates new one, not "inplace"
    console.log([1,2,3,4,5].filter(e=>e%2===0));    // 2, 4
    console.log([1,2,3,4,5].reduce((e1,e2)=>e1+e2));    // 15

## array like object

js objects are associative arrays.
- if key is a String, it's an "Object".
- if key is a Number (plus, it has some array specific methods), it's an "array".

example

    console.log( Array.isArray(["A", "B", "C"]) );  // true

    var isThisAnArrayLikeObject = {0:"A", 1:"B", 2:"C", length:3};
    console.log( Array.isArray(isThisAnArrayLikeObject) );    // false

    // why false?
    console.log( typeof(Object.keys(isThisAnArrayLikeObject)[0]) );   // string

    // in contrast:
    console.log(  typeof ( ["A", "B", "C"].keys().next().value ) );   // number

convert array-like object to array.
Array.from() creates a new Array instance from string, set, map or array-like object.

    console.log(typeof(Array.from(isThisAnArrayLikeObject))); // "object"
    console.log(Array.isArray(Array.from(isThisAnArrayLikeObject))); // "true"

Array.from() is equivalent to:

    console.log(Array.prototype.slice.call("abc")); // ['a', 'b', 'c']

### array keys()

keys() returns an array of strings.
there's no way to get an object's key with it's original type.

an array's keys are maybe internally number, but coerced as string.

    var arr = [true];
    console.log(arr[0] === true)
    arr['0'] = false;
    console.log(arr[0] === false)

## Typed arrays (aka ArrayBuffer)

allow handling of raw, untyped data as js array by splitting concerns into:
- buffer (inaccessible for reading) and
- view (giving buffer a js format)

consider this c-struct

    struct someStruct {
        unsigned long A;  // 4 bytes
        char B[16];   // 16 bytes
        float C;  // 4 bytes
    };

24 bytes of raw untyped data. filled by some source.

    var buffer = new ArrayBuffer(24);   
    var viewA = new Uint32Array(buffer, 0, 1);  // use 3 views to access raw data
    var viewB = new Uint8Array(buffer, 4, 16);
    var viewC = new Float32Array(buffer, 20, 1);
    console.log(Array.isArray(viewA));  // false
    console.log(viewA[0]);

create a regular js array from a view:

    console.log(Array.isArray(Array.from(viewB)));    // true


## More array madness: Object or Array?

    var nonIntegerKey = [];

    nonIntegerKey[3.4] = "Oranges";
    console.log(nonIntegerKey[3.4]);        // "Oranges"

    console.log(nonIntegerKey.length);                // 0
    console.log(nonIntegerKey.hasOwnProperty(3.4));   // true
    console.log(nonIntegerKey);                       // Array []


# iterable

    var arr = ["a", "b", "c"];
    var iterator = arr.keys();
    console.log(iterator.next()); // { value: 0, done: false }
    console.log(iterator.next()); // { value: 1, done: false }
    console.log(iterator.next()); // { value: 2, done: false }
    console.log(iterator.next()); // { value: undefined, done: true }

## implement iterator protocol

an object has to have a property named by "Symbol.iterator" and implement nex().

    var someString = new String('hi');
    someString[Symbol.iterator] = function() {
        return {
            next: function() {
                if (blabla) {
                    return { value: 'someValue', done: false };
                } else {
                    return { done: true };
                }
            }
        };
    };

    // use it
    for(let e of someString) {...}

# quirk time

## semicolon or not?

opening curly brace: better in same line, because of js's "semicolon insertion mechanism"

    function semicolonFail() {
        return  
  		{                   // put right after return to make it good
            some: "thing"
        }
    }
    console.log("actually we want false. but:", semicolonFail() === undefined);   // true

## strict in scope

    var dummy = "use strict;";  // ignored by emascript3, use strict lang subset in es5; once per scope

## identity vs equality

better always avoid implicit and opaque typecasting by using

- identity operators: === and !===
- instead of equality operators == and !=

## parseint

    console.log(parseInt("10", 16));    // always specify base with parseint

## for loop through arrays

cache array length, because not doing so can get expensive.

    for(var i=0, max=[1, 2, 4].length; i<max; i++) { ...  }

## ??

    function f (x, y) {
        var a = Array.prototype.slice.call(arguments, 2);
        return (x + y) * a.length;
    };
    f(1, 2, "hello", true, 7) === 9;


# Arrow Functions 

nice anonymous function syntax, like in scala or c# and Java

    console.log( [1,2,3,4].map(v => v * 2) );   // arrow functions don't have a 'this'


## namespacing
## declaring dependencies
## private, privileged, revealing private
## module pattern
## sandbox
## object constants
## chaining, method()-method

# inheritance

- it's prototypial, not class based.
- a prototype is a parent of an object.
- a prototype is an object too, it's not an entity of a different category, like in Java.
- on access, a member is searched upwards the prototype chain.


## function constructor

    let a = function () { this.x = 10; }
    let b = new a();

## prototypal inheritance

    function Parent(name) {
        this.name = name || "noname";   // "if sth doesn't exist, define it, otherwise use existing" pattern
    }

    Parent.prototype.say = function() {
        return this.name;
    }

    function Child(name) {}

    function inherit(C, P) {
        C.prototype = new P();
    }

    inherit(Child, Parent);

    var kid = new Child("francis");
    console.log("inheritance classical: " + kid.say()); // noname

    // rent a constructor

    function Child2(name) {
        Parent.apply(this, arguments);
    }

    inherit(Child2, Parent);

    var kid2 = new Child2("francis");
    console.log("inheritance rent a constructor: " + kid2.say()); // francis


    function Child3(name) {
        Parent.apply(this, arguments);
    }
    Child3.prototype = new Parent();

    inherit(Child3, Parent);

    var kid3 = new Child3("thelonious");
    console.log("rent and set prototype: " + kid3.say()); // thelonious
    delete kid3.name;
    console.log("rent and set prototype: " + kid3.say()); // noname


## classes

classes are syntactic sugar for function constructors and prototypal inheritance.

    class A {
      	#bla
        constructor(a) {      // only 1 cons allowed for each class
            this.a = a
            this.#bla="pribate"	// must be declared in enclosing class
            return a
        }
        get a() {return "no";}
        set a(x) {}
        do() {console.log("doA")}

        someMeth() {
            console.log( this.#privateMethod() )
            console.log( A.#privateStaticMethod() )
        }
        static #privateStaticMethod() {     // private scope with so called "hash names"
            return 'statpriv';
        }
        #privateMethod() {
            return 'inspriv';
        }
    }

    class B extends A {
        constructor() { super("fromB") }
        do() {console.log("doB")}
        privateMethod() { return 'insnoprivB'; }
    }
    console.log(new B().a)  // "no"
    new A().do()  // "doA"
    new B().do()  // "doB"
    b = new B()
    b.someMeth()    // instpriv  statpriv

### immediate super access

    class A {    do() {console.log("A")}    }
    class B extends A {    do() {console.log("B")}    }
    class C extends B {    do() {super.do()} }	// no access to A.do(), just like in Java
    new C().do()

# symbol

    as seen in Ruby...

# labels

are a prefix for a statement or a code block, but not for functions.

    loop1:
    for (...) {
        if (...) {  continue loop1; }    // continue or break
        ...
    }

    bla: {... break bla ...}


# spread operator

    var params = [ "hello", true, 7 ]
    var other = [ 1, 2, ...params ] // [ 1, 2, "hello", true, 7 ]

# Template Literals (template strings)

## interpolation

    var customer = { name: "Foo" }
    var message = `Hello ${customer.name}`

## custom

    function bla(texts, ...expressions) {       // so called "tag function"
        console.log(texts)
        console.log(expressions)
        return "NO"
    }
    let age=42
    console.log(bla`The building is ${age} years old.`)

    > Array ["The building is ", " years old."]
    > Array [42]
    > "NO"

## raw string builtin

    console.log(String.raw`\n`)     // "\n"

# Extended Literals

    0b0011
    0o766
    𠮷" === "\u{20BB7}"

# regex

    y flag

    TODO

# Destructuring Assignment

    // Array Matching

    var x = [1,2,3]
    var [a,,b]=x

    [b,a]=[a,b]     // funny

    // Object matching, similar, but object, not array; goes deep

    someObj = { A:1, B:{C:2} }
    var { A:a, B:{C:b}, d=3 } = someObj  // creates a and b default d
    console.log(a,b,d)

this also works similar for function args.

# Modules

see ["ecma script modules"](../tech/js-module-systems.md#ESM)

# Generators

    let someGen = {
        *[Symbol.iterator]() { yield 1 }
    }

    // or like this ("direct")

    function* someGen() { yield 1 }

    // or like this in classes and objects

    *someGen () {}

generation stops, if generator returns (so, not yielding).

# weak ref

    let o = {id:"bla"}
    let ro = o      // ref to object
    let wro = new WeakRef(o)     
    console.log( o === ro, wro === ro)              // true false
    o = null
    console.log(JSON.stringify(ro), o === ro)       // {"id":"bla"} false
    console.log(JSON.stringify(wro))                // {}

So, when an object becomes null, references to it become a copy but weak refs don't.

# collections

## set

    let s = new Set()   // new mandatory
    s.add(33)
    s.size
    s.delete(33)
    s.has(33)
    s.forEach((value, key, theSetBeingIterated) => { /* ... */ })
    s.clear()
    s.keys()  == s.values()     // seems odd :-/

## map

    let m = new Map()   // a JS map is ordered
    m.set(1,1)          // key can be of any type (even object)
    m.get(1)
    m.size
    m.delete(1)
    m.has(1)
    m.keys(); m.values(); m.entries()
    m.forEach((value, key) => { /* ... */ } )
    m.clear()

## weak map/set

This is based on the concept of a "weak ref".
Weak collections are not enumerable. (TODO: why?)

    let s = new Set() 
    let m = new Map();

    let o = {id:"bla"}
    s.add(o)
    m.set(o,"...")      // object as a key
    console.log( [...s] )
    console.log( JSON.stringify(Object.fromEntries(m)) )
    console.log(s.has(o), m.has(o))     // true true
    o=null
    console.log( s.has(o), m.has(o))    // false false

TODO: why is there no difference in the outputs when using WeakSet/Map here?
Isn't supposed to be true true, true true in the case of Set/Map above?

## order

- Object: unordered collection of properties
    - actually, there are complicated rules for order (depending on ES version and property type)
    - practically not reliable because no one can memorize those
- map: iteration order = insertion order

### sort map

    // Step 1: Turn the Map into an array
    const unsortedArray = [...unsortedMap]
    // Step 2: Sort the array with a callback function
    const sortedArray = unsortedArray.sort(([key1, value1], [key2, value2]) => key1.localeCompare(key2))
    // Step 3: Turn the array back into a Map
    const sortedMap = new Map(sortedArray)

# ["asynchronicity"](https://www.youtube.com/watch?v=Si5CSpUCDGY) in JS

- callbacks, promises and async/await are single-threaded based on timeout (setTimeout or Ajax)- they just reorder code execution to avoid blocking.
- web workers are multi-threaded (processor threads, seperate from the browser thread)

the first can't be cancelled, web workers can be terminated (immediately).

## Promises

    function get() {
        return new Promise(
            function(resolve,reject) {
                if( Math.random()>0.5 ) {
                    resolve("okay")
                } else {
                    reject(Error("fail"))
                }
            }
        )
    }


    get()
    .then(function(r) {return "good "+r})
    .then(function(r) {console.log(r)})
    .catch(function(e){console.log(e.message)})
    .finally(e=>console.log("endo"))

promises can't be transpiled to older ES, so there's a promise-polyfill available.

#### multiple promises (promise combination)

    Promise.all([get(),get(),get()])
    .then( results => results.map( e => console.log(e)) )
    .catch( e => console.log(e.message) )

"then" gets evaluated only if all three succeed.

### generators with promises (aka coroutines)

TODO

## async/await


It's syntactic sugar for promises

    (async function() {
        try {
            console.log(await get())
        } catch(e) {
            console.log(e.message)
        } finally {
            console.log("endo")
        }
    })();

- "async" keyword in fct. defs or expressions
- within an async fct, "await" keyword before a statement which returns a promise

### chaining promises vs await

the following code examples show the behaviour of await.

    function getSomeData() {
        fetch("8.8.8.8").then(response1 => {
            console.log("A")
            fetch("8.8.8.8").then(response2 => {
                console.log("B")
                fetch("8.8.8.8").then(response3 => {
                    console.log("Really done")
                })
            })
        })
    }

    getSomeData()
    console.log("Not done yet")

is equivalent to

    async function getSomeData() {
        let response1 = await fetch('8.8.8.8');
        console.log("A")
        let response2 = await fetch('8.8.8.8');
        console.log("B")
        let response3 = await fetch('8.8.8.8');
        console.log("Really done")
    }
    getSomeData()
    console.log("Not done yet")

    // this leads to error: "await is only valid in async functions and the top level bodies of modules"
    // await getSomeData()

and both print

    Not done yet
    A
    B
    Really done

leaving out "await" in 2nd example prints

    A
    B
    Really done
    Not done yet

## web workers

It cannot access the DOM directly and you lose direct access to window object.
you cannot rely on a global state within them.
you cannot send in data that cannot be handled by structured clone algorithm

### structured clone algorithm

- copies complex JavaScript objects
- is used internally to transfer data between Workers via postMessage(), storing objects with IndexedDB, or copying objects for other APIs
- avoids infinitely traversing cycles
- can't duplicate: functions, DOM nodes, RegExp.lastIndex, property descriptors,setters,getters, any such metadata, prototype chain

### minimal example

main.js

    w = new Worker("someWorker.js")
    worker.addEventListener("message", e => {
        console.log(e.data)
    })
    w.postMessage("Hi")     // can also be an object

someWorker.js

    this.addEventListener("message", e => {
        console.log(e.data)
        this.postMessage("OK")
    })

### other workers

- shared worker: pretty much given up by now
- service worker: can do some things that a shared worker can - is more for network requests
- websockets: runs in browser main thread, can access DOM

# Meta-Programming

## proxy

The Proxy object enables you to create a proxy for another object, which can intercept and redefine fundamental operations for that object.

    let target = {greet:"Hello"}
    let proxy = new Proxy(target, 
        {                               // this is the "handler"
            get(receiver,name) {        // this is a trap
                return name in receiver ? receiver[name] : "Bon jour"
        }
    })
    console.log(proxy.greet, proxy.notThere)

### trap

- another word for "call interception" or "fundamental operations"
- methods inside the handler are called traps
- There are 13 traps
    - get, deleteProperty, ownKeys, has, apply, defineProperty, getPrototypeOf, setPrototypeOf, isExtensible, preventExtensions, getOwnPropertyDescriptor, enumerate, construct

## reflection

Reflect is a built-in object that provides methods for interceptable JavaScript operations. Those are the same as the 13 trap methods. Some of them are equivalent to methods in "Object" - e.g. Reflect.defineProperty and Object.defineProperty.

note: Unlike most global objects, Reflect is not a constructor. You cannot use it with a new operator or invoke the Reflect object as a function. All properties and methods of Reflect are static.

    let target = {greet:"Hello"}
    concole.log( Reflect.has(target,"greet") )

# Internationalization & Localization

## language-sensitive string comparison aka Collation

    var list = [ "ä", "a", "z" ]
    var c = new Intl.Collator("de")
    console.log(list.sort(c.compare)) // [ "a", "ä", "z" ]

## Number Formatting

    var c = new Intl.NumberFormat("de-DE")
    l10nDE.format(1234567.89) === "1.234.567,89"

## Currency Formatting

    var c = new Intl.NumberFormat("de-DE", { style: "currency", currency: "EUR" })
    c.format(100200300.40) === "100.200.300,40 €"

## Date/Time Formatting

    var c = new Intl.DateTimeFormat("de-DE")
    c.format(new Date("2015-01-02")) === "2.1.2015"

    Date()      returns string
    new Date()  returns date obj

# Built-In Methods

## Object Property Assignment

    var dest = {}
    Object.assign(dest, {a:1}, {b:2})
    console.log(dest)       // Object { a: 1, b: 2 }

## Array Element Finding

    console.log( [ 1, 3, 4, 2, 5 ].find(x => x > 3)  )  // 4
    console.log( [ 1, 3, 4, 2, 5 ].findIndex(x => x > 3)  )  // 2

## String Repeating

    "foo".repeat(3)

## String Searching

- startsWith("bla",startIdx)
- endsWith("bla",length)
- includes("bla",startIdx)

## Number Type Checking

    Number.isNAN(NaN)
    Number.isFinite(-Infinity)
    Number.isFinite(NaN) === false

## Number Safety Checking

    Number.isSafeInteger(9007199254740992) === false

btw: numbers are floats

## epsilon for Number Comparison

    console.log(Math.abs((0.1 + 0.2) - 0.3) < Number.EPSILON) // true

## Number Truncation

    Math.trunc(42.7)  // 42

## Number Sign Determination

    console.log(Math.sign(7))   // 1
    console.log(Math.sign(0))   // 0
    console.log(Math.sign(-0))  // -0
    console.log(Math.sign(-7))  // -1
    console.log(Math.sign(NaN)) // NaN
