# intro

bootstrap...

- ... is used to create responsive web UIs
- ... uses flexbox (a css3 web layout model)
- ... is a templating framework (whereas - for instance, angular is a structural framework)
- ... is mobile first, so the default always is what looks best on phones (e.g. nav is stacked)

## visual viewport

the part of the document that is currently visible.

Mobile browsers render pages in a virtual window ("viewport"), generally at 980px, which is usually wider than the screen, and then shrink the rendered result down so it can all be seen at once.
on small screens, everything gets tiny because of this default behaviour.

so, developers explicitly say: "don't do that" with this:

	<meta name="viewport" content="width=device-width, initial-scale=1">

device-with = screen in CSS pixels at a scale of 100%

## layout viewport

...

# style basics

## text utils

- text-justify  "text-lg-center" conditional halign (when bigger than lg (992px) center it)
- align-SID  valign (SID = top, mittle, etc)
- text-TYP   TYP=lowercase etc
- font-STYLE  ST=light, etc.
- text-decoration-none  (links have no underline)
- text-reset  links color becomes parents container color
- text-FLOW  FLOW=nowrap,break etc.
- text-COLOR  primary (bg-primary), success, etc.  colors: "contextual color names" primary,danger...


## images

- img-fluid		responsive images
- img-tumbnail  1px border
- rounded-top-pill-sm
- float-left/right
- text-center or mx-auto (when img is a block element)
- figure  figure-img  figure-caption

## css variables

- bootstrap defines them
    - colors, media queries (breakpoint-xs), font-family-sans-serif...
- and you can use and modify them

## text selection

- default: user-select-auto  -all -none
- blockquotes: can have footer; anything cna be bootstrap class blockquote, even a div
- list-unstyled  no bullets

# layouting with the grid

- 12 col grid
- breakpoints are widths, defined by bootstrap
    -xs: 0, sm: 576px, md: 768px, lg: 992px, xl: 1200px, xxl: 1400px
- breakpoints are meant for devices of 5 different size categories (extra small to extra large)
- layout is flexbox based
- relevant css-classes: container, row, column

they're typically used hierarchically like this:

    container
        row
            col

a row inside a col creates a nested grid (12 cols again)

## layout mechanic

- auto hsizing
    - something happens at breakpoints when viewport is resized horizontally
    - for instance, when an element (eg div) is declared w class ".container-sm", then below the threshold "sm", it'll take up 100% of available space, above only a fixed 540px (effectively leaving space for other elements "on cols" in the row)
- other behaviour
    - ".container-fluid" always takes all available space.
    - container adds 15px padding to each col per default

## specifying layout

### on a col

- col-2     take 2 cols
- col-sm-2  if window is <sm, take 100%, above, take 2 cols
- col-sm    if window is <sm, take 100%, above, determine automatically how much cols this takes

### on a row

- row-cols-2  try take 2 cols in each row
- row row-cols-1 row-cols-sm-2 row-cols-md-4"
    -  >md, take 4; >sm take 2, otherwise take 1
- row row-cols-2 row-cols-lg-4  
    - above the large breakpoint, try to fit 4, else 2

### wherever

- no-gutters  no 30px auto grid padding regarding row/col

- align-TYP-DIR  TYP items/self  DIR start/center/end (valign)
- justify-content-DIR  start around between center etc. (halign)

## vcenter items

- vh-100  100% of viewport height
- align-items-center

## multiple column classes

i.e. for specifying for each element, how much cols it should take up, depending on the device size

- offset-sm-2   on small devices, shift 2 cols to the right

## control order of elements in the layout

- order-BP-ORD		BP=sm,md,etc.  ORD=1-12

## grid alignment

- valign
    - in rows: align-items-start  center,end
    - in cols: align-self-start  center,end
- halign
    - justify-content-center   start,around...

# non-grid positioning

besides the grid, there's other css classes to position elements

## display props

- position classes: fixed-top, fixed-bottom, sticky-flow
- sticky-top doesn't work w/ MS (scrolls it to top egde and stays there, the rest keeps on scrolling )
- fixed-top
- fixed-bottom

## the d classes

they control the display properties

- d-block   		makes el a block
- d-inline-block  makes it inline
- d-flex        makes it flex container
- d-print-none    not going to paper
- d-none d-print-block    only shows up on printer
- d-md-flex       above md it's "controlled by flex", becomes flex item
- d-md-finline-flex   same, but becomes inline

### note: block element vs inline element

- Block elements: consume the entire width available, always start in a new line, have top and bottom margins
    - div,h1,p,table,ul,ol,header,footer,nav,main,section,article,aside,fieldset,...
    - by default, flex elements are block elements
- inline: consume just minimal space, other elements can be "in the same line"
    - a,br,img,span,label,b,...

## influence d-flex container

flex-row, flex-col, flex-sm-row-reverse, flex-wrap, align-content-stretch (works only w/o fixed width/height)

## influence d-flex element

- align-self-end

## floating

- float-BP-SIDE   float-md-right

can shift things left/right

## clearfix

Clears the floats of any child elements

## padding/margin

    PRO(-BP)-(N)SIZ

- PRO: m p
- SID: t r b l x y
- BP: sm >576px, md ...
- N: negative
- SIZ: 0 1 2 3 4 5 auto

examples:

mb-4  4 unit margin bottom
px-2  2 unit padding left/right

note: margins can be negative, paddings can't

## show hide content

- invisible/visible (space is taken up anyway)
- d-sm-none doesn't take up the space (dont show when width>sm)
- "d-none visible" both possible, effect: can't see it, but readable by descriptive devices (aria?)

## sizing

    SIZ(-AMT)

- SIZE: w h mw mh vw vh min-vw min-vh
- AMT: 25 50 75 100 auto

## border

- BORDER-SID-COL-0
- ROUNDED-SID-SHA-SIZ
- border-0 rounded-circle

# nav

- navs,tabs,pills,navbars

## with ul

    <ul class="nav nav-pills flex-column flex-sm-row">
        <li class="nav-item"> <a class="nav-link disabled" href="#">Höm</a>  </li>
    </ul>

## without ul

    <nav class="nav nav-tabs justify-content-end">
        <a class="nav-item nav-link" href="#">Höm</a>
    </ul>

## navbar

- navbar                default stacked
- navbar-expand-BP      when should they go horizontal

put nav/navbar in container to stay aligned with main content if you want to.

    nav navbar bg-dark navbar-expand-sm
        div container
            div navbar-nav
                a nav-item nav-link

## branding

- navbar-brand
    - link/headline/image
- navbar-text

## dropdown

- class "dropdown" on a div or a list item
- dropdown-toggle on link (little image revealing that it's dropdown)
- data attribute (alternative to javascript) data-toggle="dropdown"

    <!DOCTYPE html>
    <html>
        <head>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
			<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css">
        </head>
        <body>
                
            <nav class="navbar navbar-expand-sm">
                <div class="container">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="#">Höme</a>
                        <div class="dropdown">
                            <a class="nav-item nav-link dropdown-toggle" data-toggle="dropdown" href="#">Wassab</a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">A</a>
                                <a href="#" class="dropdown-item">AA</a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>

            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
        </body>
    </html>

## collapse

- collapse, navbar-collapse, id (tie together), navbar-toggler, navbar-toggler-icon

    <nav class="navbar">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#clps">Collapse</button>
            <div class="collapse navbar-collapse" id="clps">
                <div class="navbar-nav">
                        <a class="nav-item nav-link" href="#">One</a>
                        <a class="nav-item nav-link" href="#">Two</a>
                        <a class="nav-item nav-link" href="#">Three</a>
                </div>
            </div>
        </div>
    </nav>

# style elements

## buttons

- btn-sm / lg   on a button input look the same
- btn-COLOR
- btn-outline-COLOR
- btn-block  active / disabled
- groups to style many at once
    - btn-group (e.g. on a div, which contains buttons)
    - btn-group-vertical
    - btn-toolbar
    - btn-group-SIZE

## badges

- badge, badge-pill, badge-COLOR
- they resize (eg to font size) to fit well in the container

## progress bar

	<div class="progress">
        <div class="progress-bar  bg-success progress-bar-striped progress-bar-animated" style="width:42%"></div>
        <div class="progress-bar  bg-warn " style="width:10%"></div>
	</div>

here, the 2nd one starts at 42%.

## list group

for styling lists

- active disabled
- list-group-item-action
- list-group-item-COLOR
- list.group-horizontal-BREAKPOINT
- add a badge in list item
    - justify-content-between pushes badge to the edge

## breadcrumbs

can be used in:
- unorderd/unordert lists 
- and nav with anchors

    <style>
        .breadcrumb-item+.breadcrumb-item::before {
            content: '|'
        }
    </style>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">bla</li>
        <li class="breadcrumb-item active">bla2</li>
        <li class="breadcrumb-item">bla3</li>
    </ol>

## shadow

- shadow-sm shadow shadow-lg shadow-none
- only for box elements (not for text)

# layout component

there's design patterns - predefined through various components.
bootstrap offers various of them for common layout tasks.

## jumbotron

- jumbotron, jumbotron-fluid

    <header class="jumbotron">
        <div class="display-2 mb-4">BLABLA</div>
        <p class="lead">some more blabla blurb</p>
    </header>

## tables

- table table-dark table-striped table-bordered table-borderless table-hover
- thead-light thead-dark table-COLOR bg-COLOR text-COLOR
- table-sm table-responsive-BREAKPOINT

## cards

- card container, cart-text, title subtitle link img img-top img-bottom img-overlay
- colors: bg-... border-... text-...
- card-body header footer
- list-group -item -flush
- card-group card-deck card-columns or use the grid (meaning: row, div for each item)

## media

class media for positioning some media alongside some content.

# form styles

## form

    <form class="form-inline">
        <input class="form-control" type="text" placeholder="blabla">
        <button class="btn" type="submit">STOP</button>
    </form>

- form-group can be used w any block element.
- form-control for input, select & text areas
- form-control-file
- form-inline make it go in one line

in desktop GUI frameworks, a fieldset is often called a "panel".


    <form>
        <fieldset class="form-group">
            <legend>Your Info</legend>
            <div class="form-group">
                <label class="form-control-label" for="name">your Name</label>
                <input class="form-control" type="text" id="name" placeholder="Give it"/>
                <small class="form-text text-muted">Blabla</small>
            </div>
        </fieldset>
    </form>

## multicol form

- use the grid (row and col)
    - note: once you put "row" to the container (eg div), you can use col-md-xx stuff.
    - just using "col" w/o -md... makes elements take up all availabe space
- form-row  col-auto  col-form-label

## input groups

- adding text, buttons, or button groups on either side of textual inputs, custom selects, and custom file inputs
- input-group  -prepend -append -text

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">@</span>
        </div>
        <input type="text" class="form-control" placeholder="Username">
    </div>

## checkbox / radio

- form-check goes on container for boxes
- form check-label, form-check-input form-check-inline (side by side, default as block by default)
- custom-control custom-control-input

## custom controls

- look similar in different browsers / platforms
- custom-TYPE   select-sm radio checkbox switch range file-input
- custom-control-label


# interactive components

## tooltips

- data-toggle="tooltip" title="my tooltip" data  data-trigger="click"
- off by default
- common options: placement trigger html

    	<p data-toggle="tooltip" data-palcement="top" title="BLABLA">TOOLTIP!</p>

## popover

    <button type="button" class="btn btn-info" title="TITLE" data-toggle="popover" data-content="Out bla Ultra, X">PopoVer</button>
    <script>
        $(function(){$('[data-toggle="popover"]').popover() })
    </script>

## alerts

	<div class="alert alert-info alert-dismissible fade show">
		<button type="button" class="close" data-dismiss="alert">
			<span>&times;</span>
		</button>
		<p class="alert-heading">ATTENTION</p>
		<p>blabla</p>
	</div>

## accordion

    <div id="acco" role="tablist">
        <div class="card">
            <div class="card-header" role="tab">
                <a href="#contento" data-toggle="collapse" data-parent="acco">BLABLA</a>
            </div>
            <div id="contento" class="collapse show" role="tabpanel">
                <div class="card-block">
                    contentcontent<br>
                    collapsse
                </div>
            </div>
        </div>
    </div>

note: anchors use "href", buttons use "data-target"

## modal

- modal-dialog -content -header -body -footer
- modal-title modal-dialog-centered (vcenter) modal-dialog-scrollable
- data-dismiss="modal" for e.g. a close button

4 sizes 300px - 1140 px

    <button data-toggle="modal" data-target="#mymo">MODAL</button>
    <div class="modal fade" id="mymo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    BLABLA<br>BLABLABLA 
                </div>
            </div>
        </div>
    </div>

## carousel

- carousel, slide, carousel-fade and a target
- carousel-inner 
    - carousel-item
        - img d-block & w-100
        - crop the images, make all same size
        - carousel-caption
- controls & indicators
- data attributes
    - ride carousel | false
    - interval in ms
    - pause hover | false
    - touch keyboard wrap  true
- controls
    - carousel-control-prev -next
    - href to target
- carousel-indicators
    - data-target=
    - data-slide-to="#"

    <div class="carousel slide carousel-fade" data-ride="carousel" id="featured">
        <ol class="carousel-indicators">
            <li data-target="#featured" data-slide-to="0" class="active"></li>
            <li data-target="#featured" data-slide-to="1"></li>
            <li data-target="#featured" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="https://ec.europa.eu/info/sites/default/themes/europa/images/svg/logo/logo--de.svg" alt="">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="https://ec.europa.eu/info/sites/default/themes/europa/images/svg/logo/logo--en.svg" alt="">
                <div>Blabla</div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="https://ec.europa.eu/info/sites/default/themes/europa/images/svg/logo/logo--fr.svg" alt="">
            </div>
        </div>
        <a href="#featured" class="carousel-control-prev">Prev</a>
        <a href="#featured" class="carousel-control-next">Next</a>
    </div>

## scrollspy

	<body data-spy="scroll" data-target="#mynavbar" data-offset="10">

## toasts

    <div class="toast" data-delay="5000">
        <div class="toast-header">
            Header
        </div>
        <div class="toast-body">
            Blablablab.. blalbla
        </div>
    </div>
    <button id="showToast" class="btn">Toast</button>

    <script>
        $(document).ready(function() {
            $("#showToast").click(function() {
                $(".toast").toast("show")
            })
        })
    </script>

## spinnners

- spinner-TYP-SIZ   border/grow  sm
- text-COL

    <div class="spinner-border" role="status">
        <span class="sr-only">Loading...</span>
    </div>

## pagination

    <nav>
        <ul class="pagination">
            <li class="page-item active"> <a class="page-link" href="#">A</a> </li>
            <li class="page-item"> <a class="page-link" href="#">B</a> </li>
            <li class="page-item disabled"> <a class="page-link" href="#">C</a> </li>
        </ul>
    </nav>

## strechted links

not only the element, but it's enclosing container becomes clickable.

    <a href="#" class="btn stretched-link">Go away</a>

## formatting embeds

control looks of embedded elements

- embed-responsive
    - embed-responsive-SIZ   21by9  16by9  4by3 1by1
- embed-responsive-item

    <div class="embed-responsive embed-responsive-16by9">
        <iframe width="100" height="100" class="embed-responsive-item" src="https://ec.europa.eu/info/index_de" frameborder="0"></iframe>
    </div>
