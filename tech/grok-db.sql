
DB OBJECTS IN GENERAL
____________________________________________

oracle: 
table, trigger, sequence, type, procedure, synonym, view


CONCEPTS
____________________________________________

oracle:
tablespace has n data files. segment (contains table or index) can span multiple data files.
schema = formal organization of data, independent of physical storage.
each user has automatically their own schema, user=schema.

database = files; instance = in memory (sid).
instance is selected via ENV variable ORACLE_SID - "startup nomount" starts the instance w/o database.
instance is created/edited via initMYINSTANCENAME.ora (spfile > pfile).
database is created with "create database".

for oracle <=11, 1 instance can host 1 database
for oracle >= 12: PDB = pluggable db; CDB = container db (contains 0..n PDBs)

role = collection of rights; a role can be given to a user, they get all the rights of the role.


CONNECT
____________________________________________

oracle:
connect schema@dbName;
connect / as sysdba
sqlplus / as sysdba

mssql:
c:\Program Files\Microsoft SQL Server\100\Tools\Binn>sqlcmd -S .\SQLEXPRESS (dbName)


RUN SQL FROM FILE
____________________________________________

oracle:
sqlplus -S someuser/somepass@dbName < bla.sql > out.txt


TABLES
____________________________________________

oracle:
select distinct object_name from dba_objects where object_type = 'TABLE' and owner = 'COMEXT';

select OWNER, TABLESPACE_NAME, TABLE_NAME from dba_tables where TABLESPACE_NAME='USERS' and OWNER='COMEXT';

mssql:
select * from information_schema.tables where table_type='BASE TABLE'


USERS
____________________________________________

oracle:
select username, common from sys.all_users;

In Oracle 12c and above, there are two types of users.
1) create CDB user (aka "common user")
create user c##username identified by password;
2) create "pluggable user" aka "local user"
alter session set container = nameofyourpluggabledatabase;
create user comext identified by comext;

When you set the undocumented (hidden) parameter "_oracle_script"=true  you can create the user without a C## in from of the user ID.


alter session set container=COMEXT;				# ORA-28000: the account is locked
ALTER USER COMEXT IDENTIFIED BY oracle ACCOUNT UNLOCK;


SELECT * FROM DBA_USERS; 								# mysql "database" == schema/user in Oracle
SELECT username FROM DBA_USERS;
SELECT username, default_tablespace FROM DBA_USERS;		# relation database(=user) <-> tablespace

mssql:
select suser_sname(owner_sid) as 'Owner', state_desc from sys.databases;


LOGINS
____________________________________________

mssql:
select * from master.sys.server_principals


DATABASES
____________________________________________

mssql:
select name from master.sys.databases; go
use someDbName;

oracle:
check tnsnames.ora and init*.ora to see which db is mounted in an instance
select name from v$database;


TABLESPACES
____________________________________________

oracle:
select distinct tablespace_name from dba_data_files order by 1;

There is no built-in method for oracle to auto-add datafiles to tablespaces.
BEWARE: The "unlimited" operator in oracle is not actually unlimited. It only means unlimited up to oracle's hard cap on datafile sizes, which depends on your db_block_size parameter. A default database is going to have an 8k block size and use smallfile tablespaces, which means the datafiles won't grow past 32GB.
Once the datafile reaches the hard limit, even when you have AUTOEXTEND MAXSIZE UNLIMITED set, you will still have to add a new datafile by hand.

CREATE BIGFILE TABLESPACE somename DATAFILE '/somepath/somefile.dbf' SIZE 100G AUTOEXTEND ON;


TABLES OF ALL TABLESPACES & THEIR SIZE
____________________________________________

oracle:
select table_name, b.tablespace_name,sum( bytes)/1024/1024 "SIZE IN MB"
from USER_segments a,user_tables b
where  table_name=segment_name
group by segment_name,b.tablespace_name,table_name
order by "SIZE IN MB" desc;


SCHEMA OBJECTS IN TABLESPACE
____________________________________________

oracle:
select distinct segment_type from dba_segments where tablespace_name='INDX';

GRANT SOME PERMISSIONS

oracle:
grant dba to someuser;

mssql:
GRANT SELECT, INSERT, UPDATE, DELETE ON SCHEMA :: dbo TO sa;


DDL
____________________________________________

oracle:
select dbms_metadata.get_ddl('TABLE','sometable','someuser') from dual;


SET PASSWORD
____________________________________________

mssql:
alter login sa enable;     # e.g. "sa" or any other login
alter login sa with password = 'myPassword';
go


DB LINK TO MSSQL
____________________________________________

oracle:
create database link myLinkName connect to "myUser" identified by "myPassword" using 'myDsnEntry'; # user, e.g. "sa"
select * from dba_db_links;             #  all_db_links, user_db_links
drop database link myLinkName;

# during install of mssql or in MSSqlMgmentStudio, set "Server authentication" to "SQL Server and Windows authentication mode"
# in ODBC Data Admin, for "myDsnEntry", select "With SQL Server authentication using login/id entered by user"
# mssql password, ODBC DSN password and oracle-dblink password, all have to match

tnsnames.ora
TEST =
  (DESCRIPTION =
   (ADDRESS = (PROTOCOL = TCP)(HOST = myComputerName)(PORT = 1521))
    (CONNECT_DATA = (SID = TEST))
   (HS = OK) 
  )

listener.ora
(SID_DESC =
    (SID_NAME = TEST)
    (ORACLE_HOME = C:\orakel\app\oracle\product\11.2.0\server)
    (PROGRAM = dg4odbc)
)

initTEST.ora
HS_FDS_CONNECT_INFO=TEST


ORACLE CONTAINERS (CDB)
____________________________________________

select name from V$CONTAINERS;			/**/

alter session set container=SOMEPDBNAME;		/* switch to PDB from inside CDB */


ORACLE role
____________________________________________

a role is a list of access grants.
it can be given to a user.
CREATE ROLE somename NOT IDENTIFIED;  # all rights


ORACLE: directories
____________________________________________

CREATE DIRECTORY somedir AS '/somepath';
GRANT READ, WRITE ON DIRECTORY somedir TO system;

SELECT * FROM dba_directories;


ORACLE: privileges
____________________________________________

SELECT * FROM USER_SYS_PRIVS;   # current user's privs

GRANT CREATE SEQUENCE TO COMEXT;


FIND VERSION
____________________________________________

Oracle: SELECT * FROM v$version;
