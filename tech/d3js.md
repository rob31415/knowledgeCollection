# notes d3.js

## D3js vs jQuery

Both are JavaScript DOM manipulation libraries, having:

- CSS selectors
- fluent API

jQuery can manipulate DOM elements in an imperative style only.

But what if you have data and want to declaratively sync DOM elements to it?

**D3js introduces a model of computation** specialised for this purpose.
It does this basically by providing 3 functions: data, enter, exit.
They declare what to do and they're executed by d3.js whenever the data changes.

### data()

data is given to d3js via this function.
Suppose, initially there's some data and no DOM elements - e.g. d3.select(someThing).data(["A,"B"])

### enter()

implements how to add DOM nodes according to the given data.
E.g. an array ["A", "B"] could result in DOM nodes: <li>A</LI> <li>B</LI>
After execution, the initially empty DOM is populated.

### exit()

implements how to remove DOM nodes.
E.g. given the state after enter() from above, if A got deleted from the data - it's ["B"] now - that could result in <li>A</LI>

### example

To a developer, the chained functions are comprehended in sequence, e.g.:

    select('#someThing')            // into this
    .insert('div', '.bla')
    .attr('class', 'someClass')     // insert a div w/ "someClass"

    .data(['A', 'B'])               // that's the data, the basis of everything

    .enter()                            // when d3js is supposed to add DOM elements
    .append('div')                      // it appends a div for each array entry
    .attr('class', 'legend-block')      // gives it this attribute
    .html(function (id) {...})          // and it will put that html into the div
    .on('mouseover', function (id) {})  // and attach an event to the div

    .exit()                         // when data is removed
    .remove()                       // also remove the DOM node

## DOM selection

- select(some css selector)
- selectAll(some css selector)

### examples

    d3.select('tag');
    d3.select('.class');
    d3.select('#id');

## DOM mod

- text()
- html()
- append()
- insert()
- remove()

## mod attributes/props

- style() (like html's stale attribute)
- classed() toggle classes on/off
- attr()
- property()

## add data

- data()
- one by one, matches elements of given array with DOM elements
- callback
- index

example

    d3.selectAll(".some")
    .data([1,2,3,4,5])
    .html(function(el,idx) {
        if(idx==0) {
            return "<bold>"
        } else {
            return el;
        }
    })

## subselection (selection revisited)

enter, "enter()". 

    <tbody></tbody>

    var someData = [
        {someField:'bla'}
    ]

    d3.select('tbody')
    .selectAll('tr')    # not yet in the html
    .data(someData)
    .enter()
    .append('tr')       # inserted w/ the data from above. they're appended, yet selected above
    .html(function(el) {
        return "<h>"+el.someField+"</h>";
    })

## SVG

- (0,0) is top left
- xlink:href copies an element
- svg has events

### naive creation example

    <div id="viz">
        <svg width height...>
            <line...>
            <rect...>
        </svg>
    </div>

### programmatic creation example

    <div id="viz"></div>

    d3.select('#viz').append('svg').append('rect').attr("x",100);   # another .append() wouldn't show
    d3.select('#viz svg').append('circle').append('rect').attr("x",100);

## example: draw bars

var bardata = [20, 30, 45, 15]; 
var height = 400, width = 600, barWidth = 50, barOffset = 5; 

    d3.select('#viz').append('svg') 
    .attr('width', width)
    .attr('height', height)
    .style('background', '#C9D7D6') 
    .selectAll('rect').data(bardata)
    .enter().append('rect')
        .style('fill', '#C61C6F') 
        .attr('width', barWidth) 
        .attr('height', function(d) { return d; }) 
        .attr('x', function(d, i) { return i*(barWidth + barOffset) }) 
        .attr('y', function(d) { return height — d; }); 

## scaling

### using linear scales

    var yScale = d3.scaleLinear().domain([0, d3.max(bardata)]).range();

        .attr('height', function(d) { return yScale(d); }) 

### ordinal scale

    var xScape = d3.scaleBand()
    .domain(bardata)
    .padding(0.2)
    .range([0,width]);

### color scales

color changes with height of a bar

    var color = d3.scaleLinear()
    .domain([0,d3.max(bardata)])
    .range(["#ffb832", "#c51c5f"])

        .attr("fill",function(el) {return color(el);})

## events

    .on("mouseover", function(el) {
        d3.select(this).style("opacity",.5)
    })

## transitions

animates a selection

    .on("mouseover", function(el) {
        de.select(this).transition().delay(500).duration(1000).style("fill","green")
    })

more elaborate

    myChart = d3.select....

    myChart.transition()
    .attr("height",function(el) {return yScale(el);})
    .attr("y",function(el) {return height - yScale(el);})
    .delay(function(el,i) {return i*20;})
    .duration(1000)
    .ease(d3.easeBounceOut)

## tooltip

    var tooltip=d3.select("body")
        .append("div")
        .style("position","absolute");
    
    .on("mouseover", function(el) {
        tooltip.transition().duration(200).style("opacity",0.8);

        tooltip.html(el)
            .style("left",(d3.event.pageX-25) + "px")
            .style("top",(d3.event.pageY-25) + "px")
    })

## accessing data

- openweathermap.org
- jsoneditoronline.org

example

    d3.json("somefile.json", function(el) {
        for(var i=0;i<el.list.length;i++) {
            mydata.push(el.list[i].some.nested.access):

        }
    })

## guide

create axes and ticks and then put them into the chart

