# toolkits

- create react app (learning & new browser SPA)
- next.js (server rendered w& node)
- gatsby (static)
- react native

# start a project quickly

    npx create-react-app hello-react --use-npm
    cd hello-react
    npm start

or
    codesandbox.io

note: npm as root gives EACCES errors - only solution is, to just not be root when using create-react-app.

# start a project from scratch manually

TODO

# the simplest index.js

    import React from 'react';
    import ReactDOM from 'react-dom';
    import './index.css';

    ReactDOM.render(
        React.createElement("h1",null,"Hay"),
        document.getElementById('root')
    );

## nested

    ReactDOM.render(
    React.createElement("div",null,  React.createElement("h2",null,"Hay2")  ),
    document.getElementById('root')
    );

# JSX

write html directly in js, translated by babel.

    ReactDOM.render(
    <ul>
        <li>A</li>
        <li>B</li>
    </ul>,
    document.getElementById('root')
    );

## templates!

    let bla="Nr1"
    ReactDOM.render(
        <ul>
            <li>{bla}</li>
            <li>B</li>
        </ul>,
        document.getElementById('root')
    );

# component

- an element is the most atomic unit in react
- component is a function which returns some UI
- components must start with capital letter
- numbers need curlys
- html "class" in jsx is "className"

## pass data from parent to child

    function Hi(props) {
        return <h1>Welcome {props.greet} {props.a + props.b}</h1>
    }
    ReactDOM.render(
        <Hi greet="Gal" a={3} b={3}/>,
        document.getElementById('root')
    );

## pass data from child to parent

- Pass a function as a prop to the Child component.
- Call the function in the Child component and pass the data as arguments.
- Access the data in the function in the Parent.


# looping

    const someData = ["Saturn","Venus","Pluto"]

    function Hi( {data} ) {
        return (
            <ul>
            { data.map(e => (<li>{e}</li>)) }
            </ul>
        )
    }
    ReactDOM.render(
        <Hi data={someData}/>,
        document.getElementById('root')
    );

- you might use object destructuring where applicable
- conditional rendering is just using if/switch

# fragments

- if you want to use 2 components, react complains (parsing error: adjacent jsx sibling components...)
- just wrap it in <React.Fragment> (equivalent to empty tag <></>)

# useState hook

    import React,{useState} from 'react';

    function Hi() {
        const [status,setStatus] = useState("Open");

        return (
            <>
            <h1>Status: {status}</h1>
            <button onClick={()=>setStatus('Closed')}>Close it</button>
            </>
        )
    }
    ReactDOM.render(
        <Hi/>,
        document.getElementById('root')
    );

# cors problem

    package.json:
    
    "proxy":"http://localhost:3000/"

# useRef

useRef creates a ref to 1.) any arbitrary, mutable JS object and 2) DOM elements

### useRef for object/variables

trivial in classes: this.myVariable
in fct. style: useRef()

### useRef for child DOM elements

React's raison d'être is (amongst other things), to promote composability.
One aspect of that is, that any component offers a (preferably narrow) interface.
In React, this interface are the "properties" (props).
It's information that a parent gives it's child - and the child defines which props it accepts.
So,props are a key part of creating components that are adaptable to different situations - i.e. re-usable and well composable.

useRef bypasses this interface and gives complete access to the "native" DOM element.
It's like if you'd be able to access private members of a C++ class (which you can, if you just know how).

in functional style: useRef()
in classes: createRef()

# Lifecycle Hooks

- classes have them.
- there's hooks pendants for them.
- the following is listed in the order they're called

## mounting phase

    constructor()
    static getDerivedStateFromProps()
    render()                            function body + returned value
    componentDidMount()                 useEffect

## updating

    static getDerivedStateFromProps()
    shouldComponentUpdate()
    render()                            function body + returned value
    getSnapshotBeforeUpdate()
    componentDidUpdate()                useEffect

## unmounting

    componentWillUnmount()              useEffect

## error stuff

    static getDerivedStateFromError()
    componentDidCatch()

# state

    class: setState()       modifies in place; this.state to access read-only; triggers re-render (direct access via this.someObject=... doesn't)

    fct: useState()         creates new object (ie you'd have to merge yourself))

# forceUpdate

    class: forceUpdate()
    fct: some workaround via setState

# props

attributes from html-tags are available via this.props in classes and fct parameter in function-based.

# previous state

Some lifecycle methods, like componentDidUpdate, provide the previous state and props.
functional style needs a workaround: useState or useMemo.

# context

definition: share data that can be considered "global" for a tree of React components.

## provide

    const userContext = React.createContext({name:"Hi"})
    // there's userContext.Provider and userContext.Consumer

    <UserProvider value=...>
      <HomePage />
    </UserProvider>

## use ("consume") like this

    class HomePage extends Component {
        this.context
    }

## or like this

    <UserConsumer>
        {(value) => {
            return <div>{value}</div>
        }}
    </UserConsumer>


to see how it goes for fct. style see: https://www.taniarascia.com/using-context-api-in-react/ - so much will be mentioned here: it ain't "easier".


# useEffect

A react-component should be pure.

    function Greet({ name }) {
        const message = `Hello, ${name}!`; // pure
        document.title = `Greetings to ${name}`; // BAD!
        return <div>{message}</div>;       // pure
    }

Why? 
Performance: React decides when and how often to run the function in order to render - not the user.
So, in the example above, title is modified every time.

Solution:

    function Greet({ name }) {
        const message = `Hello, ${name}!`;
        useEffect(() => {
            document.title = `Greetings to ${name}`;
        }, [name]);
        return <div>{message}</div>;
    }

...
https://dmitripavlutin.com/react-useeffect-explanation/

## useEffect and fetching data

    const [info,setInfo] = useState({})
    
    useEffect(() => {
        const fetchData = async () => {
            const result = await fetch("/apiv1/someResource")
            const body = await result.json()
            setInfo(body)
        }
        fetchData()
    }, [watchThis])

# useFetch

# useSelect

# useDispatch
