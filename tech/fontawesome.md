# can be imported/used in multiple ways

## via CDN in plain html

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    </head>
    <body>
        <a class="fas fa-camera"></a>
        <i class="fas fa-battery-half"></i>
    </body>

[see the gallery](https://fontawesome.com/v5.0/icons?d=gallery&p=2)

# features

## data-fa-transform

    grow-#, shrink-#, up-# down left right, rotate-#, flip-v