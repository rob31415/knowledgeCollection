# JWT

## alternative

- anything that works in servlets, works also in jaxrs.
    - oauth, client ssl certs, rbac (@RolesAllow), username/password


- monolith = session
- microservices = token

### username/pw

#### web.xml

    <security-constraint>
        <web-resource-collection>
            <url-pattern>/myWebApi/*</url-pattern>
        </web-resource-collection>
        <auth-constraint>
            <role-name>admin</role-name>
        </auth-constraint>
    </security-constraint>
    <login-config>
        <auth-method>BASIC</auth-method>
        <realm-name>authorized-user</realm-name>
    </login-config>

add user in application server (security/realms/...).
here, a group = a role

#### support in classes

    HttpAuthenticationFeature af = HttpAuthenticationFeature.basic("admin","password");
    Client client = ClientBuilder.newClient().register(af);

From now on, every sent request is using this registered auth.

note: With BASIC, creds go via cleartext base64 http header. use https only!

#### securityContext

    @Context SecurityContext securityContext;

    @Get
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt(@Context UriInfo info) {
        // info now gives you lots of things:  name/role of user; connection is secure etc.
        return "got it";
    }

## deps

pom.xml

    <dependency>
        io.jsonwebtoken
        jjwt-api
        0.10.7
    </dependency>
    <dependency>
        io.jsonwebtoken
        jjwt-impl
        0.10.7
        scope runtime
    </dependency>
    <dependency>
        io.jsonwebtoken
        jjwt-jackson
        0.10.7
        scope runtime
    </dependency>

the REST bean

    @Context SecurityContext securityContext;

## generate

    @Path("/generate-jwt")
    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public String generateJwt() {
        return generate(securityContext.getUserPrincipal().getName());
    }

    private String generate(String userName) {
        String fixedId = "aGeneratedStringId";
        String clearTextPwd = "this-is-very-long-this-is-very-long-this-is-very-long-";
        byte [] base64Pwd = DatatypeConverter.parseBase64Binary(clearTextPwd);

        Key signatureKey = new SecretKeySpec(base64Pwd,"HMACSHA256");

        JwtBuilder builder = Jwts.builder()
            .setId(fixedId)
            .setIssuedAt(Date.from(Instant.now()))
            .setSubject(userName)
            .SetIssuer("someIssuer")
            .setExpiration(Date.from(Instant.now().plus(3,ChronoUnit.HOURS)))
            .signWith(signatureKey);

        return builder.compact();
    }


## validate


    @Path("validate-jwt")
    @Post
    public void validateJwt(String jwt) {
        validate(jwt);
    }

    private void validate(String jwt) {
        String fixedId = "aGeneratedStringId";
        String clearTextPwd = "this-is-very-long-this-is-very-long-this-is-very-long-";
        byte [] base64Pwd = DatatypeConverter.parseBase64Binary(clearTextPwd);

        Key signatureKey = new SecretKeySpec(base64Pwd,"HMACSHA256");

        JwtParser parser = Jwts.parser();

        Claims claims = parser.SetSigningKey(signatureKey).parseClaimsJws(jwt).getBody();
        claims.getSubject();    
        claims.getIssuer();
        //...
    }

claims are key:values grouped as claim-set.

# CORS (cross origin res sharing)

- a way to allow legit js code to safely access remote hosts.
- passes around special headers.

## preflight

ORIGIN header has server from which the request comes.


    @Provider
    @PreMatching
    public class CORSFilter implements ContainerRequestFilter, ContainerResponseFilter {

        @Override
        public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
                throws IOException {
            
            if(!isCors(requestContext)) {
                return;
            }
            
            if(isPreflight(requestContext)) {
                responseContext.getHeaders().add("Access-Control-Allow-Credentials", true);
                responseContext.getHeaders().add("Access-Control-Allow-Methods", "GET,PUT,POST,OPTIONS,HEAD");
                responseContext.getHeaders().add("Access-Control-Allow-Headers", "*");
            }
            
            
        }

        @Override
        public void filter(ContainerRequestContext requestContext) throws IOException {
            if(isPreflight(requestContext)) {
                requestContext.abortWith(Response.ok().build());
            }
            
        }
        
        public boolean isPreflight(ContainerRequestContext ctxt) {
            return ctxt.getHeaderString("Origin") != null && ctxt.getMethod().equalsIgnoreCase("OPTIONS");
        }
        
        public boolean isCors(ContainerRequestContext ctxt) {
            return ctxt.getHeaderString("Origin")!= null;
        }

    }
