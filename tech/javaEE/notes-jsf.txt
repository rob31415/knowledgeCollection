 
JSF

It's MVC without the controller - there is one, the Faces Servlet (pendant to FrontController in Spring MVC), but the developer doesn't write controllers (aka handlers in spring MVC).
There's just the backing bean, referencing services and acting as plumbing between V and M.

config over code.

Faces Servlet - Manages request-processing lifecycle
Facelet - jsf file (the view)
Component - client side XHTML elements
Backing bean - CDI beans binding to component-data and actions
event handlers - backing bean methods bound to component actions

@Named("someBackingBean") and @SessionScoped (or @RequestScoped etc) makes a class' instance available in view (.jsf file).
<h:commandButton value="Submit" action="#{someBackingBean.greet}"/>  calls BackingBean.greet() method.

There's a couple of tag libraries (facelet, html, core, jstl).

someTemplate.xhtml contains some <ui:insert name="pageHeader"/>
another jsf can use that template and inject values into it:
<ui:composition template="someTemplate.xhtml"> <ui:define name="pageHeader"></> ...

With JSF expression language, properties from file can be referenced. e.g.:
<f:loadBundle basename="bla.class" var="bla">  #{msg['myProperty.pageTitle']}

images, css, js can of course be referenced.
<h:outputStylesheet library="css" name="someCss.css"/>  // library is a folder
<h:graphicImage library="img" value="/resouces/img/some.jpg"/>
<h:outputScript library="js" name="some.js"/>

A list (e.g. "items") in a backing bean is displayed thusly:
<h:dataTable value="#{someBean.items}"> 
	<h:column>
		<f:facet name="header">Name      # facets add additional configs which are not in the attribute list of the parent... quirky
		<h:outputText value="#{item.name}">
It's column oriented - multiple column can be added.


UI Components
	Simple (like text box, button...)
	Compound (dataTable...)
	Composite ?

Deleting from list via UI:
	have a fild "item" in backing bean.
	<h:link value="Remove" outcome="confirm"/>
	in confirm.jsf:
.....?


In JSF 1.2, a managed bean had to register it in JSF configuration file such as facesconfig.xml. From JSF 2.0 onwards, managed beans can be easily registered using annotation
JSF is a simple static Dependency Injection (DI) framework. Using @ManagedProperty annotation, a managed bean's property can be injected in another managed bean.


javax.faces.application
	@ResourceDependencies
	@ResourceDependency
javax.faces.bean
	@ManagedBean(name = "helloWorld", eager = true)   deprecated, use @Named
	@ManagedProperty		inject property to another bean
	@ReferencedBean
	scopes:
	@NoneScoped				lives during a single EL evaluation
	@Dependent				default scope - same scope as client bean
	@RequestScoped			single http request
	@ViewScoped				"view" in JSF means "html page". survives postback (an HTTP POST to the same page that the form is on)
	@SessionScoped			multiple http requests
	@ApplicationScoped		across entire app
	@ConversationScoped 	(JSF specific, ecplicitly controlled boundaries by developer)
	@CustomScoped
javax.faces.component	
	@FacesComponent
javax.faces.component.behavior	
	@FacesBehavior
javax.faces.convert	
	@FacesConverter
javax.faces.event	
	@ListenerFor
	@NamedEvent
javax.faces.render	
	@FacesBehaviorRenderer
	@FacesRenderer
javax.faces.validator	
	@FacesValidator

	@PostConstruct			init data in there, not in constructor or getters
	@PreDestroy

you can always get the underlying java http/servlet stuff, since JSF builds upon it:
(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false)

UI elements

<f:viewAction>
<h:inputText transient="true">		# don't create java representation (code/object() from this xthml element (JSF runtime builds a so called "component tree")
can't submit data without a <form>

input boxes, selection ui elements, jsf file uploading, page layout (grid,group,component,composition,fragment,include,remove,repeat,decorate), data table, links, buttons, <h:message>

JSF strives to provide as many facelet components as there are HTML controls. But there are still gaps.
possible: HTML5 + EL + JSF named-attributes


integration with HTML5 and javaEE via pass through elements (PTE) & pass through attributes (PTA):
PTE = HTML5 components + snippets (making them feel like JSF components)  (e.g. HTML5 datetimepicker binding to backing-bean property)
PTA = JSF component + snippets (making them feel like HTML5 components)

THere is a Java equivalent of most JSF components - and the can be bound together - e.g. to mofify element from java.
someBean {private HtmlInputSecret someProperty;}
<h:InputSecret binding="#{someBean.someProperty}"/>

EL was availabe in JSPs, also in SPring. It provieds Java-like functionality outside of Java.
It's got streams (without Java8): ...stream().filter(someLambda...)

in web.xml FACELET_SKIP_COMMENTS can avoid jsp comments to go to browser.



<f:ajax render="out" execute="sometextbox someinputbox" listener="someMethodOnBean" delay=.. diabled=.. event=.. immediate=.. >
	<outputText id="out"  value="#{someBean.someProp}>									# this is "ajax updated"; someBean.someProp is string
	some input/command elements (having id="sometextbox" and id="someinputbox") 		# these are "ajax updating"; page is not reloaded.

"focussed" ajax:

<h:inputText id="x" value="#{someBean.someProp}">
	<f:ajax render="out" event="keyup" onEvent=someJavascript onError=anotherJS >
</>

render and execute support EL (so, source/targets can be determined at runtime)

<f:ajax render="@form" execute="@this"...


Lifecycle


1. Restore view phase								update/create component tree
2. Apply request values phase; process events		set values from HTML elements on java objects
3. Process validations phase; process events
4. Update model values phase; process events
5. Invoke application phase; process events			call backing bean's methods
6. Render response phase


Converters

Converter is a component, which converts web-page input to a java object in backing bean.
Used in lifecycle step 3.
<h:inputText converter="javax.facex.BigDecimal" converterMessage="Bad"

Own converter: @FacesConverter(forClass=someClass.class) overwrite "getAsObject" (from UI to java obj) and "getAsString" (java object to UI) methods.

<f:validateLength min= max=>

see also: INTERPRET_EMPTY_STRING_AS_NULL setting in web.xml

@FacesValidator("someValidator") (on class) and overwrite validate(...) to throw ex; input-tag validator="someValidator"
do your own stuff here...


ViewState object

can be configured STATE_SAVING_METHOD to be on client or on server (better).
"com.sun.faces.CLientStateSavingPassword" config in web.xml is mandatory

to protect aginst CSRF and XSS, "protected views" have to be set up - "faces.config" - then use escape="true" in all input elements.


Protect source code

JSF2.2 context parameter WEB_APP_RESOURCES_DIRECTORY; <url.pattern> in web.xml


3rd party & integration
	jsf was built to be a platform supporting the enhancement of core functionality.
	in the beginning it was very bare bones.
	then there was primefaces.

	Spring integration via SPringBeanFacesELResolver (helps JSF runtime resolve springbeans using EL) & ContextLoaderListener (starts up spring context)
	"joinfaces" for springboot
	primfaces is most popular jsf lib
	bootsfaces (brings bootstrap flare to JSF)
	adminfaces builds on primefaces (responsive & mobile friendly)
	angularfaces for angularjs
	omnifaces (tools & utilities)

some next things

h:outputScript, h:outputStylesheet, f:view, h:graphicImage, f:event


navigation

forward - internal page forward, browser's URL is not updated
	<action="somePage">
redirect - browser's URL is being updated
	<action="somePage?faces-redirect=true">

navigation rules

centralize navigations. also specify them as redirect or not.

<navigation-rule>
	<from-view-id>start.xhtml</from-view-id>
	<navigation-case>
		<from-outcome>page1</from-outcome>
		<to-view-id>page1.xhtml</to-view-id>
		<redirect />
	</navigation-case>
</navigation-rule>


binding

With binding attribute you are mapping the actual component and NOT the component's value.
when bound,
the backing bean can programmatically modify component attributes.
the backing bean can instantiate components rather than let the page author do so.

