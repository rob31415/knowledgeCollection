annotations to secure java ee applications (IBM)


intro
	Spring security is as broken up in sub-projects as e.g. JPA is. 
	That's just a Spring thing.
	You need detailled knowlegte of all them to get around.

	most common
		spring-security-core (for authN and authZ)
		spring-security-config (for xml or javaconfig)
		spring-security-web (for servlet API)
		spring-security-test

	more specific commonly used
		spring-security-ldap
		spring-security-oauth2-core
		spring-security-oauth2-client
		spring-security-openid

	less commonly used (more for specific use cases)
		spring-security-oauth2-jose (jot, jws, jwk, jwe)
		spring-security-remoting (remote client invocation)
		spring-security-cas (single sign on)
		spring-security-acl (domain object based security operations)

authentication (authN)

	principal (user or machine) is who they say they are. 

	naive
		via http GET
	http basic auth
		uses base64 for usename & pw
		can't log off, no log off functionality at all
	digest auth
		uses MD5, it's broken
	https
		= http+tls
		provides:
			confidentiality - when I enter my password, no one else can see it. when my bank tells me account details, no one else can see that.
			integrity - what I get (.js, .css) is trustworthy and comes untampered
				one could put a link to mybank.org with valid certificates via https in there
		has vulns, they get fixed, we move on, it's the best we can do with this tec
		has to be set up correct
			there are online services checking your website if TLS is set up ok
		only slow during handshake (because it uses asym)
			after that there's a switch to sync
		tls can be optimized - istlsfastyet.com
	http basic auth via https?
		use sha instead of md5...
		but rainbow table (a lookup containing lots of passwords and pre calculated hashes for them - space/time tradoff)...
		then use salt (prefix/suffix before/after hashing)
		but hashcat (ashley madison, July 2015)
			hashkiller-dict
			sha512 5.2 billion hashes/sec
		then use one-way adaptive function (fast in one way, very slow in reverse way)
			pbkdf2, bcrypt, scrypt
			"adaptive" = about 0.5 seconds on targed hardware
			you want ~ a few hundred hashes / sec
		so, http basic via https is at least .5 seconds per request - and the plus the rest of the request
			might be acceptable, but might not
		then cache it (in a session)
		but then there's state
		so, use tokens
		but tokens needs to be secure
		so encrypt it (aes_cbc)
		but this gives just confidentiality, no integrity; encrypted token can be changed
			username=someguy&name=SOme+Name => username=admin&name=SOme+Name
		ok, then use a standard, e.g. JWT
			JWT = representing tokens using JSON stateless, encrypt, sign etc.
		but you're doing sth before validating token:
			the header specifies an algo, then you're supposed to take data and validate against algo(your data)
			there's also: "alg":"none" and >50% of implementations do this
	x509

	forms based
		needs TLS
		provides "remember me" option

	ldap

	active directory

	OpenID

	Jasig CAS

	JAAS

	Kerberos

	SAML

authorization (authZ)

	access control; what a principal can and can not do

	web request

	method invocation

	domain object instance access control
