# sankey

## 1. define nodes

    nodes = [0 sockeye released, 1 re-cap, 2 not recap']

### data visualised as table

    label src           label target    val  src-idx target-idx
    sockeye released    re-captured    5000    0       1
    sockeye released    not recap'    45000    0       2

note: idx refers to node array

## 2. define links

    links = [ src-idx[], target-idx[], val[] ]

## 3. draw stuff

give the lib the **nodes** and **links** and it should be able to display this:

    ~50k      5k
    |sock     --->   |re-cap
    |released
    |         45k
    |         --->   |not-recap

