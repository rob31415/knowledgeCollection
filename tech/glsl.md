# debugging

no printf :-(

# GLSL primitive types

int, float, double, uint, bool, vector, matrix

# shader pipeline (shader stages)

vertex -> geometry -> tesselation -> fragment

compute (not part of pipeline)

## "invocation"

- execution of a shader stage
- only compute shader invocations can communicate w/ another
  - think of it like static inside a C function

# transfer data

## data types

uniform (per-primitive; constant during an entire draw call)
uniform buffer
attribute (per-vertex; positions, normals, colors, UVs, ...)
varying (per-fragment/pixel)
samplers
shader storage buffers (VBO, VAO)
Shader Storage Buffer Object

## direction

- out in inout
- transfer data to next shader: declare output and input w/ equal type and name

## OpenGL or WebGL application to vertex shader
  glVertexAttribPointer specifying attributes and locations
  stride says how many bites to the next point

  define meaning of raw data (specify vertices):
    vertex buffer object (VBO): [x,y,z,r,g,b,  x,y,z,r,g,b, ...]
    vertex array object (VAO): {Attrib1: 3 floats (pos); Attrib2: 3 floats (color)}

# compute shader

### "workgroup" 

- a collection of shader invocations that execute the same code, potentially in parallel
- like "small computers", aligned in a 3D array
- every small computer is pure

While the individual shader invocations within a work group are executed as a unit, work groups are executed completely independently and in unspeciﬁed order.

### inputs

- 5 inputs: numberOfWorkgroups, this workgroup id, local invocation id (within workgroup), global invocation id (within chunk), local invocation index (within workgroup)
  - IDs are 3d vectors, index scalar

### communication between invocations

- share keyword: share data (a variable) with other members of the same workgroup
- use barrieres to sync.
- use atomic operations (add, comp&swap, etc.)


# SHADERed

  #version 440

  uniform mat4 matVP;
  uniform mat4 matGeo;

  layout (location = 0) in vec3 pos;
  layout (location = 1) in vec3 normal;

  out vec4 color;
  uniform float time;

  void main() {
    color = vec4(normal, 1.0);
    gl_Position = matVP * matGeo * vec4(pos+sin(time*10)/2, 1);
  }


  #version 440

  in vec4 color;
  out vec4 outColor;
  uniform float time;

  void main() {
    outColor = vec4(.5, 0.+abs(sin(time*4.5)), 0.9, 1.0);  // /abs(sin(time/0.5));
  }