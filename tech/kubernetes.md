
# words...

> "When I use a word," Humpty Dumpty said in rather a scornful tone, "it means just what I choose it to mean - neither more nor less."
Lewis Caroll

This section aims to explain the main concepts.

## node

- a machine in a cluster
- a cluster has 1 master node
    - should be replicated for redundancy
    - has API server, scheduler, controller (-manager), etcd
        - also cloud ctrl, addons, DNS
- most of them are worker nodes 
    - each wn has 2 processes: kubelet, kube-proxy

## cluster

- a bunch of nodes, and
- proxy
    - routes traffic
    - is present on every node
- dns
    - 1 (or many) in a cluster
- ui
    - some dashboard

### kinds of installation

- minikube: single node k8s cluster inside 1 VM
- docker desktop: single node k8s cluster within docker
- cloud
- on VMs, baremetal
- etc.

## manifests

- files that describe k8s objects
    - many things can be done w/ arguments to command-line tools
    - but putting it into a file has it's benefits, obviously
- typically yaml files are used in k8s
    - YAML is a superset of JSON. Any JSON file is also a valid YAML file
    - The Kubernetes API receives the resource in JSON even if you write YAML files. 
        - kubectl serialises the YAML into JSON

## mandatory content

    apiVersion:
    kind:
    metadata:
    spec:

- kind - what k8s object this file is about
- apiVersion - is specific to what object is being created
- metadata - e.g. name and labels
- spec - specifics of the object (of course specific to kind)
    - kind=Pod can have "template" sections to describe containers

## pod

- smallest deployable unit
- set of 1-n containers
    - 1 container pro pod is pretty common; K8S manages pods, not containers
- a pod runs on 1 node !
- every pod has 1 IP
- shares networking & storage
- pod manifest = declarative text file ("kind: Pod")
- can be scaled vertically (multiplied)
- are ephemeral (might change IP), that's why there's services

### why Pods?

because port mapping:

- container - maps host-port to container
    - e.g. 2 containers have 2 different host ports
    - problem: keep track of free ports on host
- pod
    - is like a isolated v-host w/ it's own network namespace
    - so, e.g. 10 containers, 10 pods
    - all containers can mapping 8080:8080 no problems

also, k8s yams stays the same (talking about pods) when the container runtime is exchanged.

## service

- its a resource making a set of pods accessible for DNS address
- logical collection of pods
- load balances to a pod (or multiple if they're replicated)
- kubectl expose ("kubekatl")
    - makes an endpoint available via IP & name
    - can be exposed to outside of cluster (e.g. load balander or IP address)
        - good for dev, but insecure; ingress is better
- coupled through labels on pods
- service manifest ("Kind: Service")

### service discovery

- uses DNS (L4)
- name e.g. myService.default.svc.cluster.local
    - default = namespace, svc = service, cluster.local = base domain name

### headless service

- DNS access to pods within a stateful set

## replica set

- it's a process to monitor Pods
- user specifies the number of replicas wanted for each pod
- a cluster wide manager ("controller mgr") now makes sure: observed state == desired state
    - example: "i want 3 instances of pod X running"
    - you don't need to update a version in each pod manually, just edit it in 1 place
- it represents a microservice
- it's for stateless services
- it's able to just start managing already existing Pods - alternatively to creating them itself
- in manifest ("Kind: ReplicaSet")

### template section

- it's nested: ReplicationController is parent, child is a Pod def.
- ReplicaSet can manage any Pods, incl. those not defined here.


    apiVersion: apps/v1
    kind: ReplicaSet
    metadata:
        labels:
        type: sometype
    spec:
        template:
            metadata:           <---- inline pod definition
                labels:
                    type: front-end
            spec:
                containers:
                    ...

        selector:                   <---- which pods should this RepSet manage?
            matchLabels:
                type: front-end     <---- incidentally the one defined in here (see above)


### replication controller

- old version of replica set;
- differences: set requires "selector" section, in ctrl it's optional


### autoscale

## deployment

- = replica set + automatic rolling update
- doesn't interact with pods directly, it just does rolling update using ReplicaSets
- As long as you don't have a rollout in progress, a deployment will result in a single replicaset with the replication factor managed by the deployment
- manifest ("Kind: Deployment"), the rest is similar to replica set

### rolling update

Let there be ReplicaSet-A and it's pods should be updated to a newer version.
How to do this manually:

1. create Replicaset-B
2. scale down ReplicaSet-A 
3. scale up ReplicaSet-B 

do 2 and 3 repeatedly, one pod at a time.

That's done by K8s in the so called "control loop".

## replica set vs deployment

- rolling-update command is *imperative*, deployments are *declarative*
- the docs advise to prefer deployments

## daemonset (vs deployment)

- deployment says *how many replicas* of a pod it wants
    - controller mgr automatically makes it happen
- daemonsets however, puts one pod on *every* node
    - new node comes -> pod is put on it
    - good for log collection or monitoring, just stuff that each node should have

## stateful set (vs deployment)

deployment is for stateless pods. this is for stateful pods.

- manage stateful apps
    - guarantees ordering and uniqueness of pods
- maintains sticky identity for each pod
    - each pod gets a uid (predictable pod name)
- service names are like: mysql-0.svc, mysql-1.svc, mysql-2.svc (fixed individual DNS names)
- persistence via PersistentVolume
    - the volume is independent from the statefule pod
    - unique uid means, the volume gets reconnected if a pod is restarted
- good for databases

### operators

- stateful apps can't be automated generally
- managing stateful apps is tedious if done manually
- so, many apps/services bring specialised tasks/configs etc.
- an operator has basically it's own control loop
- operators are implemented by specialists of the particular apps
- there's an ecosystem of operators: operatorhub.io

### custom resource definition (CRD)

- default components: deployment, stateful set, etc..
- CRD: custom component (operator uses this)

## example illustration deployment and service

                   Node1    Node2

    deployment1    pod1     pod1     <-- service1
    deployment2             pod2     <-- service2
    deployment2    pod3              <-- service3

- *deployment* defines and controls which pods and on how many nodes the pods should be
- *service* defines access to a particular pod (1, 2 or 3)
    - a client says "let me talk to pod1" - client can't control on which node they land - that's what's meant by saying "a service is stateless"
- deployment can have different pods (type), service should have only one pod (type)
    - technically, a service can have different pods as well, but that's probably nonsense

### recap: service vs deployment

- deployment: keep a set of pods running
- service: enable network access to a set of pods
- https://matthewpalmer.net/kubernetes-app-developer/articles/service-kubernetes-example-tutorial.html

## configmap

- idea: separate config from app
- configmap stores config data of max. 1MiB unencrypted
- it's a API object that has to be created and can then be filled
    - from file contents 
    - env file
    - a directory
    - literally
- ConfigMaps must be mounted as directories not as files
    eg  mountPath: /etc/nginx/conf.d

### using a configmap in a pod definition

    apiVersion: v1
    kind: Pod
    metadata:
    name: basic-pod
    spec:
    containers:
        - name: basic
        image: nginx
        volumeMounts:
        - name: basic-config
            mountPath: /etc/nginx/conf.d
    volumes:
        - name: basic-config
        configMap:
            name: basic-config 


## namespaces

- organize objects in a cluster
- separate resource limits

    db-service.dev.svc.cluster.local        db-service.dev.svc.cluster.local
    service   |NS |ser-|   domain
    name      |   |vice|

- use "metadata: namespace: bla" in file or "create --namespace=bla" for kubectl

- kubectl create namespace bla
- or: "apiVersion:1 kind:Namespace metadata: name: bla"

### quota

  apiVersion: v1
  kind: ResourceQuota
  metadata:
    name: bla
    namespace: myNS
  spec:
    hard:
      pods: "10"
      requests.cpu: "4"
      requests.memory: 10Gi
      limits.cpu: "10"
      limits.memory: 20Gi
    scopeSelector:
      matchExpressions:
      - operator : In
        scopeName: PriorityClass
        values: ["medium"]


## ingress controller

- load balance traffic to services
- There is no standard Ingress controller that is built into Kubernetes
- envoy based (envoy is a load balancer)
    - ambassador, gloo, contour
- other
    - nginx, traefik, citrix, haproxy, kong

### api gateway vs service mesh

- api gw : external traffic to internal resources
- mesh : brokering internal resources

they overlap and the market is in motion...

so for instance, istio service mesh can be used as ingress

## labels & selectors

- used in many places in k8s
- labels are tags for objects
    - can be filtered
- selectors specify labels
    - e.g. some Pods have a label and a ReplicaSet's selector can specify this label
      for the RS to be able to manage the pods w/ the given label
- labels are of the format "someKey: someValue" eg. "type: back-end"

## context

    kubectl set-context $(kubectl config current-context) --namespace=MY-NS

## job

- creates pods and handles termination
- there's also CronJobs

## etcd

- service registry
- persistent storage
- key/value semantic
- just k/v store for configuration data, state data, and metadata

## standalone

### deployment-manifest
    - defines a pod by specifying it's containers (via image name)
    - gives the pod a label

### service-manifest
    - select pods via label

## single manifest

put all above in 1 file, separate by "---"

    apiVersion: apps/v1
    kind: Deployment
    metadata:
    name: nginx-depl
    spec:
    replicas: 1
    selector:
        matchLabels:
        name: nginx-pod
    template:
        metadata:
        labels:
            name: nginx-pod
        spec:
        containers:
        - image: nginx
            name: nginx-container
            ports:
            - containerPort: 80
    ---
    apiVersion: v1
    kind: Service
    metadata:
    name: ngnx-service
    spec:
    type: NodePort
    ports:
    - targetPort: 80
        port: 80
        nodePort: 30008
    selector:
        name: nginx-pod

then you can

    curl localhost:30008
    # clean up
    kubectl delete service ngnx-service
    kubectl delete deployments nginx-depl 


## helm charts

- package manager for k8s
- has repos

## examples

### Pod

pod with volume and configmap

    apiVersion: v1
    kind: Pod
    metadata:
    name: webserver
    spec:
    containers:
    - name: nginx
        image: TODO
        ports:
        - port: 80            # from inside the cluster (not neccessary to specify; as long as a pod is listening it'll work w/o this)
        - nodePort: 8080      # from outside the cluster, via IP
        - targetPort: 80      # where the app inside the pod actually listens to
        - containerPort: 80   # puerly for documentation (just like EXPOSE from docker)
        volumeMounts:
        - name: webserver-cfg
        mountPath: /etc/nginx/conf.d
        - name: webserver-data
        mountPath: /data
    volumes:
    - name: webserver-cfg
        #hostPath:
        #  path: /any/path/it/will/be/replaced
        configMap:
        name: webserver-configMap
    - name: webserver-data
        hostPath:
          path: /any/path/it/will/be/replaced

### Deployment

### Service

# service mesh

has 2 components: data plane, control plane.

- data plane: it's a proxy.
- control plane: setup and configure the proxies

## why?

- a microservice talks to localhost, where a proxy listens
    - it says "i want to talk to "microservice C"
- that proxy knows the IP of "microservice C", even when it changes
- it's like a DNS, but easily programmable which is instantaniously updated

so, outbound goes via proxy, inbound directly to microservice itself (not via proxy)

## proxies can do cool things

- 1 stable uri for each service
- service discovery
- logging
- monitoring latency
- routing (A/B testing)
- circuit breaking
- protocol translation (e.g. rest to kafka)
- ssl termination (SSL termination or SSL offloading decrypts and verifies data on the load balancer instead of the application server.)

## roi

"operational transparency"

# hands on


## kubectl

    list clusters (also which one is currently active; active meaning commands apply to this cluster)

    cluster-info
    cluster-info dump
    cluster-info dump|grep CIDR

    get all                             list all objects in cluster

    get nodes                           list nodes of current active/selected cluster
    get nodes -o wide
    describe nodes                      good to start investigating

    get svc                             list services

    get pods --all-namespaces
    describe pods --all-namespaces      good to start investigating as well
    kubectl describe pods MY-POD-NAME   find out what's the problem w/ a container inside a pod
    delete pods <pod>

    get pod <pod-name> -o yaml > pod-definition.yaml    get yaml def.
    edit pod <pod-name>


    run MYNAME --image=SOMEIMAGE         create pod

    create  -f manifest.yml              create a deployment or service (imperative mgmnt; if resource exists, error)
    apply   -f manifest.yml              if resource exists, it's modified; no error (declarative mgmnt)
    replace -f manifest.yml
    delete  -f manifest.yml              delete

    if you use "apply" where you should use "replace", you get:
        Warning: resource pods/redis is missing the kubectl.kubernetes.io/last-applied-configuration annotation which is required by kubectl apply. 
        kubectl apply should only be used on resources created declaratively by either kubectl create --save-config or kubectl apply.
        The missing annotation will be patched automatically.

    get componentstatuses

    logs <myPodName>
    # specify namespace explicitly
    kubectl -n kube-flannel logs kube-flannel-ds-m8wwg

    # see live what's happening in the cluster. also good for investigating.
    watch -c kubectl get pods --all-namespaces -o wide

    # restart pods of a deployment
    kubectl rollout restart -n kube-system deployment/coredns

    kubectl -n kube-system get deployments

    # configmaps
    kubectl -n kube-system get configmap coredns -oyaml|less
    kubectl -n kube-system edit configmap coredns

    you can often do:
    -o json
    -o name
    -o wide
    -o yaml

    --dry-run=client -o yaml        use this in the certification to generate a resource definition file fast


### ??

kubectl -n kube-system get deployment coredns -o yaml | sed 's/allowPrivilegeEscalation: false/allowPrivilegeEscalation: true/g' | kubectl apply -f -

### taint

modify node affinity by allowing a node to repel a set of pods.

    kubectl taint nodes node1 key1=value1:NoSchedule

# components of a k8s system and their interactions

- some system-critical components run natively, some in containers.
- the system-critical containers' manifests are called "static manifests" in /etc/kubernetes/manifests
    - API server exposes those manifests read-only (for use w/ kubectl on any node)
- /etc/kubernetes/bootstrap-kubelet.conf

## API server

- runs in a container
- all components talk to this
- human users use kubectl to upload specs to it
- auth/authz
- the only guy that should talk to etcd (a lot)
- heart of the system

## etcd

- runs in a container
- db for k8s; distributed KV store
- consenus algo called "raft"
    - quorum = more then 1/2 of nodes agree (uneven number of nodes)
    - qorum lost -> consensus lost

## scheduler

- runs in a container
- assigns pods to nodes (by talking to API server)

## controller-manager (for workloads and networking)

- runs in a container
- runs control loops to manage resources
- manages service to endpoint mappings (the "endpoints-controller" does that)
    - works together w/ kube-proxy to do that

## kubelet (for workloads)

- node agent on every node.
- just another controller
- runs native /usr/bin/kubelet
- watches API server for changes
- instructs container-runtime (via CRI)
- /var/lib/kubelet/config.yaml
- /etc/kubernetes/kubelet.conf 

## kube-proxy (for networking)

- a container
- talks to apiserver
- runs on every node
- implements services
- kubectl -n kube-system edit daemonset kube-proxy
    - /var/lib/kube-proxy/config.conf
- modifies iptables (talking to apiserver, which talks with controller-manager)
    - so the pod can reach another through a services IP

## DNS (core-dns)

- provides naming and discovery for the services

## CRI

- container runtime interface
- installed natively on node
- implemented by docker, containerd, cryo

### interact with crictl

    crictl images list
    crictl pods
    crictl ps --all
    crictl logs :containerId:
    journalctl
    less /var/log/messages
    crictl exec -i -t CONTAINERID /bin/sh
    crictl --runtime-endpoint unix:///run/containerd/containerd.sock ...

#### start a pod manually

    root@master:~# cat con.json 
    {
    "metadata": {
        "name": "bla"
    },
    "image":{
        "image": "aebe758cef4cd"
    },
    "command": [
        ""
    ],
    "linux": {
    }
    }
    root@master:~# cat sb.json 
    {
        "metadata": {
            "name": "bla",
            "namespace": "default",
            "attempt": 1,
            "uid": "abc123"
        },
        "linux": {
        }
    }
    crictl run --no-pull con.json sb.json

### interact with ctr

    ctr images ls
    ctr containers ls

# dependecies of k8s components

    etcd (static, container)
    apiserver (static, container) -> etcd
    kube-controller-manager (static, container) -> apiserver
    scheduler (static, container) -> apiserver
    kube-proxy (container) -> apiserver
    kubelet (native) -> apiserver
    dns (container) -> CNI
    CRI (native)
    CNI (container)

# networking

## layers refresher

    5 app HTTP/FTP/SNMP/SMTP (messages)
    x presentation TELNET
    x session NetBIOS/RPC/PPTP
    4 transport UDP/TCP (segments)
    3 network IP/ICMP/IGMP/ARP/IPsec (datagrams)
    2 data frames/vLAN/switching/MAC (frames)
    1 physical (bits)

- 4-7: firewall/security, service discovery, monitoring, loadbalance, app delivery
- 2-3: routing,gateway,subnetting,dns

- note: layer 2 and 3 is relevant for CKA

- flannel is layer2 (vxlan), calico is layer 3 (IPinIP)

## devices

layer2: network adapter (MAC)
layer2: switch (MAC bridge) - MAC to hardware-port - within a subnet
layer3: router/defaut gateway - between subnets

bridge connects virtual ethernets (veth)

these hardware device exist as virtualized versions.

## types of networking in k8s

- node network (kubeproxy; via DHCP or static)
- pod network (each POD its own IP; come from network or from CNI provider's IP range)
- cluster network (used by services; cfg by APIserver & controller-manager)

k8s nw rules:

- every pod can commuicate with every other
- node agents can comm w/ all pods on their node
- no NAT

##  comm

containers in a POD have same IP.

PODs talk to other pods:

  - on same node: via bridge/iptables (L2 network).
  - on another node: via L3 overlay network


## host networking (for VM to VM comm)

kubeadm controlplane endpoint flag...?

## overlay vs underlay network

- underlay: 
    - L1
    - L2 (e.g. ethernet w/ vlan)
    - L3 (e.g. internet); classic hardware
- overlay: virtual network (encapsulation w/ e.g. vxlan)
- overlay creates easily a topology completely different than underlay nw
    - packet control in overlay is SW defined (SDN) in contrast to underlay (classically HW oriented)

## overlay network (for pod to pod comm)

- typically: calico, flannel, selenium
- eg flannel creates TUN devices
- core-dns won't start up before such a NW is installed
- each node has it's own CIDR IP range
- service has to have also a CIDR range

find out:

    kubeadm config print init-defaults

    networking:
        dnsDomain: cluster.local
        serviceSubnet: 10.96.0.0/12

### services IP

- virtual IP
- set in iptables of nodes by kube-proxy


# test cluster

    kubectl create deployment hello-world --image=gcr.io/google-samples/hello-app:1.0
    kubectl get deployments
    #kubectl scale --replicas=2 deployment/hello-world
    kubectl expose deployment hello-world --port=8080 --target-port=8080 --type=NodePort
    kubectl get services -o wide
    kubectl get pods -o wide    # note the IP
    curl IP:8080
    curl CLUSTERIP:8080

    kubectl exec -it hello-world-u89reofj -- sh

    kubectl delete deployment hello-world
    kubectl delete service hello-world
