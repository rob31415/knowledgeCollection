

module
	Angular has its own modularity system called NgModules.
	It's different from CommonJS (serverside), AMD (async) and ES2015 (aka ES6) modules.
	Provides a "compilation context".
	ngModel adds 2-way-binding functionality.
	Consists of a tree of components.
	Can export and import components to/from other modules.
	The modules of an app make up a tree.
	An app can have multiple modules but has 1 root module named "AppModule" in "app.module.ts" file.

															import { NgModule } from '@angular/core';
															import { BrowserModule } from '@angular/platform-browser';
															import { AppComponent } from './app.component';
															import { MediaItemComponent } from './media-item.component';

															@NgModule({
															  imports: [
															    BrowserModule
															  ],
															  declarations: [
															    AppComponent,
															    MediaItemComponent
															  ],
															  bootstrap: [
															    AppComponent
															  ]
															})
															export class AppModule {}


bootstrap (main.ts)
		
															import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
															import { AppModule } from './app/app.module';
															platformBrowserDynamic().bootstrapModule(AppModule);



Directives

	add control flow statements to HTM-language

	are put onto a "host element"

	structural directive: CUD of dom elements (ngFor, ngIf)
		ngIf
															<div *ngIf="someInstance.someProperty">
															is syntactic sugar, becomes:
															<ng-template [ngIf]="someInstance.someProperty"> only rendered when true </ng-template>

		ngFor
			uses "micro syntax", only limited capabilites. i.e.
															<some-tag *ngFor="let thing of things">
			when bound propertys change, DOM is being adapted


	attribute directive: mods behaviour or appearence of dom elements
		can't create/mod DOM elements

		e.g. put some CSS class on an element
															<some-thing [ngClass]="{ 'some-tag': someComp.someProp==='green' }" >


	custom attribute directive:

															import { Directive, HostBinding, HostListener, Input } from '@angular/core';
															@Directive({  selector: '[someSelector]'  })
															export class SomeDirective {
															  @HostBinding('class.is-someBool') isSomeBool = true;
															}

															<some-element someSelector>
	
	give values to directives:
															@Input() set mwFavorite(value) { this.isFavorite = value; }

															<some-elemtnt [mvFavorite]="true">

	events in directives:
															export class FavoriteDirective {
															  @HostBinding('class.is-favorite-hovering') hovering = false;
															  @HostListener('mouseenter') onMouseEnter() {this.hovering = true;}
															  @HostListener('mouseleave') onMouseLeave() {this.hovering = false;}
															}


Components

	component = HTML template + Typescript class + CSS selector + Optionally, CSS styles
		?in which files goes what?
	
	is a directive

	uses services

	has a lifecycle:
		?which stages?
		there are hooks (ngOnInit() etc)

	is a view
		is a set of screen elements that Angular can modify
		View encapsulation: shadow-DOM, emulated or none
		note: shadow DOM introduces a "shadow boundary" - CSS styles and JS-Events don't cross this boundary.

																		import { Component } from '@angular/core';

																		@Component({
																		  selector: 'app-root',
																		  template: '<h1>My App</h1>'
																		})
																		export class AppComponent {name='Blabla'}


																		<body>
																		  <app-root></app-root>
																		</body>

	can be nested to form a "view hierarchy"
		can share data between parent/child

	can mix views defined in components that belong to different NgModules.

	getting data in and events out of a component

		input data
																		export class SomeParent { data = {id:1, name:"bla"}; }
																		<somechild [targetThing]="data" ></<somechild>

																		export class Somechild {@Input() targetThing;}
		output events (and data along with them)

																		export class Somechild {@Output() someEvent = new EventEmitter();}
																		<somechild (someEvent)="onSomeEvent($event)" ></somechild>

																		export class SomeParent { onSomeEvent() {...} }


Templates
	 ordinary HTML with Angular directives and binding markup that allow Angular to modify the HTML before rendering it.

	expression context
	template variable
	template expression operators
	conditional templating



pipes
	transforming values for display

  																		<div>Watched on {{ mediaItem.watchedOn | date: 'shortDate' }}</div>

  	create own

																		@Pipe({name:"smokeIt"})
																		export class somePipe implements PipeTransform {
																			transform(someObject) { return something; }
																		}


forms

	template driven

																		<form
																			#handleToForm="ngForm"
																		  (ngSubmit)="onSubmit(handleToForm.value)">
																			<select name="medium" id="medium" ngModel>
																		    <input type="text" name="name" id="name" ngModel>

																		export class MediaItemFormComponent implements OnInit {
																		  form: FormGroup;
																		  onSubmit(mediaItem) {console.log(mediaItem);}
																		}


	model driven (aka "reactive")
		can do more than template driven: field contract, validation, change tracking, unit testable


																		import { Component, OnInit } from '@angular/core';
																		import { FormGroup, FormControl, Validators } from '@angular/forms';

																		@Component({
																		  selector: 'mw-media-item-form',
																		  templateUrl: './media-item-form.component.html',
																		  styleUrls: ['./media-item-form.component.css']
																		})
																		export class MediaItemFormComponent implements OnInit {
																		  form: FormGroup;

																		  ngOnInit() {
																		    this.form = new FormGroup({
																		      medium: new FormControl('Movies'),
																		      name: new FormControl('', Validators.compose([
																		        Validators.required,
																		        Validators.pattern('[\\w\\-\\s\\/]+')	//built in validation
																		      ])),
																		      category: new FormControl(''),
																		      year: new FormControl('', this.yearValidator),	// custom validation
																		    });
																		  }

																		  yearValidator(control: FormControl) {
																		    if (control.value.trim().length === 0) {
																		      return null;
																		    }
																		    const year = parseInt(control.value, 10);
																		    const minYear = 1900;
																		    const maxYear = 2100;
																		    if (year >= minYear && year <= maxYear) {
																		      return null;
																		    } else {
																		      return { year: true };
																		    }
																		  }

																		  onSubmit(mediaItem) {
																		    console.log(mediaItem);
																		  }
																		}



																		<header>
																		  <h2>Add Media to Watch</h2>
																		</header>
																		<form
																		  [formGroup]="form"
																		  (ngSubmit)="onSubmit(form.value)">
																		  <ul>
																		    <li>
																		      <label for="medium">Medium</label>
																		      <select name="medium" id="medium" formControlName="medium">
																		        <option value="Movies">Movies</option>
																		        <option value="Series">Series</option>
																		      </select>
																		    </li>
																		    <li>
																		      <label for="name">Name</label>
																		      <input type="text" name="name" id="name" formControlName="name">
																		    </li>
																		    <li>
																		      <label for="category">Category</label>
																		      <select name="category" id="category" formControlName="category">
																		        <option value="Action">Action</option>
																		        <option value="Science Fiction">Science Fiction</option>
																		        <option value="Comedy">Comedy</option>
																		        <option value="Drama">Drama</option>
																		        <option value="Horror">Horror</option>
																		        <option value="Romance">Romance</option>
																		      </select>
																		    </li>
																		    <li>
																		      <label for="year">Year</label>
																		      <input type="text" name="year" id="year" maxlength="4" formControlName="year">
																		      <div *ngIf="form.get('year').errors as yearErors" class="error">
																		      	Must be between {{ yearErrors.year.min }} and {{ yearErrors.year.max }}
																		      </div>
																		    </li>
																		  </ul>
																		  <button type="submit" [disabled]="!form.valid">Save</button>
																		</form>


elements
	Angular elements: Angular components packaged as custom elements (aka Web Components)
	custom element: extends HTML by allowing tag definitions

	Transforming a component to a custom element makes all of the required Angular infrastructure available to the browser



decorators
	Modules, components and services are classes that use decorators

change-detection


Data binding

	2 way binding

	Event binding - catch user input and put it into business logic
															<a (click)="onDoSumtin"

															class MyComponent {
																onDoSumtin() {...}
															}
	put business logic data to html
		Property binding
															<h2 [textContent]='name'
		interpolation
															<h2> {{ name }}
															is tranlated to:
															<h2 textContent="{{name}}"

Dependency injection
	only singleton and constructor injection
	scope: component and all descendant components in the tree

	Services
		provide specific functionality not directly related to views.
		has metadata for DIing it into components

		injecting a type:

			declaring a service
															import { Injectable } from '@angular/core';
															@Injectable({
															  providedIn: 'root'	// better alternative to see *) below
															})
															export class SomeService {
															  someMember = [
															    {
															      id: 1,
															      name: 'bla'
															    }
															  ];
															  someMethod() {return 1;}
															}


															*)
															app.module.ts:
															@NgModule({
															  providers: [
															    SomeService
															  ]
															})
															export class AppModule {}

			using it

															export class SomeComponent implements OnInit {
															  constructor(private someService: SomeService) {}
															}

		injecing a value:
			@Inject

				declaring it

															const someValueTypeThing = { someContent: ['a', 'b'] };

															@NgModule({
															  providers: [
															    {provide: 'bla', useValue: someValueTypeThing}
															  ]
															})
				using it
															export class SomeComponent {
															  constructor(@Inject('bla') someValueTypeThing) {}
															}

			Injection token
				it's better to use export const someToken = new InjectionToken(); instead of a string literal


http
	https://www.positronx.io/angular-7-httpclient-http-service/

Data persistence(Viewed)

Routing
	The Router enables navigation by interpreting a browser URL as an instruction to change the view.
	navigate between application states and view hierarchies
	router maps URL-like paths to views instead of pages

	steps to do it
		setup routing

		setup routes

		<router-outlet>

	there's alternatives: https://medium.com/@timon.grassl/how-to-use-routing-in-angular-web-components-c6a76449cdb


best practice regarding modules

	one module per page / feature? / use case? (aka "feature module")
	one shared module: contains Components, directive, and pipes that are shared across modules.
		is imported by feature modules when they need to use shared components
	one core module: Singleton services and components that are only used once across the application
		is only imported by the root application module

	the router config associates feature modules to page routes
	this aids lazy loading
	https://www.c-sharpcorner.com/article/f/

some notes (jan 2021)

	european commission
		they use angular: https://ec.europa.eu/component-library/ec/resources/eui/

	releases every 6 months
	don't go from 7 to 10, go from version to version 

	organization: group by filetype vs group by feature

	npm: anyone can sign up & store own NPMs either privately or public
		npm init; npm publish
		npx -p @angular/cli ng new myapp  # installs cli temporarily, runs the command, removes it again :-/
