
# Brokers, Topics and Partitions

        ┌──────────────────────┐    ┌──────────────────────┐    ┌──────────────────────┐
        │     Broker 101       │    │     Broker 102       │    │     Broker 103       │
        │                      │    │                      │    │                      │
        │  ┌────────────────┐  │    │  ┌────────────────┐  │    │  ┌────────────────┐  │
        │  │    Topic A     │  │    │  │    Topic A     │  │    │  │    Topic A     │  │
        │  │                │  │    │  │                │  │    │  │                │  │
        │  │  Partition 1   │  │    │  │  Partition 2   │  │    │  │  Partition 3   │  │
        │  │                │  │    │  │                │  │    │  │                │  │
        │  │ ┌──┬──┬──┬──┐  │  │    │  │ ┌──┬──┬──┬──┐  │  │    │  │ ┌──┬──┬──┬──┐  │  │
        │  │ │M0│M1│M2│..│  │  │    │  │ │M0│M1│M2│..│  │  │    │  │ │M0│M1│M2│..│  │  │
        │  │ └──┴──┴──┴──┘  │  │    │  │ └──┴──┴──┴──┘  │  │    │  │ └──┴──┴──┴──┘  │  │
        │  │                │  │    │  │                │  │    │  │                │  │
        │  └────────────────┘  │    │  └────────────────┘  │    │  └────────────────┘  │
        │                      │    │                      │    │                      │
        │  ┌────────────────┐  │    │  ┌────────────────┐  │    │                      │
        │  │    Topic B     │  │    │  │    Topic B     │  │    │                      │
        │  │                │  │    │  │                │  │    │                      │
        │  │  Partition 1   │  │    │  │  Partition 2   │  │    │                      │
        │  │                │  │    │  │                │  │    │                      │
        │  │ ┌──┬──┬──┬──┐  │  │    │  │ ┌──┬──┬──┬──┐  │  │    │                      │
        │  │ │M0│M1│M2│..│  │  │    │  │ │M0│M1│M2│..│  │  │    │                      │
        │  │ └──┴──┴──┴──┘  │  │    │  │ └──┴──┴──┴──┘  │  │    │                      │
        │  │                │  │    │  │                │  │    │                      │
        │  └────────────────┘  │    │  └────────────────┘  │    │                      │
        │                      │    │                      │    │                      │
        └──────────────────────┘    └──────────────────────┘    └──────────────────────┘

## broker aka "bootstrap server"

- a node, a server
- each broker has some partitions of some topics
- good start number of brokers is 3
- metadata = info about all brokers, topics, partitions in the cluster
- metadata is shared amongst all brokers, supporting "broker discovery"
    - meaning: client can connect to any broker


### what happens when a broker goes away?

...

## topic

- a sequence of messages (aka stream of data)
- a topic is split into partitions (0-n)
- vaguely reminiscent to a table in a classical db
    - but topics can't be queried like in a db

## partition

- is a topic which is broken up
- is ordered
    - order is guaranteed within a partition, not across partitions
- "offset" = id of message within a partition
- data is immutable
- retention default is 1 Week
- topic replication factor
    - a partition is duplicated 2, 3, n times on another broker
    - "in-sync replica" vs "out of sync replica"
    - "topic durability": eg factor 3 means topic data can withstand 2 broker losses

## message

- [key, value, compression-method, header (key value pairs), partition+offet info, timestamp info]
- keys and values are serialized at producer, deserialized at consumer
    - can't change data type of key or value after creation - you have to create a new topic

# producers & consumers

- producer determines the partition of a message
    - this is in contrast to e.g. Elasticsearch, where the cluster (not the producer) decides where the data goes
    - done via hashing: partitionNr = Math.abs( murmur2(key) ) % (numPartitions-1)
    - so, messages with the same key always go to the same partition
    - no key, assignment of message to partition is round robin
    - "acks" - producer acknowledgements:
        - acks=0    producer doesn't wait for ack for successful write operation from broker
        - acks=1    producer waits for leader ack
        - acks=all  producer waits for ack from leader+replicas
- consumer
    - pull model
- both
    - know which partition to read/write and which broker has the partition (see "broker discovery" above)
    - auto recovery on broker failure

# consumer group


                           ┌──────────────┐    ┌──────────────┐    ┌──────────────┐              
              ┌────────────┤ Topic A      │    │ Topic A      │    │ Topic A      │              
              │            │ Partition 1  │    │ Partition 2  │    │ Partition 3  │              
              │            └──────────────┘    └───┬──────────┘    └──────┬───────┘              
              │             ┌──────────────────────┘                      │                      
              │             │  ┌──────────────────────────────────────────┘                      
              │             │  │                                                                 
        ┌─────┴─────────────┴──┴──┐     ┌─────────────────────────────────────┐   ┌─────────────┐
        │     ▼             ▼  ▼  │     │                                     │   │             │
        │ Consumer 1   Consumer 2 │     │ Consumer 1  Consumer 3   Consumer 3 │   │ Consumer 1  │
        │                         │     │                                     │   │             │
        │                         │     │                                     │   │             │
        │      Consumer-Group 1   │     │         Consumer-Group 2            │   │  Group 3    │
        │                         │     │                                     │   │             │
        └─────────────────────────┘     └─────────────────────────────────────┘   └─────────────┘


- Mapping for Consumer-Group 2 is: each consumer reads a different partition
    - so it's parallelized
- Mapping for Consumer-Group 3 is: this 1 guy reads all partitions
    - he has to do all the work on his own
- multiple consumer groups on 1 topic is ok
    - within a consumer-group, only 1 consumer is assigned to 1 partition

## rebalance

- when adding/removing consumers in a consumer group, the group gets "rebalanced", meaning the remaining consumers are assigned partitions again.
- eager strategy: all consumers stop and get randomly newly assigned
- cooperative rebalance (aka incremental rebalance): it's more intelligent
    partition.assignment.strategy; RangeAssignor, RoundRobin, StickyAssignor, CooperativeStickyAssignor
- static group membership is possible via "group.instance.id"
    - when leaving, get reassigned before session.timeout.ms without triggering rebalance

## __consumer_offsets

- stores offsets at which a consumer group has been reading
    - tells kafka "I have been reading so far by now"
- resumeability after downtime
- note: "__consumer_offsets" is a topic, it has dunderlines because kafka internal

### consumers choose when to commit offsets

assume: receive data -> process data

- at most once
    - immediately on recv
    - if processing goes wrong, messages can get lost
- at least once (default)
    - after processing
    - re-read after failure
    - you have to ensure idempotency regarding your processing
    - make sure messages are successfully processed before calling poll() again
- exactly once
    - only from kafka to kafka
    - kafka to external - idemopotent consumer

# leader election

- 1 leader for each partition
- producers can only send to leader, consumers read data from leader
    - alternatively reading from closest broker (so, possibly from a replica)
    - cloud/datacenter/edge blabla

# zookeper

- note: is going to be replaced (as of 05-2024)
    - kafka v 2.x works w/ zookeeper
    - kafka v 3.x can use "Kafka Raft" instead
    - kafka v 4.x will not have zookeper, KRaft only
- is a broker manager
- it's own cluster besides broker cluster
- does leader election amongs zookeper instances
- works on odd numbers of servers
- there's a leader (for writes) and followers (handle reads)
- isolated from consumer/producers - doesn't store offsets
- zookeper# doesnt have to be broker#
- note: always connect to brokers, never to zookeper!

# some practical stuff for practicing and familiarizing

- create a free kafka cluster on upstash.com
- create a debian buster VM with vagrant
- install kafka
    - https://idroot.us/install-apache-kafka-debian-10/
    - nano .bashrc  PATH="$PATH:/usr/local/kafka/bin"
- connect kafka CLI directly to upstash
    - each damn kafka CLI command is in an extra .sh file
        - kafka-topics.sh, kafka-console-producer.sh, etc..
    - nano /usr/local/kafka/config/upstash.properties 
        put stuff taken from upstash website into that file (user/pass etc.)

- for conduktor, after installing (locally, docker; like their website instructs)
    - "docker compose up"
- alternatively, connect CLI in the same way to conductor instead of directly to upstash


## do some stuff w/ CLI

kafka-topics.sh --command-config /usr/local/kafka/config/upstash.properties --bootstrap-server inspired-wren-10494-eu2-kafka.upstash.io:9092 --create --topic first_topic

kafka-console-producer.sh --producer.config /usr/local/kafka/config/upstash.properties --bootstrap-server inspired-wren-10494-eu2-kafka.upstash.io:9092 --topic first_topic

kafka-console-producer.sh --producer.config /usr/local/kafka/config/upstash.properties --bootstrap-server inspired-wren-10494-eu2-kafka.upstash.io:9092 --topic first_topic --property parse.key=true --property key.separator=:

**note: producing to a non existing topic - it's auto-created - except if that option is switched off.**


in one terminal:
kafka-console-consumer.sh --consumer.config /usr/local/kafka/config/upstash.properties --bootstrap-server inspired-wren-10494-eu2-kafka.upstash.io:9092 --topic second_topic
in another:
kafka-console-consumer.sh --consumer.config /usr/local/kafka/config/upstash.properties --bootstrap-server inspired-wren-10494-eu2-kafka.upstash.io:9092 --topic second_topic
in yet another:
kafka-console-producer.sh --producer.config /usr/local/kafka/config/upstash.properties --bootstrap-server inspired-wren-10494-eu2-kafka.upstash.io:9092 --producer-property partitioner.class=org.apache.kafka.clients.producer.RoundRobinPartitioner --topic third_topic

**note: they get the messages alternatively because of round-robin partitioner. batch partitioner is more efficient for larger and faster writes.**

**note: if no consumer exists, but production goes on, messages get stored. the first consumer gets all the messages directly upon connecting.**

**note: if consuming with "--from-beginning" in a *new consumer group*, all messages are read. Doing it a 2nd time in *the same consumer group* reads nothing anymore.**

**note: kafka-console-producer.sh w/o --group creates a temporary group**

you can list, describe and delete consumer groups.

        kafka-consumer-groups.sh --reset-offsets --to-earliest --topic third_topic --dry-run
        // will increase LAG; only works when no consumer is listening

## do some stuff w/ Java

- setup w/ new Properties()
- KafkaProducer<String,String>, ProducerRecord<>(topic,key,value)
- producer has a callback in which metadata can be checked (eg in which partition was the message written etc.)
- KafkaConsumer<>().subscribe(Arrays.asList("t1","t2"))
    - while(true) {ConsumerRecords r = consumer.poll(Duration.ofMillis(1000))}
### shutdown consumer gracefully

    final Thread mainThread = Thread.currentThread();

    Runtime.getRuntime().addShutdownHook(new Thread() {
        public void run() {
            consumer.wakeup()
            try {
                mainThread.join()
            } catch(InterruptedException e) {...}
        }
    })

    try {
        consumer.subscribe(...)
        while(true) {consumer.poll()}
    } catch(WakeupException e) {
        is expected.. log something for example
    } catch(Exception e) {
        ...
    } finally {
        consumer.close();       //also commit offsets
    }

