# webpack hands on

Please be sure to have read and understood "js-module-systems.md".

- webpack basically turns anything from .js, .html, .css, .whatever into components, which become referrable through module specifiers or paths.
- during this process, text might get interpreted and transformed (like, from typescript to javascript).
- "loaders" are the things doing the trafo

note: angular-cli uses webpack

## notes on npm

    npm init    # creates package.json
    npm install # get stuff described in package.json


note: anyone can sign up & store own NPMs either privately or public

    npm publish

warning: using npm as root leads to lots of senseless EACCESS errors (for cache directory etc.). all solutions on the interents don't help - so, the only way is to just not be root.

### semver

    major.feature.patch

major may break backward comp'

    ~1.0.0 pulls 1.0.* so only bugfixes
    ^1.0.0 pulls 1.*.* so also all minor (backward compatible, new features)

### lockfile

not using ^ and ~ in package.json pins only the immediate dependency, but not all further (sub-)dependencies down the dep-tree.

package-lock.json has the whole dep'tree and for each package it's exact version.

### npx

this installs cli temporarily, runs the command, removes it again

    npx -p @angular/cli ng new myapp

## create an initial package.json (npm)

    npm init
    npm install webpack --save-dev		# puts dep in package.json
    put into package.json

        "scripts" : {"build":"./node_modules/.bin/webpack -w"}	# w=watch

## create webpack.config.js

    const path = require("path");

    module.exports = {
        entry: "./src/index.js",
        output: {
            filename: "main.js",
            path: path.resolve(__dirname, "dist")
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                }
            ]
        }
    }


## loaders

for transforming files

    npm install babel-loader @babel/core --save-dev

### .babelrc config file

lists which presets we're using

    {
        "presets":["@babel/preset-react","@babel/preset-env"]
    }

## loading assets

### CSS

if there's a .css file which is imported in a .js file via "import 'thefile.css'"

    npm install style-loader css-loader --save-dev

add another rules in webpack.config.js

    {
        test: /\.css$/,
        use: {
            loader: 'style-loader',
            loader: 'css-loader'
            }
        }
    }

and lastly

    npm install css-loader style-loader -D

### image

    {
        test: /\.(png|jpg)$/,
        use: {
            loader: 'url-loader'
            }
        }
    }

don't forget to install the loader

    npm install url-loader -D


## dev server

add to webpack.config.js

    devServer : {
        contentBase: path.join(__dirname,"dist"),
        port: 9000
    }

add to package.json


    "scripts" : {
        ...
        "start:dev":"./node_modules/.bin/webpack-dev-server"
    }

then (that's autoreloading by dafault)

    npm run start:dev

## code splitting

seperate bundles for different pages

webpack.config.js

    entry:{page1:"p1.js", page2:"p2.js"},
    output{filename:"[name].bundle.js", ...}

## split chunks

webpack.config.js

    optimization:{
        splitChunks: {
            chunks:"all"
        }
    }

this creates one vendor bundle for shared (duplicated through import) code.

## html webpack plugin

    npm install html-webpack-plugin --save-dev

put "index.js" as single entry, "main.js" as single output in webpack.config.js

    const HtmlWebpackPlugin = require("html-webpack-plugin");

    ... plugins: [new HtmlWebpackPlugin()] ...

## prepare for prod

    npm install uglifyjs-webpack-plugin --save-dev

webpack.config.js

    const ujp = require("uglify-webpack-plugin");
    optimization:{
        minimizer: [new ujp()]
    }

# example

## given

a .html and a .js in src/

## wanted

something serveable in dist/

    dist/index.html
    dist/compiled.js

## solution

    npm install --save-dev http-server webpack copy-webpack-plugin

## package.json

    {
        "name": "first",
        "version": "1.0.0",
        "description": "",
        "main": "index.js",
        "scripts": {
            "build": "webpack --progress"
            "serve": "http-server dist/ -a 0.0.0.0 -p 9000",
        },
        "author": "",
        "license": "ISC",
        "dependencies": {
            "http-server": "^14.1.0"
        },
        "devDependencies": {
            "copy-webpack-plugin": "^10.2.4",
            "webpack": "^5.70.0",
            "webpack-cli": "^4.9.2"
        }
    }

## webpack.config.js

    const path = require("path")
    const CopyWebpackPlugin = require('copy-webpack-plugin')

    module.exports = {
        entry: "./src/index.js",
        output: {
            filename: "compiled.js",
            path: path.resolve(__dirname, "dist")
        },
        plugins: [
            new CopyWebpackPlugin({
                patterns: [
                    { from: "*.html", to: ".", context: "src/", }
                ]
            })
        ],
        optimization: {
            minimize: false
        }
    }


### note about copy-webpack-plugin

it doesn't behave like "cp".

i.e. when you do

    { from: "src/*.html", to: "dist/" }

you'll end up with 

    dist/
        dist/
            src/
                index.html

because dist/ is automatically prepended and not using that weird "context" thing makes it copy the whole path to "to".

### note about minimize

as default stuff gets minimized and thus unreadable by humans.

during dev it's better to switch this off - why? - faster compilation, you can read code in dev-tools, don't need to setup mapfiles.