
# frameworks/libs

- backend frameworks/libs: PHP, django, rails, nodejs
- frontend frameworks/libs: html5, Aurelia, VueJS, Knockout, Ember, Polymer, React/Flux, jQuery, Angular
- full stack: Meteor, electron
- sane stack: sailsjs, a?, nodejs, emberjs
- mean stack: mongodb, expressjs, angularjs, nodejs

# criteria for selecting a framework

- community/future
- size
- repaint speed
- SOC
- licencing
- real world usage
- what's the *unique selling point* of a particular framework?

# common features

what are common features of web frameworks / what do they actually do?

- for single page/spa
- mvc
- routing
- orm
- form validation
- pull-based/component based
- push-based
- templating
- caching
- sign up/on
- authentication
- authorization
- scaffolding
- url mapping
- i18n
- unit testing
- webcomponent compatible
- ecma version/js-dialect
- data binding & update method
- create desktop appa

# js libs

jquery, sass, corejs, lodash, handlebars, systemjs

# langs

that are used for webapps

- ecmascript 6/2015 
- ecmascript 7/2016
- typescript
- swift
- java 8
- php
- go
- elixir (dynamo)
- rust
- smalltalk

# tooling

npm = package mgr
yarn = package mgr
yeoman = yo (scaffolding) + grunt (task runner) + bower (front ent package mgr & dependency mgmnt)
gulp = build system (create build pipelines)
babel/traceur = transcompiler: compiles ES.Next to currently browser compatible js
webpack = static module bundler
bootstrap = pre-defined css classes that can be used on elements to make them responsive; also widgets
angular material 2
prime ng
cli (scaffold, generate component, service, module, serving, bundling, generate unit tests & test runners)
Protractor (automate ui; for angular)
karma (automate angular services)
cucumber
selenium (automate ui; many lang bindings)
jasmine
