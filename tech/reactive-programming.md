

# meta

There's a bunch of areas of conflict to be aware of what they mean in this context:

- event driven vs reactive
- backend vs frontend
- push vs pull
- sync vs async

This document is supposed to explain the differences.

# event driven vs reactive

The main difference between Event-Driven programming and Reactive Programming is the trigger of the action.

While event-driven focuses on handling an event (such as a button click) to trigger the corresponding action, reactive wraps data into the reactive system as events.

Both help to structure an application's logic, but in a different way:
- The application of the **event driven** paradigm is utilized mostly **to manage state** - most prominently in UIs.
- "**reactive**" has a different purpose - it's about **dealing with asyncronicity/latency using a stream model**.
	- JS promises for example also deal with asyncronicity, but don't employ streams to accomplish this

A reactive stream ...
- ... offers many **operations on the stream**
	- Search the internets for "reactive programming marble diagram" to see those operations in action.
- ... is observed, not modified
- ... **is a standard** for asynchronous stream processing with non-blocking backpressure
- ... is like a function pipeline + lazy evaluation
- ... promotes a shift from writing imperative logic to async, non-blocking, functional-style code.

## examples of operations

Creating,Transforming,Filtering,Combining,Error Handling,Observable Utility,Conditional and Boolean,Mathematical and Aggregate,Connectable Observable

# reactive ("RX")

- reactive systems are by nature distributed systems
- there's usually two areas to which "reactive" applies:
	- for back end
		- enables serving a large number of requests with only a small number of underlying worker threads
	- for front end
		- straightforward testing, a comprehensive flow of events, and time travel (event sourcing)

## properties

- elastic (loadbalance, devops, cloud, VM and stuff) 
	- predicitve and reactive scaling logs, which take/release resouces, according to workload.
- message-driven (loose coupling, async nonblocking messaging)
- responsive (doherty threshold 2.0. just be fast. plus, adapt visually to available screen-space)
- resilient (error handling)

# backpressure

Backpressure describes a situation, in which a producer generates faster than a consumer can absorb.

There's 4 possible ways of dealing w/ this situation:

- Ignore
	- feasible if that causes no problems
- Drop
	- sample a percentage of the incoming data
- Buffer
	- accumulate incoming data spikes temporarily
- Control the producer (Observable)
	- slow down/speed up is decided by consumer

# observable

- a source, emitting data endlessly
- pushes data when it's ready
	- vs iterators - they are pull
- can be sync or async
- it's observer-pattern + end-of-stream signal + error-propagation

## cold vs hot observable

- cold Observable emits a particular sequence of items, but can begin emitting this sequence when its observer finds it to be convenient, and at whatever rate the observer desires, without disrupting the integrity of the sequence.
- hot observable:  begins generating items to emit immediately when it is created. Subscribers typically begin observing the sequence of items emitted by a hot Observable from somewhere in the middle of the sequence, beginning with the first item emitted by the Observable subsequent to the establishment of the subscription. Such an Observable emits items at its own pace, and it is up to its observers to keep up.

When a cold Observable is multicast, it effectively becomes hot and for the purposes of backpressure and flow-control it should be treated as a hot Observable.

# notes on reactive backend techstacks (java and spring)

- Servlet API is blocking, Servlet 3.0 has async, Servlet 3.1 has non-blocking I/O.
- SPring MVC has "selective async HTTP request handling" and "annotation-based programming model".
- Spring reactive keeps the the latter while making the first not selective, but holistic.

- asynchronous non-blocking behavior in Java <= 8
	- big anon classes due to callback based API, Future.get() is async but blocks thread.
- Spring's ListenableFuture is non-blocking callback-based.
- Java 8: CompletionStage and CompleteableFuture are non blocking + Lambdas + chaining of result processing
	- but is push-based (?)
- Java 8: Stream is pull-based
	- but not been designed to deal with latency, such as I/O
- in a reactive stream, there's another channel for error, parallel to the data channel. In Java stream not.

## Reactive types in Spring Reactive:

- Mono = CompleteableFuture + reactive  (meaning: async & pull-based?, no callback?); observes to 0..1 Objects
- Flux: represents/observes zero to many objects. (0..N)
- Reactive Streams = 4 interfaces (Publisher, Subscriber, Subscription and Processor)

## handling backpressure

- Java9 has java.util.concurrent.Flow

## sync vs async

- In both the asynchronous and the synchronous consumer, you the programmer are responsible for orchestrating multiple calls.
- In the sync style, when you need to call multiple web services in succession, you need to write code that spends quite a bit of time waiting for each step. 
- In the async consumer, you don't spend as much time waiting for each service, but you're stuck writing orchestration code that must check when the future is available with data. 
- There are ways around this, using some callback functions, but that just leads to a lot of spaghetti code.
- This is the value proposition of reactive programming:
	- We get to write clean code, while optimizing the use of CPU time during orchestration of the flow between separate but immediate web service calls.

java

- In a reactive client, the data coming back from each of the web services will kick off the calls to the subsequent web service.
- In Java, somethin like that is done by chaining multiple instances of class "CompletionStage" (thenAcceptAsync(), resolveTemplate(), toCompletableFuture(), whenCompleteAsync()).
- Code is lambda-heavy

# timeline of front end architectures

The stuff here can be considered to be "event driven", it's not about "reactive".

## 1. GoF observer pattern

- can be used to implement MVC.
- drawback: tight coupling

## 2. callbacks

drawback: nested callbacks are incomprehensible for humans

## 3. chained promises (return antoher promise from inside a promise)

drawback: they mix event, callback, and promise - therefore incomprehensible for humans

## 4. flux

Flux is an "event driven" composable observer pattern.

e.g. model A listening to view A, model B listening to model A, model C listening to model B...

like choreography from SOA, just loosly coupled components with an event broadcast bus between them.

notorious drawback of choreography: you don't clearly see the higher level program flow from the code, just a bunch of actions and dispatches of actions.

## 5. redux

- redux = flux + state mgmnt
- event sourcing for the model
- used for React

## 6. 	Ngrx 

- it's the same as redux somehow
- tailored to/implemented for angular; can't be used elsewhere

# redux concepts

## state

- is not a state of a classical statemachine.
- is an immutable entity (interface/class/object) holding data regarding a certain concern, in a certain revision
- e.g. UsernameState has a list of usernames, which is initially empty (i.e revision 0.1).
- "changing state" means: creating a new state with different data (i.e revision 0.2)
- changing state only possible by dispatching actions via the store
- every piece of data in the app is represented by states
- single point of truth (SPOT)
- read only
- managed by the store

## action

- an event with payload data (or function or promise)

## reducer

- a pure function that gets called for every action that is dispatched
- implements what to do with the action's payload
- someReducer(state, action) {do some work with action.payload - mostly adding/removing from or changing a copy of current state's data; return new state with different data;}
- the observer from observer pattern
- synchronous state change

## store

- container for all states
- container for all reducers
- a deeply nested object, representing the entire state of a Redux application + subject from observer pattern (aka pubsub)
- an action can be "dispatched" on it
- notifies all observers/publishers when there was a "state change"
- uses RxJS (observables and operators)

## effect

- asynchronous state change
- triggered by an action, then start an async task, afterwards dispatch a new action


# ReactiveX

- Toolkit for the reactive programming model
	- shares ideas w/ flux
	- but **not to be confused** with event-driven style front end app architectures!
- An API for asynchronous programming with observable streams.
- There's libs for mostly all relevant ecosystems (Java, Microsoft, C++, Python, etc)
- for instance, **RxJS** is the javascript implementation.

## RxJs

- declarative dataflow with functional programming like composition (e.g. iteration factored out)
- Library for composing asynchronous and event-based programs by using observable sequences.
- Provides one core type, Observable.
- And satellite types (Observer, Scheduler, Subject)
- Using operators inspired by Array's higher order fcts (map, filter, etc)
- used in angular prominently; angular cli is a workaround for a bunch of boilerplate

# RxJs in detail

## store

	ng generate @ngrx/schematics:store MyStore --root --module app.module.ts

modifies: app.module.ts

	StoreModule.forRoot(reducers, config: {metaReducers})

creates: reducers/index.ts


		export interface MyStore {
			zipcodes: ZipcodeState,
			currentConditions: CurrentConditionsState
		}

		export const reducers: ActionReducerMap<MyStore> = {
			zipcodes: zipcodeReducer,
			currentConditions: currentConditionsReducer
		};

## reducer & state

	ng generate @ngrx/schematics:reducer ZipCodes --group

creates: something.reducer.ts

	export interface ZipcodeState {
		zipcodes: Array<string>
	}

	export const initialState: ZipcodeState = {
		zipcodes: []
	};

	export function zipcodeReducer(state = initialState, action: ZipcodeActions): ZipcodeState {
		switch (action.type) {
			case ZipcodeActionTypes.AddZipcode:
			return {...state, zipcodes: [...state.zipcodes, action.zipcode]};
			case ZipcodeActionTypes.RemoveZipcode:
				return {...state, zipcodes: state.zipcodes.filter( item => item !== action.zipcode)};
			default:
			return state;
		}
	}

## action

	ng generate @ngrx/schematics:action Zipcode --group

creates: zipcode.actions.ts

	export enum ZipcodeActionTypes {
		AddZipcode = '[Zipcode] Add Zipcode',
		RemoveZipcode = '[Zipcode] Remove Zipcode'
	}

	export class AddZipcode implements Action {
		readonly type = ZipcodeActionTypes.AddZipcode;
		constructor(public zipcode: string){}
	}

	export class RemoveZipcode implements Action {
		readonly type = ZipcodeActionTypes.RemoveZipcode;
		constructor(public zipcode: string){}
	}


	export type ZipcodeActions = AddZipcode | RemoveZipcode;

## dispatch an action

	<input type="text" #zipcode placeholder="Zipcode" class="form-control">
	<button class="btn btn-primary" (click)="addLocation(zipcode.value)" />

	export class ZipcodeEntryComponent {
		constructor(private store: Store<MyStore>) { }
		addLocation(zipcode : string){
			this.store.dispatch(new AddZipcode(zipcode));
		}
	}

### listen to state changes (updates)

	export class SomeComponent {
		zipcodes: Array<String>;
		constructor(private store: Store<MyStore>) {
			store.select(state => state.zipcodes)
				.subscribe(source => this.zipcodes = source.zipcodes);
		}
	}

select() is an RxJs Operator - it's the same as map() - so here, we just get zipcodes out of the state.


## effect

	ng generate @ngrx/schematics:effect CurrentConditions --module app.module --group

creates: 
