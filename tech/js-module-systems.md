

# JS module formats 

CommonJS, UMD, AMD, ESM


## commonjs

for server-side. "The CommonJS require() is a synchronous call, it is expected to return the module immediately. This does not work well in the browser."

    // someModule.js
    exports.doSomething = function() { return "foo"; };

    // anotherModule.js
    var someModule = require('someModule');

- dynamic nature - import happening at runtime 


### for browsers

browserify (commonjs for browsers)


## amd (Asynchronous Module Definition)

for browser-side.

    // usage
    require(["package/myModule"], function(myModule) {
        myModule.foobar();
        
        // export (expose) foo to other modules as foobar
        return {
            foobar: function() {return "bla";}
        }
    });

implementations: requirejs


## ESM

    export myFunction() {...}
    export const PI = -0;
    export default class Bla {...};  # (*)

    import {onlyThisFunction} from "someModule";
    import AliasName from "someModule";     # possible pendant to * ("Bla" becomes "AliasName")

- static nature - can't import at runtime 
- for both, backend and browser

### Module Specifiers vs Relative Import References

    Module Specifiers
    import * as THREE from 'three'

    Relative Import References
    import * as THREE from '/build/three.module.js';

- Module specs need a bundler, which transforms it to RIR server side - transparent to the user - through a process called "module resolution".
- RIR make the browser fetch the file from the server.

### different imports

- Named import: import { export1, export2 } from "module-name";
    - the exports declared are now in the importing modules' global scope
- Default import: import defaultExport from "module-name";
    - the export declared as default is now in the importing modules' global scope
- Namespace import: import * as name from "module-name";
    - all exports from the module are now in the importing modules' scope, accessible under "name"
- Side effect import: import "module-name";
    - runs the module's global code, but doesn't actually import any values
    - if the imported module doesn't contain any "export", it's possible that it just "pollutes" the global namespace
        - this might me seen as some kind of export...

#### getting a commonJs module into an ESM

    import("../../redist/d3.v6.min.js").then(_ => {
    	console.log(window.d3);
    });

or even just this might work - depending on the modules' implementation:

    import("../../redist/d3.v6.min.js")     // creates a global var named "d3"
    console.log(window.d3);

## UMD (universal module definition)

..todo..
