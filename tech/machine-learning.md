# intro

autor: rob

started: nov 21

this is an attempt of structuring what i've learned about ML.

Sources:
- Grokking Deep Learning
- Deep learning with python
- generative deep learning, teaching machines to paint, write, compose
- IX developer machine learning 2020
- linkedin courses
- https://www.kaggle.com/

# theory

## what's ML?

    |--------| deep learning
    |-----------| ML
    |----------------| AI

searching for useful representations / structure of some input data, within a predefined space of possibilities, using guidance from a feedback signal. This simple idea allows for solving a remarkably broad range of intellectual tasks, from speech recognition to autonomous car driving.

fitting a function to examples and use that function to generalize and make predictions about new examples.

## shallow vs deep

- shallow: 2 or 3 layers of artificial neurons.
- deep: multiple layers.

NOTE: an artificial neuron and a artificial neural network ***do not*** work like their biological counterparts! For instance, they're much faster and much less interconnected.

### greedy vs jointly

why not stack shallow networks?

that is called "greedy" and it's bad, because:
the optimal first representation layer in a three-layer model isn’t the optimal first layer in a one-layer or two-layer model.

in contrast, deep learning employs joint feature learning ("jointly") - meaning it trains all layers at the same time guided by just one feedback signal and it does so automatically. each layer is a relatively simple transformation of the one before and the representations get increasingly complex layer-by-layer. This and jointlyness make deep learning so successful.

## classical programming vs ML

    model->  |Classical  |
    data ->  |programming|   -> answers

    data    ->  |Machine |
    answers ->  |learning|   -> model

### algorithm vs model

algo: a formula (e.g. linear regression: y = mx + b)

model: the formula + concrete values for it's coefficients (here, m and b)

## what came before deep learning?

### naive bayes

### support vector machine (SVN)

SVMs aim at solving classification problems by finding good decision boundaries between two sets of points belonging to two different categories.
Such a boundary is called "hyperplane" - when in higher dim space that's fine - in 2D you might as well call it what it is: a line - one side are the one points, other side the others.
For "perceptual" problems, you have to manually find representations - this is called "feature engineering".

#### feature engineering

deep learning automates feature engineering

#### kernel trick

avoid calculating coordinates of points (in a new higher dimension space) by just calculating distance between pairs of points in that space.

vs

transform data to a higher dimension and use a hyperplane to seperate it into 2 groups. eg give 2D points a Z coordinate and lift the one you want above a threshold - that threshold is the hyperplane.

### decicion tree

leading datapoints through a tree of questions to arrive at a leaf.

#### random forest

- for shallow ML tasks

#### gradient boosting machine

- an alternative for deep learning
- good for non-perceptual problems

## ML categories

### supervised

- aka "narrow AI"
- for predictions
    - predict continuous value ("regression problem")
    - predict categorial value ("classification problem")

### unsupervised

- for clustering ("grouping")
- there's no training, no training data
- the network finds structures and groups data
- but it can't tell what groups these actually are
- Clustering:
    - Exclusive (partitioning)
    - Agglomerative
    - Overlapping
    - Probabilistic
- Clustering Types:
    - Hierarchical clustering
    - K-means clustering
    - Principal Component Analysis
    - Singular Value Decomposition
    - Independent Component Analysis

### parametric

- like "trial and error"
- the form of a function (and it's number of parameters) is chosen fix by a scientist.
- the coefficients are either learned (supervised) or ? (unsuperv.)
- there are groups, each has the same fixed in number (e.g. cols of a table) parameters ("dials")
- each datapoint is like a row in a table, it's columns' values go in one group, depending on the group's parameter-values (dial-setting)

### nonparametric

- like "counting"
- there are no fixed number of parameters (dials)
- number of parameters increases with datapoints

### supervised parametric

- find the right weights (i.e. like finding the right coefficients of a mathematical function)

### unsupervised parametric

- assign an input (datapoint) to one of a set of given groups

## bias/variance tradeoff

- low variance = more concentrated hits
- high variance = hits get scattered  (algo is fitting to noise)
- low bias = bullseye
- high bias = off center (algo is missing relevant feature/target output relations)

variance = algorithm's sensitivity to noise

bias = algorithm's tendency to consistently learn the wrong thing by not taking into account all info given in the data.

- complexity up = bias down & variance up
- complexity down = bias up & variance down
- error curve has a U (bathtub) shape

    error = bias + variance + irreproducible error

in the middle there's the sweet spot - the optimal model complexity. it's where bias and variance are lowest - and therefore error too.

### overfitting

- right part of the graph, where complexity is high
- the algo just memorizes examples, therefore data that deviates too much it can't predict well enough

### underfitting

- left part of the graph, where complexity is low
- the algo is too dumb to get the trend in the data

### detecting over/underfitting

this is done via train error and test error rates.

- when test error is low, train error is too and that's fine.
- when test error is high, train error is too, that indicates underfitting.
- when test error is high, train error is low, that indicates overfitting.

### amending overfitting

#### hyperparameter tuning

a model parameter (formula's coefficients) comes from within the data.
hyperparameter are external, are chosen by the scientist - it's values guide how the algo learns model parameters.

these are eg. in decicion trees cutoff points, tree depth, features to consider etc.

#### regularization

- eg ridge regression or lasso regression - penalty for loss function to constrain coefficients.
- dropout (some nodes are ignored during training)

## glossary

- "feature" a measurable property
- "target" what you want to predict
- "sample" a datapoint
- "loss function/value" diff between prediction and target
- "class" category in a classification problem (e.g. dog and cat)
- "label" instance of a class + sample (eg pic #123 is annotated w/ dog, dog is label of pic #123)
- "Ground-truth" or "annotations" all targets for a dataset
- "binary classification"
- "multiclass classification"
- "multilabel classification" each input sample can be assigned multiple labels
- "scalar regression" target is continuous
- "vector regression" target is set of continuous values

## math

### what is a tensor

in ML, it's an universal container for (mostly numerical) data.

in mathematics, it's a generalization of things like scalar, vector, matrix.

it consists of components and basis vectors. That makes it "portable", as in "the same for all observers", because values bring their reference (units) along with them.

    https://www.youtube.com/watch?v=f5liqUk0ZTw


#### scalar

- 0-dimensional tensor
- tensor has 0 "axes" = has a "rank" of 0

#### vector

1-dimensional tensor. it has 1 basis vector for each component.

basis vector = index (possibly nested)

e.g. 

    [1,2,3,4]

is a 4D vector and a 1D tensor.

#### matrix

is a "rank 2 tensor", having 2 basis vectors for each component.
e.g. 

    [[1,2],[3,4]]

Each component has 2 indices.

    Axx Axy Axz
    Ayx Ayy Ayz
    Azx Azy Azz

So the *position of a component* determines, to which basis vectors (axes/units) a value relates to.

##### feature matrix

rows = samples, cols = features

target is a vector

#### rank 3 tensor

    [  [[a,b],[c,d]],  [[e,f],[g,h]]  ]

With this, you can nicely describe forces inside a solid 3D object, where each of the three indices describes a force in XYZ direction at each point (=component) inside the object.

#### higher ranks

in deep learning, usually 0D to 4D tensors, 5D for video.

#### shape

(non mathematically/practically) describes how many dimensions the tensor has along each axis. just like arrays in Java (they're not matrices there, unlike in C++ for example...).

#### OPs on tensors

that's basically linear algebra (scalar product, cross product, system of linear equations, etc.)

##### add/mult

is done by a Basic Linear Algebra Subprograms (BLAS) implementation.
They're low-level, highly parallel, efficient tensor-manipulation (usually written in - you gessed it - C).

##### broadcast

##### tensor dot

the usual dot/scalar product (like vector*matrix)

##### reshaping

transpose etc.

### geometric interpretation of deep learning

neural networks consist entirely of chains of tensor operations and all of these tensor operations can be *thought of geometric transformations* of the input data.
It follows that you can interpret a neural network as a very complex geometric transformation in a high-dimensional space, implemented via a long series of simple steps.
Uncrumpling paper balls is what machine learning is about: finding neat representations for complex, highly folded data manifolds.

### mean and standard deviation

### activation function

##### Rectified Linear Unit ("relu")

set everything that's <0 to zero, leave everything else as is.

### logistic regression (logreg)

    y = 1 / (1+e^-(mx+b))

- statistical process to estimate relationship amongst variables
- target is binary
- linear not suitable for binary targets
- transparent through odd ratio
- no good for noisy/overcomplex data
- no good for continuous (not binary) target variable
- no good for massive data
- it's slower than the best

## FF
## CNN
## Backpropagation
## LSTM

# tools

## grundlagen

- python
    - NumPy, SciPy, Matplotlib and Pandas
- TensorFlow
- Keras

## einstieg rumspielen

- scikit-learn

## nlp (understanding/generation)

- spaCy
- Huggingface Transformers
- nltk

## visualisierung

- Plotly

## git für ML

- dvc
- mlflow
- Airflow

## data repos

https://opendata.stackexchange.com/questions/302/are-there-datasets-prepared-for-machine-learning

# use cases

what do people do with it, which tech is it?

## einsatzzwecke

Feature Engineering, Klassifikation, Regression und Clustering, Trend Detection, NLP

## Sequence generation

Given a picture, predict a caption describing it

supervised

## Syntax tree prediction

Given a sentence, predict its decomposition into a syntax tree.

supervised

## Object detection

Given a picture, draw a bounding box around certain objects inside the picture. 

supervised

## Image segmentation

Given a picture, draw a pixel-level mask on a specific object

supervised

## visualize

unsupervised

## denoise

unsupervised

## compress

unsupervised

## autoencode

selfsupervised

## play video game

reinforced

## speech recognition

## recommendation systems

## self driving cars

## uber driver/rider pairing
