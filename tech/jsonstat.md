# meta

understanding json-stat format.

# versions

json stat v2.0 from 2016

# users

lots. one of which is eurostat:

https://ec.europa.eu/eurostat/web/json-and-unicode-web-services/getting-started/rest-request

## size limitation

providers might do that. eg:

https://ec.europa.eu/eurostat/api/dissemination/statistics/1.0/data/MIGR_ACQ

    {"error":{
    "status": "413",
    "id": "413",
    "label": "EXTRACTION_TOO_BIG: The requested extraction is too big, estimated 8976732 rows, max authorised is 1000000, please add or change filters to reduce the extraction size."
    }}

# defs

    Dataset
        Dimension
            Category
    Data

- Dataset is - like an OLAP cube - a container for dimensions.
- Dimension consists of 1 .. N categories.
- Category is sometimes also referred to as "code".
    - It's the elements that make up a dimension.

If for a dim, the # of categories is 1 -> it's called a "constant".

- When dataset is displayed on a 2d device, usually a table is utilized.
- That means, fixing all dims but two.
- Fixing means: selecting 1 category (element) from all dimensions but two.
    - Fixing a constant: Trivial, there's nothing to choose from.
    - Fixing a dim with N elements: that's called "filtering". Usually, users get to select that visually.

For the 2 dims being displayed in a table, user usually gets to select what dim is col and what's row.

# json elements

## id

ordered list of dimension IDs

     "id" : ["metric", "time", "geo", "sex"]

## size

trivial.

    "size" : [1, 1, 1, 3]

## dimension

- contains all dimensions
- and for each
    - all their categories
    - order of the categories ("index")
    - label of the categories ("label")

### example

    "dimension": {
        "A": {
            "label": "My first dim",
            "category": {
                "index": {
                    "TOTAL": 0,
                    "STLS": 1,
                    "UNK": 2
                },
                "label": {
                    "TOTAL": "Total",
                    "STLS": "Stateless",
                    "UNK": "Unknown"
                }
            }
        },
        "B": {
            "label": "My second dim",
            "category": {
                "index": {
                    "X": 0,
                    "Y": 1
                },
                "label": {
                    "X": "Total",
                    "Y": "Stateless"
                }
            }
        },

        ...


## value

https://json-stat.org/format/#value


### flattening (sort semantic)

this are the rules, by which the data (which, in this case, is multi-dimensional in nature) is spaghettified / serialized / transformed into one long list of values (ie array):

- data sorted according to the dataset dimensions (see "id")
- row-major order (first go through the rows)
- start with the last dim first

#### example

(A and B with 3 and 2  categories respectively (6 elements in cube):

	A1B1 A1B2
	A2B1 A2B2
	A3B1 A3B2


## status

- contains metadata for value
- same size and "flattening" as value
- does not have a standard meaning nor a standard vocabulary

## extension

- allows JSON-stat to be extended for particular needs
