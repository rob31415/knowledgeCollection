# maven

## what for?

To include a lib in your build, you can do:

    javac -cp some-lib.jar mySourcefile.java

But dependecies...

## repo types

Maven has 3 types of repos

- Local repository (~/.m2)
- Central repository (https://repo.maven.apache.org/maven2/)
- Remote repository (you can set it up in your infrastructure i.e. company)

## archetypes

archetypes is like rubyonrails's scaffolding

    mvn archetype:generate > archetype-list.txt         # list archetypes (Ctrl-C it, because waits for user input)

    mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-webapp -DarchetypeVersion=1.4

some targets are:

    clean
    validate
    compile (into target/ directory)
    test
    package (make war, jar, whatever in target/)
    install (copy to local repo)
    deploy (copy to remote repo)

plugins may introduce targets.


## types of dependencies

###	direct

the ones explicitly specified in pom.xml like this:

    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
    </dependency>

### transitive

dependencies required by our direct dependencies.

    mvn dependency:tree     # see your projects deps

## scopes 

- can help to limit transitivity of the dependencies
- "compile" is the default scope when no other scope is provided.
- Dependencies are available on the classpath of the project in all build tasks and they're propagated to the dependent projects (modules).

### available scopes

    <scope>provided</scope>

dependencies that should be provided at runtime by JDK or a container

     <scope>runtime</scope>

The dependencies with this scope are required at runtime, but they're not needed for compilation of the project code. They are missing from the classpath.

     <scope>test</scope>
Test dependencies aren't transitive and are only present for test and execution classpaths, dont go into main project.

    <scope>system</scope>
        <systemPath>${project.basedir}/libs/custom-dependency-1.3.2.jar</systemPath>

points to jar file

    <scope>import</scope>

this dependency should be replaced with all effective dependencies declared in it's POM


## maven modules

###  inheritance

A Maven module is a sub-project. You can create a POM in the root dir, create a sub dir for each sub-project and have another POM inside them. Most elements from the parent POM are inherited by its children. 

### aggregation

If the root dir POM contains

    <module> ...

then it's a "super POM". Any commands issued on this are also issued to it's modules.

## profiles

- create customized build configurations
- specified using a subset of the elements available in the POM itself 
- modify the POM at build time

### definition

- Per Project: Defined in the POM itself (pom.xml).
- Per User: Defined in the Maven-settings (%USER_HOME%/.m2/settings.xml).
- Global: Defined in the global Maven-settings (${maven.home}/conf/settings.xml).

example

    <profile>
        <id>no-tests</id>
        <properties>
            <maven.test.skip>true</maven.test.skip>
        </properties>
    </profile>

### activate

profiles can be activated in lots of ways (based on default, some prop, java version, OS version, file, -P arg).

    mvn groupId:artifactId:goal -P profile-1,profile-2,?profile-3

### which profile is active?

    mvn help:active-profiles
