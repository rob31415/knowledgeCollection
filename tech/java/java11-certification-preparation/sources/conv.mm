<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Conv" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1090958577" CREATED="1409300609620" MODIFIED="1631608030304" VGAP_QUANTITY="3 pt" COLOR="#000000" BACKGROUND_COLOR="#ffffff"><hook NAME="MapStyle" background="#d6e8e8" zoom="0.826">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_4172522" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#051552" BACKGROUND_COLOR="#5cd5e8" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.7 px" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#1164b0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_4172522" STARTINCLINATION="116.25 pt;0 pt;" ENDINCLINATION="116.25 pt;28.5 pt;" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#051552" WIDTH="2" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#fff024" BACKGROUND_COLOR="#000000"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#f6f9a1" TEXT_ALIGN="LEFT">
<icon BUILTIN="clock2"/>
<font SIZE="10" ITALIC="true"/>
<edge COLOR="#000000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#ffffff" BACKGROUND_COLOR="#cc7212" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#1164b0"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_1823054225" COLOR="#fff024" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#9d5e19">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#9d5e19" TRANSPARENCY="255" DESTINATION="ID_1823054225"/>
<font SIZE="11" BOLD="true"/>
<edge COLOR="#9d5e19"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#053d8b" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#2c2b29" BORDER_DASH_LIKE_EDGE="true">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#ffffff" BACKGROUND_COLOR="#1164b0" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR="#2c2b29">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#ffffff" BACKGROUND_COLOR="#298bc8" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#ffffff" BACKGROUND_COLOR="#3fb7db" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#051552" BACKGROUND_COLOR="#5cd5e8" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<font BOLD="true"/>
<node TEXT="Arrays.asList(array)" POSITION="right" ID="ID_138964795" CREATED="1631480293843" MODIFIED="1631480638428" HGAP_QUANTITY="36.5 pt" VSHIFT_QUANTITY="-33.75 pt">
<node TEXT="modifiable" ID="ID_809086562" CREATED="1631480485367" MODIFIED="1631607986225" COLOR="#000000" BACKGROUND_COLOR="#ffffff"/>
<node TEXT="view" ID="ID_1973465663" CREATED="1631480508395" MODIFIED="1631608009681" COLOR="#ff0000" BACKGROUND_COLOR="#ffffff"/>
</node>
<node TEXT="*.toArray(opt. storageArray)" POSITION="left" ID="ID_1985918120" CREATED="1631480327440" MODIFIED="1631480635980" HGAP_QUANTITY="16.25 pt" VSHIFT_QUANTITY="-61.5 pt">
<node TEXT="List,Set,Deque,Vector..." ID="ID_285375087" CREATED="1631480400565" MODIFIED="1631607966313" COLOR="#000000" BACKGROUND_COLOR="#ffffff"/>
<node TEXT="modifiable" ID="ID_1828086775" CREATED="1631480472940" MODIFIED="1631607966314" COLOR="#000000" BACKGROUND_COLOR="#ffffff"/>
<node TEXT="view" ID="ID_1881464364" CREATED="1631480511413" MODIFIED="1631607974848" COLOR="#ff0033" BACKGROUND_COLOR="#ffffff"/>
</node>
<node TEXT="*.copyOf(orig)" POSITION="left" ID="ID_1767163441" CREATED="1631480352924" MODIFIED="1631480753003" HGAP_QUANTITY="37.25 pt" VSHIFT_QUANTITY="1.5 pt">
<node TEXT="Arrays,List,Set,Map" ID="ID_418493777" CREATED="1631480423305" MODIFIED="1631607966314" COLOR="#000000" BACKGROUND_COLOR="#ffffff"/>
<node TEXT="mofifiable" ID="ID_1408555732" CREATED="1631480933773" MODIFIED="1631607966315" COLOR="#000000" BACKGROUND_COLOR="#ffffff"/>
<node TEXT="copy" ID="ID_554026527" CREATED="1631480865791" MODIFIED="1631607974849" COLOR="#ff0033" BACKGROUND_COLOR="#ffffff"/>
</node>
<node TEXT="Collections.unmodifiable*(orig)" POSITION="right" ID="ID_647340764" CREATED="1631480370890" MODIFIED="1631480797680" HGAP_QUANTITY="23.75 pt" VSHIFT_QUANTITY="22.5 pt">
<node TEXT="List,Set,Map,etc." ID="ID_1790188616" CREATED="1631480434851" MODIFIED="1631608017905" COLOR="#000000" BACKGROUND_COLOR="#ffffff"/>
<node TEXT="view" ID="ID_1683463241" CREATED="1631480534298" MODIFIED="1631608009686" COLOR="#ff0000" BACKGROUND_COLOR="#ffffff"/>
</node>
<node TEXT="*.of(1..n)" POSITION="right" ID="ID_990909393" CREATED="1631480344385" MODIFIED="1631480628708" HGAP_QUANTITY="-74.5 pt" VSHIFT_QUANTITY="37.5 pt">
<node TEXT="dates,numbers,coll&apos;s" ID="ID_1976264330" CREATED="1631480412541" MODIFIED="1631608017910" COLOR="#000000" BACKGROUND_COLOR="#ffffff"/>
<node TEXT="init" ID="ID_1476651700" CREATED="1631480869114" MODIFIED="1631608009687" COLOR="#ff0000" BACKGROUND_COLOR="#ffffff"/>
</node>
</node>
</map>
