<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="StringBuilder" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1090958577" CREATED="1409300609620" MODIFIED="1632213828490">
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<font BOLD="true"/>
<hook NAME="MapStyle" background="#f9f9f8" zoom="0.685">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_506805493" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#484747" BACKGROUND_COLOR="#efefef" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#8fbcbb" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_506805493" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="11" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#2e3440" WIDTH="3" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#ffffff" BACKGROUND_COLOR="#2e3440" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#f0f0f0" BORDER_DASH_LIKE_EDGE="true">
<font SIZE="11" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#f6f9a1" TEXT_ALIGN="LEFT">
<icon BUILTIN="clock2"/>
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#eceff4" BACKGROUND_COLOR="#bf616a" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#bf616a"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_915433779" BORDER_COLOR="#bf616a">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#bf616a" TRANSPARENCY="255" DESTINATION="ID_915433779"/>
<font NAME="Ubuntu" SIZE="14"/>
<edge COLOR="#bf616a"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#484747" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#eceff4" BACKGROUND_COLOR="#d08770" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#3b4252" BACKGROUND_COLOR="#ebcb8b" MAX_WIDTH="50 cm">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#2e3440" BACKGROUND_COLOR="#a3be8c">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#2e3440" BACKGROUND_COLOR="#b48ead">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" COLOR="#e5e9f0" BACKGROUND_COLOR="#5e81ac">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BACKGROUND_COLOR="#81a1c1">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BACKGROUND_COLOR="#88c0d0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BACKGROUND_COLOR="#8fbcbb">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BACKGROUND_COLOR="#d8dee9">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BACKGROUND_COLOR="#e5e9f0">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="" POSITION="right" ID="ID_1768488427" CREATED="1632213762538" MODIFIED="1632213762539">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="get" POSITION="right" ID="ID_1349015785" CREATED="1632213302617" MODIFIED="1632213303405">
<node TEXT="CharSequence subSequence​(int start, int end)" ID="ID_1989110268" CREATED="1632213303843" MODIFIED="1632213303843"/>
<node TEXT="String substring​(int start)" ID="ID_906119002" CREATED="1632213303843" MODIFIED="1632213303843"/>
<node TEXT="String substring​(int start, int end)" ID="ID_1387713617" CREATED="1632213303844" MODIFIED="1632213303844"/>
</node>
<node TEXT="index" POSITION="right" ID="ID_1420500381" CREATED="1632213281798" MODIFIED="1632213282889">
<node TEXT="int indexOf​(String str)" ID="ID_498339945" CREATED="1632213283323" MODIFIED="1632213283323"/>
<node TEXT="int indexOf​(String str, int fromIndex)" ID="ID_410240586" CREATED="1632213283323" MODIFIED="1632213283323"/>
<node TEXT="int lastIndexOf​(String str)" ID="ID_228367789" CREATED="1632213283324" MODIFIED="1632213283324"/>
<node TEXT="int lastIndexOf​(String str, int fromIndex)" ID="ID_348143581" CREATED="1632213283325" MODIFIED="1632213283325"/>
</node>
<node TEXT="cons" POSITION="left" ID="ID_1372650501" CREATED="1632213192029" MODIFIED="1632213707748" HGAP_QUANTITY="27.5 pt" VSHIFT_QUANTITY="-20.25 pt">
<node TEXT="" ID="ID_1322445584" CREATED="1632213774229" MODIFIED="1632213774230">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="StringBuilder()" ID="ID_452381627" CREATED="1632213198256" MODIFIED="1632213198256"/>
<node TEXT="StringBuilder​(int capacity)" ID="ID_468717285" CREATED="1632213198256" MODIFIED="1632213198256"/>
<node TEXT="StringBuilder​(CharSequence seq)" ID="ID_1670807300" CREATED="1632213198259" MODIFIED="1632213198259"/>
<node TEXT="StringBuilder​(String str)" ID="ID_327232009" CREATED="1632213198260" MODIFIED="1632213198260"/>
<node TEXT="" ID="ID_948686288" CREATED="1632213774228" MODIFIED="1632213774229">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="C" ID="ID_1081261657" CREATED="1632213774230" MODIFIED="1632213806851"/>
</node>
</node>
<node TEXT="delete" POSITION="left" ID="ID_1291619421" CREATED="1632213265390" MODIFIED="1632213828488" HGAP_QUANTITY="137 pt" VSHIFT_QUANTITY="-3.75 pt">
<node TEXT="StringBuilder delete​(int start, int end)" ID="ID_886654169" CREATED="1632213267650" MODIFIED="1632213267650"/>
<node TEXT="StringBuilder deleteCharAt​(int index)" ID="ID_1195752797" CREATED="1632213267650" MODIFIED="1632213267650"/>
</node>
<node TEXT="" POSITION="left" ID="ID_1145458420" CREATED="1632213794441" MODIFIED="1632213794441">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="append" POSITION="left" ID="ID_626626648" CREATED="1632213208761" MODIFIED="1632213686900" HGAP_QUANTITY="15.5 pt" VSHIFT_QUANTITY="29.25 pt">
<node TEXT="StringBuilder append​(boolean b)" ID="ID_1890031977" CREATED="1632213211714" MODIFIED="1632213211714"/>
<node TEXT="StringBuilder append​(char c)" ID="ID_1819048462" CREATED="1632213211714" MODIFIED="1632213211714"/>
<node TEXT="StringBuilder append​(char[] str)" ID="ID_1775879048" CREATED="1632213211716" MODIFIED="1632213211716"/>
<node TEXT="StringBuilder append​(char[] str, int offset, int len)" ID="ID_1452760272" CREATED="1632213211716" MODIFIED="1632213211716"/>
<node TEXT="StringBuilder append​(double d)" ID="ID_713627994" CREATED="1632213211717" MODIFIED="1632213211717"/>
<node TEXT="StringBuilder append​(float f)" ID="ID_1055971992" CREATED="1632213211719" MODIFIED="1632213211719"/>
<node TEXT="StringBuilder append​(int i)" ID="ID_1648261304" CREATED="1632213211719" MODIFIED="1632213211719"/>
<node TEXT="StringBuilder append​(long lng)" ID="ID_1234959002" CREATED="1632213211719" MODIFIED="1632213211719"/>
<node TEXT="StringBuilder append​(CharSequence s)" ID="ID_860269078" CREATED="1632213211719" MODIFIED="1632213211719"/>
<node TEXT="StringBuilder append​(CharSequence s, int start, int end)" ID="ID_1753688840" CREATED="1632213211719" MODIFIED="1632213211719"/>
<node TEXT="StringBuilder append​(Object obj)" ID="ID_916422114" CREATED="1632213211719" MODIFIED="1632213211719"/>
<node TEXT="StringBuilder append​(String str)" ID="ID_1110757724" CREATED="1632213211720" MODIFIED="1632213211720"/>
<node TEXT="StringBuilder append​(StringBuffer sb)" ID="ID_1789172107" CREATED="1632213211720" MODIFIED="1632213211720"/>
<node TEXT="StringBuilder appendCodePoint​(int codePoint)" ID="ID_342834047" CREATED="1632213211721" MODIFIED="1632213211721"/>
</node>
<node TEXT="size" POSITION="right" ID="ID_1836481208" CREATED="1632213235941" MODIFIED="1632213237175">
<node TEXT="int capacity()" ID="ID_1470615765" CREATED="1632213237837" MODIFIED="1632213237837"/>
<node TEXT="void ensureCapacity​(int minimumCapacity)" ID="ID_469777330" CREATED="1632213237837" MODIFIED="1632213237837"/>
<node TEXT="int length()" ID="ID_59830965" CREATED="1632213237838" MODIFIED="1632213237838"/>
<node TEXT="void setLength​(int newLength)" ID="ID_1858404119" CREATED="1632213237838" MODIFIED="1632213237838"/>
</node>
<node TEXT="char" POSITION="left" ID="ID_1188423326" CREATED="1632213248060" MODIFIED="1632213249171">
<node TEXT="char charAt​(int index)" ID="ID_562007204" CREATED="1632213249693" MODIFIED="1632213249693"/>
<node TEXT="void getChars​(int srcBegin, int srcEnd, char[] dst, int dstBegin)" ID="ID_579908108" CREATED="1632213249693" MODIFIED="1632213249693"/>
<node TEXT="void setCharAt​(int index, char ch)" ID="ID_1590377448" CREATED="1632213249694" MODIFIED="1632213249694"/>
<node TEXT="IntStream chars()" ID="ID_186322278" CREATED="1632213249697" MODIFIED="1632213654860" BACKGROUND_COLOR="#99ff99"/>
</node>
<node TEXT="codepoint" POSITION="right" ID="ID_8426565" CREATED="1632213254773" MODIFIED="1632213256826">
<node TEXT="int codePointAt​(int index)" ID="ID_681245958" CREATED="1632213257261" MODIFIED="1632213257261"/>
<node TEXT="int codePointBefore​(int index)" ID="ID_1784516541" CREATED="1632213257261" MODIFIED="1632213257261"/>
<node TEXT="int codePointCount​(int beginIndex, int endIndex)" ID="ID_992246304" CREATED="1632213257262" MODIFIED="1632213257262"/>
<node TEXT="IntStream codePoints()" ID="ID_486492947" CREATED="1632213257262" MODIFIED="1632213664938" BACKGROUND_COLOR="#99ff99"/>
<node TEXT="int offsetByCodePoints​(int index, int codePointOffset)" ID="ID_1998461591" CREATED="1632213257263" MODIFIED="1632213257263"/>
</node>
<node TEXT="compare" POSITION="right" ID="ID_473301836" CREATED="1632213309673" MODIFIED="1632213310950">
<node TEXT="int compareTo​(StringBuilder another)" ID="ID_1351876241" CREATED="1632213311346" MODIFIED="1632213311346"/>
</node>
<node TEXT="" POSITION="right" ID="ID_1641857597" CREATED="1632213762536" MODIFIED="1632213762538">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="R" ID="ID_1038811410" CREATED="1632213762539" MODIFIED="1632213808525"/>
</node>
<node TEXT="insert" POSITION="right" ID="ID_692061982" CREATED="1632213274028" MODIFIED="1632213692908" HGAP_QUANTITY="17.75 pt" VSHIFT_QUANTITY="40.5 pt">
<node TEXT="" ID="ID_1965349325" CREATED="1632213803199" MODIFIED="1632213803200">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="StringBuilder insert​(int offset, char c)" ID="ID_1104677342" CREATED="1632213275668" MODIFIED="1632213275668"/>
<node TEXT="StringBuilder insert​(int offset, char[] str)" ID="ID_764847825" CREATED="1632213275668" MODIFIED="1632213275668"/>
<node TEXT="StringBuilder insert​(int index, char[] str, int offset, int len)" ID="ID_62046951" CREATED="1632213275669" MODIFIED="1632213275669"/>
<node TEXT="StringBuilder insert​(int offset, double d)" ID="ID_1358520035" CREATED="1632213275670" MODIFIED="1632213275670"/>
<node TEXT="StringBuilder insert​(int offset, float f)" ID="ID_1586395820" CREATED="1632213275672" MODIFIED="1632213275672"/>
<node TEXT="StringBuilder insert​(int offset, int i)" ID="ID_1831160418" CREATED="1632213275674" MODIFIED="1632213275674"/>
<node TEXT="StringBuilder insert​(int offset, long l)" ID="ID_1537245574" CREATED="1632213275674" MODIFIED="1632213275674"/>
<node TEXT="StringBuilder insert​(int dstOffset, CharSequence s)" ID="ID_970032486" CREATED="1632213275675" MODIFIED="1632213275675"/>
<node TEXT="StringBuilder insert​(int dstOffset, CharSequence s, int start, int end)" ID="ID_675345429" CREATED="1632213275675" MODIFIED="1632213275675"/>
<node TEXT="StringBuilder insert​(int offset, Object obj)" ID="ID_777888252" CREATED="1632213275677" MODIFIED="1632213275677"/>
<node TEXT="StringBuilder insert​(int offset, String str)" ID="ID_976510293" CREATED="1632213275679" MODIFIED="1632213275679"/>
<node TEXT="" ID="ID_607796636" CREATED="1632213803198" MODIFIED="1632213803199">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="U" ID="ID_974365927" CREATED="1632213803201" MODIFIED="1632213811387"/>
</node>
</node>
<node TEXT="mod" POSITION="left" ID="ID_581081633" CREATED="1632213292800" MODIFIED="1632213294063">
<node TEXT="StringBuilder replace​(int start, int end, String str)" ID="ID_1534437455" CREATED="1632213294491" MODIFIED="1632213294491"/>
<node TEXT="StringBuilder reverse()" ID="ID_125068944" CREATED="1632213294491" MODIFIED="1632213294491"/>
<node TEXT="void trimToSize()" ID="ID_1556321012" CREATED="1632213294494" MODIFIED="1632213294494"/>
</node>
<node TEXT="" POSITION="left" ID="ID_1121772296" CREATED="1632213794439" MODIFIED="1632213794440">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="U" ID="ID_641681148" CREATED="1632213794442" MODIFIED="1632213812498"/>
</node>
</node>
</map>
