<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Reader" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1090958577" CREATED="1409300609620" MODIFIED="1632141942525" VGAP_QUANTITY="3 pt">
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<font BOLD="true"/>
<hook NAME="MapStyle" background="#fdf6e3" zoom="0.685">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_1704360557" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#002b36" BACKGROUND_COLOR="#fdf6e3" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1704360557" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="11" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#93a1a1" WIDTH="3" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes" COLOR="#2e3440">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#f6f9a1" TEXT_ALIGN="LEFT">
<icon BUILTIN="clock2"/>
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#fdf6e3" BACKGROUND_COLOR="#d33682" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#d33682"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_1083719472" COLOR="#ffffff" BACKGROUND_COLOR="#bf616a" BORDER_COLOR="#bf616a">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#bf616a" TRANSPARENCY="255" DESTINATION="ID_1083719472"/>
<font SIZE="12" BOLD="true"/>
<edge COLOR="#bf616a"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#002b36" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_WIDTH="3.1 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#2c2b29" BORDER_DASH_LIKE_EDGE="true">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#fdf6e3" BACKGROUND_COLOR="#073642" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR="#2c2b29">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#fdf6e3" BACKGROUND_COLOR="#586e75" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" MAX_WIDTH="50 cm">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#fdf6e3" BACKGROUND_COLOR="#657b83" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#fdf6e3" BACKGROUND_COLOR="#839496" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" COLOR="#fdf6e3" BACKGROUND_COLOR="#93a1a1" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BACKGROUND_COLOR="#eee8d5" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BACKGROUND_COLOR="#eedfcc" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BACKGROUND_COLOR="#fdf6e3" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" BORDER_COLOR="#f0f0f0" BACKGROUND_COLOR="#fdf6e3">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="InputStream" POSITION="right" ID="ID_593636817" CREATED="1631458782812" MODIFIED="1632141942524" HGAP_QUANTITY="74 pt" VSHIFT_QUANTITY="168 pt">
<node TEXT="InputStreamReader​(InputStream in)" ID="ID_458799850" CREATED="1632141733360" MODIFIED="1632141733360"/>
<node TEXT="InputStreamReader​(InputStream in, String charsetName)" ID="ID_1177216631" CREATED="1632141733360" MODIFIED="1632141733360"/>
<node TEXT="InputStreamReader​(InputStream in, Charset cs)" ID="ID_66633271" CREATED="1632141733361" MODIFIED="1632141733361"/>
<node TEXT="InputStreamReader​(InputStream in, CharsetDecoder dec)" ID="ID_294097550" CREATED="1632141733362" MODIFIED="1632141733362"/>
<node TEXT="String getEncoding()" ID="ID_1412370548" CREATED="1632141733363" MODIFIED="1632141733363"/>
<node TEXT="int read()" ID="ID_878928234" CREATED="1632141733363" MODIFIED="1632141733363"/>
<node TEXT="int read​(char[] cbuf, int offset, int length)" ID="ID_712983672" CREATED="1632141733363" MODIFIED="1632141733363"/>
<node TEXT="boolean ready()" ID="ID_773072759" CREATED="1632141733363" MODIFIED="1632141733363"/>
</node>
<node TEXT="File" POSITION="left" ID="ID_898616337" CREATED="1631458828935" MODIFIED="1632141936964" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="-97.75 pt" VSHIFT_QUANTITY="-48 pt">
<node TEXT="FileReader​(File file)" ID="ID_1149395080" CREATED="1632141931964" MODIFIED="1632141931964"/>
<node TEXT="FileReader​(FileDescriptor fd)" ID="ID_1979985487" CREATED="1632141931964" MODIFIED="1632141931964"/>
<node TEXT="FileReader​(File file, Charset charset)" ID="ID_205544044" CREATED="1632141931966" MODIFIED="1632141931966"/>
<node TEXT="FileReader​(String fileName)" ID="ID_1532841618" CREATED="1632141931966" MODIFIED="1632141931966"/>
<node TEXT="FileReader​(String fileName, Charset charset)" ID="ID_1410804295" CREATED="1632141931967" MODIFIED="1632141931967"/>
</node>
<node TEXT="String" POSITION="left" ID="ID_1079245268" CREATED="1631458805641" MODIFIED="1631459574818" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="28.25 pt" VSHIFT_QUANTITY="-54.75 pt">
<node TEXT="StringReader​(String s)" ID="ID_850798741" CREATED="1632141813672" MODIFIED="1632141813672"/>
<node TEXT="void close()" ID="ID_220093229" CREATED="1632141813672" MODIFIED="1632141813672"/>
<node TEXT="void mark​(int readAheadLimit)" ID="ID_647988592" CREATED="1632141813673" MODIFIED="1632141813673"/>
<node TEXT="boolean markSupported()" ID="ID_404047803" CREATED="1632141813673" MODIFIED="1632141813673"/>
<node TEXT="int read()" ID="ID_1481387701" CREATED="1632141813674" MODIFIED="1632141813674"/>
<node TEXT="int read​(char[] cbuf, int off, int len)" ID="ID_1630478467" CREATED="1632141813675" MODIFIED="1632141813675"/>
<node TEXT="boolean ready()" ID="ID_567303738" CREATED="1632141813676" MODIFIED="1632141813676"/>
<node TEXT="void reset()" ID="ID_1498369244" CREATED="1632141813676" MODIFIED="1632141813676"/>
<node TEXT="long skip​(long ns)" ID="ID_1335407367" CREATED="1632141813677" MODIFIED="1632141813677"/>
</node>
<node TEXT="Buffered" POSITION="left" ID="ID_31654149" CREATED="1631458813346" MODIFIED="1632141940010" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="-45.25 pt" VSHIFT_QUANTITY="57 pt">
<node TEXT="Stream&lt;String&gt; lines()" ID="ID_254752323" CREATED="1632141648225" MODIFIED="1632141648225"/>
<node TEXT="void mark​(int readAheadLimit)" ID="ID_443249768" CREATED="1632141648225" MODIFIED="1632141648225"/>
<node TEXT="boolean markSupported()" ID="ID_1346311883" CREATED="1632141648226" MODIFIED="1632141648226"/>
<node TEXT="int read()" ID="ID_1693480243" CREATED="1632141648226" MODIFIED="1632141648226"/>
<node TEXT="int read​(char[] cbuf, int off, int len)" ID="ID_209764263" CREATED="1632141648226" MODIFIED="1632141648226"/>
<node TEXT="String readLine()" ID="ID_1067266801" CREATED="1632141648226" MODIFIED="1632141966392" BACKGROUND_COLOR="#999900"/>
<node TEXT="boolean ready()" ID="ID_1631369942" CREATED="1632141648227" MODIFIED="1632141648227"/>
<node TEXT="void reset()" ID="ID_268167713" CREATED="1632141648227" MODIFIED="1632141648227"/>
<node TEXT="long skip​(long n)" ID="ID_1809513814" CREATED="1632141648227" MODIFIED="1632141648227"/>
</node>
</node>
</map>
