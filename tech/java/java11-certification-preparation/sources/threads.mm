<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Threads" FOLDED="false" ID="ID_1090958577" CREATED="1606664858024" MODIFIED="1631479464056" VGAP_QUANTITY="3 pt">
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<hook NAME="MapStyle" background="#3c3836" zoom="0.566">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_602083445" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#3c3836" BACKGROUND_COLOR="#fbf1c7" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#a89984" WIDTH="2" TRANSPARENCY="255" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_602083445" STARTINCLINATION="102.75 pt;-19.5 pt;" ENDINCLINATION="102.75 pt;3 pt;" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="11" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#93a1a1" WIDTH="3" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#ffffff" BACKGROUND_COLOR="#458588" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#458588"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_749235638" BORDER_WIDTH="3 px" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#cc241d">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#cc241d" TRANSPARENCY="255" DESTINATION="ID_749235638"/>
<font SIZE="12" ITALIC="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#fdf6e3" BACKGROUND_COLOR="#282828" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_WIDTH="3.1 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#2c2b29" BORDER_DASH_LIKE_EDGE="true">
<font NAME="Ubuntu" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#282828" BACKGROUND_COLOR="#ff9933" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR="#2c2b29">
<font NAME="Ubuntu" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#282828" BACKGROUND_COLOR="#ebdbb2" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" MAX_WIDTH="50 cm">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#282828" BACKGROUND_COLOR="#d79921" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#ffffff" BACKGROUND_COLOR="#98971a" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" COLOR="#ffffff" BACKGROUND_COLOR="#b16286" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" COLOR="#ffffff" BACKGROUND_COLOR="#689d6a" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" COLOR="#ffffff" BACKGROUND_COLOR="#a89984" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BACKGROUND_COLOR="#ebdbb2" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BORDER_COLOR="#f0f0f0" BACKGROUND_COLOR="#ebdbb2">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Executor" POSITION="right" ID="ID_61181113" CREATED="1631479203413" MODIFIED="1631479460631" HGAP_QUANTITY="-0.25 pt" VSHIFT_QUANTITY="-69 pt">
<node TEXT="void execute​(Runnable command)" ID="ID_1493152959" CREATED="1631479349080" MODIFIED="1631479349080"/>
</node>
<node TEXT="Executors" POSITION="left" ID="ID_328009540" CREATED="1631479234495" MODIFIED="1631479805724" VGAP_QUANTITY="4.5 pt" HGAP_QUANTITY="11 pt" VSHIFT_QUANTITY="-43.5 pt" TEXT_ALIGN="DEFAULT">
<node TEXT="Callable&lt;Object&gt; callable​(Runnable task)" ID="ID_355756346" CREATED="1631479301139" MODIFIED="1631479681132" HGAP_QUANTITY="405.49999 pt" VSHIFT_QUANTITY="1.5 pt"/>
<node TEXT="Callable&lt;T&gt; callable​(Runnable task, T result)" ID="ID_1716661753" CREATED="1631479301139" MODIFIED="1631479677092" HGAP_QUANTITY="381.49999 pt" VSHIFT_QUANTITY="0.75 pt"/>
<node TEXT="Callable&lt;Object&gt; callable​(PrivilegedAction&lt;?&gt; action)" ID="ID_1291161384" CREATED="1631479301142" MODIFIED="1631479674716" HGAP_QUANTITY="319.24999 pt" VSHIFT_QUANTITY="3.75 pt"/>
<node TEXT="Callable&lt;Object&gt; callable​(PrivilegedExceptionAction&lt;?&gt; action)" ID="ID_336320242" CREATED="1631479301143" MODIFIED="1631479670532" HGAP_QUANTITY="247.99999 pt" VSHIFT_QUANTITY="0.75 pt"/>
<node TEXT="ExecutorService newCachedThreadPool()" ID="ID_823876624" CREATED="1631479301145" MODIFIED="1631479667853" HGAP_QUANTITY="415.24999 pt" VSHIFT_QUANTITY="-3.75 pt"/>
<node TEXT="ExecutorService newCachedThreadPool​(ThreadFactory threadFactory)" ID="ID_218539439" CREATED="1631479301146" MODIFIED="1631479569197" HGAP_QUANTITY="214.24999 pt" VSHIFT_QUANTITY="-3 pt"/>
<node TEXT="ExecutorService newFixedThreadPool​(int nThreads)" ID="ID_1177148913" CREATED="1631479301147" MODIFIED="1631479556510" HGAP_QUANTITY="349.99999 pt" VSHIFT_QUANTITY="0.75 pt"/>
<node TEXT="ExecutorService newFixedThreadPool​(int nThreads, ThreadFactory threadFactory)" ID="ID_218943721" CREATED="1631479301148" MODIFIED="1631479549108" HGAP_QUANTITY="135.5 pt" VSHIFT_QUANTITY="0.75 pt"/>
<node TEXT="ScheduledExecutorService newScheduledThreadPool​(int corePoolSize)" ID="ID_1357945671" CREATED="1631479301149" MODIFIED="1631479693300" HGAP_QUANTITY="220.99999 pt" VSHIFT_QUANTITY="-2.25 pt"/>
<node TEXT="ScheduledExecutorService newScheduledThreadPool​(int corePoolSize, ThreadFactory threadFactory)" ID="ID_1582502603" CREATED="1631479301150" MODIFIED="1631479800659" HGAP_QUANTITY="6.5 pt" VSHIFT_QUANTITY="0.75 pt"/>
<node TEXT="ExecutorService newSingleThreadExecutor()" ID="ID_1878298273" CREATED="1631479301151" MODIFIED="1631479699298" HGAP_QUANTITY="398.74999 pt" VSHIFT_QUANTITY="-1.5 pt"/>
<node TEXT="ExecutorService newSingleThreadExecutor​(ThreadFactory threadFactory)" ID="ID_1144149799" CREATED="1631479301152" MODIFIED="1631479702972" HGAP_QUANTITY="191.74999 pt" VSHIFT_QUANTITY="-1.5 pt"/>
<node TEXT="ScheduledExecutorService newSingleThreadScheduledExecutor()" ID="ID_1283459889" CREATED="1631479301153" MODIFIED="1631479707284" HGAP_QUANTITY="248.74999 pt" VSHIFT_QUANTITY="1.5 pt"/>
<node TEXT="ScheduledExecutorService newSingleThreadScheduledExecutor​(ThreadFactory threadFactory)" ID="ID_1726222277" CREATED="1631479301154" MODIFIED="1631479796427" HGAP_QUANTITY="40.25 pt" VSHIFT_QUANTITY="-1.5 pt"/>
<node TEXT="ExecutorService newWorkStealingPool()" ID="ID_1191500070" CREATED="1631479301155" MODIFIED="1631479805723" HGAP_QUANTITY="423.49999 pt" VSHIFT_QUANTITY="-3.75 pt"/>
<node TEXT="ExecutorService newWorkStealingPool​(int parallelism)" ID="ID_1311907308" CREATED="1631479301156" MODIFIED="1631479720923" HGAP_QUANTITY="322.24999 pt" VSHIFT_QUANTITY="-5.25 pt"/>
</node>
<node TEXT="ExecutorService" POSITION="right" ID="ID_1814373164" CREATED="1631479208059" MODIFIED="1631479456006" HGAP_QUANTITY="7.25 pt" VSHIFT_QUANTITY="-78.75 pt">
<node TEXT="boolean awaitTermination​(long timeout, TimeUnit unit)" ID="ID_359799278" CREATED="1631479339413" MODIFIED="1631479339413"/>
<node TEXT="List&lt;Future&lt;T&gt;&gt; invokeAll​(Collection&lt;? extends Callable&lt;T&gt;&gt; tasks)" ID="ID_1890133748" CREATED="1631479339413" MODIFIED="1631479339413"/>
<node TEXT="List&lt;Future&lt;T&gt;&gt; invokeAll​(Collection&lt;? extends Callable&lt;T&gt;&gt; tasks, long timeout, TimeUnit unit)" ID="ID_1569404068" CREATED="1631479339416" MODIFIED="1631479339416"/>
<node TEXT="T invokeAny​(Collection&lt;? extends Callable&lt;T&gt;&gt; tasks)" ID="ID_1754468869" CREATED="1631479339416" MODIFIED="1631479339416"/>
<node TEXT="T invokeAny​(Collection&lt;? extends Callable&lt;T&gt;&gt; tasks, long timeout, TimeUnit unit)" ID="ID_1680074215" CREATED="1631479339417" MODIFIED="1631479339417"/>
<node TEXT="boolean isShutdown()" ID="ID_769292450" CREATED="1631479339417" MODIFIED="1631479339417"/>
<node TEXT="boolean isTerminated()" ID="ID_1654002025" CREATED="1631479339418" MODIFIED="1631479339418"/>
<node TEXT="void shutdown()" ID="ID_1959715192" CREATED="1631479339418" MODIFIED="1631479339418"/>
<node TEXT="List&lt;Runnable&gt; shutdownNow()" ID="ID_1224355836" CREATED="1631479339418" MODIFIED="1631479339418"/>
<node TEXT="Future&lt;?&gt; submit​(Runnable task)" ID="ID_117620652" CREATED="1631479339418" MODIFIED="1631479339418"/>
<node TEXT="Future&lt;T&gt; submit​(Runnable task, T result)" ID="ID_1214815418" CREATED="1631479339418" MODIFIED="1631479339418"/>
<node TEXT="Future&lt;T&gt; submit​(Callable&lt;T&gt; task)" ID="ID_863014488" CREATED="1631479339419" MODIFIED="1631479339419"/>
</node>
<node TEXT="ScheduledExecutorService" POSITION="right" ID="ID_1777564251" CREATED="1631479221572" MODIFIED="1631479464054" HGAP_QUANTITY="17 pt" VSHIFT_QUANTITY="-45.75 pt">
<node TEXT="ScheduledFuture&lt;?&gt; schedule​(Runnable command, long delay, TimeUnit unit)" ID="ID_1027809001" CREATED="1631479318171" MODIFIED="1631479318171"/>
<node TEXT="ScheduledFuture&lt;V&gt; schedule​(Callable&lt;V&gt; callable, long delay, TimeUnit unit)" ID="ID_997717200" CREATED="1631479318171" MODIFIED="1631479318171"/>
<node TEXT="ScheduledFuture&lt;?&gt; scheduleAtFixedRate​(Runnable command, long initialDelay, long period, TimeUnit unit)" ID="ID_608565645" CREATED="1631479318174" MODIFIED="1631479318174"/>
<node TEXT="ScheduledFuture&lt;?&gt; scheduleWithFixedDelay​(Runnable command, long initialDelay, long delay, TimeUnit unit)" ID="ID_1531265376" CREATED="1631479318176" MODIFIED="1631479318176"/>
</node>
<node TEXT="Future" POSITION="left" ID="ID_439254656" CREATED="1631479241232" MODIFIED="1631479757645" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="37.25 pt" VSHIFT_QUANTITY="-65.25 pt">
<node TEXT="boolean cancel​(boolean mayInterruptIfRunning)" ID="ID_210276890" CREATED="1631479265775" MODIFIED="1631479744587" HGAP_QUANTITY="183.49999 pt" VSHIFT_QUANTITY="15.75 pt"/>
<node TEXT="V get()  throws InterruptedException, ExecutionException" ID="ID_25519390" CREATED="1631479265775" MODIFIED="1631479738195" HGAP_QUANTITY="115.25 pt" VSHIFT_QUANTITY="-0.75 pt"/>
<node TEXT="V get​(long timeout, TimeUnit unit)" ID="ID_851904628" CREATED="1631479265778" MODIFIED="1631479734803" HGAP_QUANTITY="277.24999 pt" VSHIFT_QUANTITY="-0.75 pt"/>
<node TEXT="throws InterruptedException, ExecutionException, TimeoutException" ID="ID_1204344880" CREATED="1631479265780" MODIFIED="1631479265780"/>
<node TEXT="boolean isCancelled()" ID="ID_1490406342" CREATED="1631479265780" MODIFIED="1631479757643" HGAP_QUANTITY="364.24999 pt" VSHIFT_QUANTITY="-3.75 pt"/>
<node TEXT="boolean isDone()" ID="ID_802647872" CREATED="1631479265781" MODIFIED="1631479754155" HGAP_QUANTITY="397.99999 pt" VSHIFT_QUANTITY="-26.25 pt"/>
</node>
</node>
</map>
