<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="GenericCollAdd" FOLDED="false" ID="ID_696401721" CREATED="1610381621824" MODIFIED="1632210197652" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="2.143">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false" show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
<edge COLOR="#808080"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<node TEXT="List&lt;? super T&gt;" POSITION="right" ID="ID_758250513" CREATED="1632210014496" MODIFIED="1632210195440" HGAP_QUANTITY="2 pt" VSHIFT_QUANTITY="-49.5 pt">
<edge COLOR="#0000ff"/>
<node TEXT="add(T)" ID="ID_397096638" CREATED="1632210053361" MODIFIED="1632210151382"/>
<node TEXT="Object get()" ID="ID_991120120" CREATED="1632210108345" MODIFIED="1632210332146" COLOR="#ff3333"/>
</node>
<node TEXT="List&lt;?&gt;" POSITION="right" ID="ID_1859818811" CREATED="1632210020268" MODIFIED="1632210191591" HGAP_QUANTITY="14 pt" VSHIFT_QUANTITY="36.75 pt">
<edge COLOR="#00ff00"/>
<node TEXT="add(null)" ID="ID_992445799" CREATED="1632210077490" MODIFIED="1632210080694"/>
<node TEXT="Object get()" ID="ID_479682452" CREATED="1632210082731" MODIFIED="1632210338586" COLOR="#000000"/>
</node>
<node TEXT="List (rawtype)" POSITION="left" ID="ID_1277595362" CREATED="1632210025586" MODIFIED="1632210188905" HGAP_QUANTITY="12.5 pt" VSHIFT_QUANTITY="-48 pt">
<edge COLOR="#ff00ff"/>
<node TEXT="add anything" ID="ID_1733424833" CREATED="1632210034727" MODIFIED="1632210038203"/>
<node TEXT="Object get()" ID="ID_1993058198" CREATED="1632210039693" MODIFIED="1632210325751" COLOR="#ff3333"/>
</node>
<node TEXT="List&lt;? extends T&gt;" POSITION="left" ID="ID_1676215586" CREATED="1632210005495" MODIFIED="1632210197650" HGAP_QUANTITY="7.25 pt" VSHIFT_QUANTITY="30.75 pt">
<edge COLOR="#ff0000"/>
<node TEXT="add(null)" ID="ID_974616124" CREATED="1632210116940" MODIFIED="1632210119205"/>
<node TEXT="T get()" ID="ID_1141968343" CREATED="1632210121629" MODIFIED="1632210123432"/>
</node>
</node>
</map>
