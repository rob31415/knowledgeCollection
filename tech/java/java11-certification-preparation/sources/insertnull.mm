<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="insert&#xa;null?" FOLDED="false" ID="ID_696401721" CREATED="1610381621824" MODIFIED="1631803171592" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="1.818">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<node TEXT="YES" POSITION="left" ID="ID_1616457482" CREATED="1631802566755" MODIFIED="1631803129289" HGAP_QUANTITY="62 pt" VSHIFT_QUANTITY="-71.25 pt">
<edge COLOR="#ff0000"/>
<node TEXT="List" ID="ID_1188881614" CREATED="1631803142979" MODIFIED="1631803142979"/>
<node TEXT="Queue" ID="ID_1705937937" CREATED="1631803142981" MODIFIED="1631803142981"/>
<node TEXT="Deque" ID="ID_1684607813" CREATED="1631803142981" MODIFIED="1631803142981"/>
<node TEXT="ArrayList" ID="ID_243637197" CREATED="1631803142979" MODIFIED="1631803142979"/>
<node TEXT="LinkedList" ID="ID_485447452" CREATED="1631803142980" MODIFIED="1631803142980"/>
</node>
<node TEXT="No" POSITION="right" ID="ID_206132337" CREATED="1631803102365" MODIFIED="1631803133006" HGAP_QUANTITY="56 pt" VSHIFT_QUANTITY="-85.5 pt">
<edge COLOR="#0000ff"/>
<node TEXT="TreeMap" ID="ID_167540109" CREATED="1631803160971" MODIFIED="1631803160971"/>
<node TEXT="TreeSet" ID="ID_1039412885" CREATED="1631803160971" MODIFIED="1631803160971"/>
<node TEXT="Hashtable" ID="ID_839845759" CREATED="1631803160973" MODIFIED="1631803160973"/>
<node TEXT="ArrayDeque" ID="ID_1211484373" CREATED="1631803160974" MODIFIED="1631803160974"/>
<node TEXT="BlockingDeque" ID="ID_1246602120" CREATED="1631803160975" MODIFIED="1631803160975"/>
<node TEXT="BlockingQueue" ID="ID_848043281" CREATED="1631803160975" MODIFIED="1631803160975"/>
</node>
<node TEXT="1" OBJECT="java.lang.Long|1" POSITION="right" ID="ID_100340300" CREATED="1631803107746" MODIFIED="1631803127235" HGAP_QUANTITY="-3.25 pt" VSHIFT_QUANTITY="55.5 pt">
<edge COLOR="#ff00ff"/>
<node TEXT="HashMap" ID="ID_1175844440" CREATED="1631803151995" MODIFIED="1631803151995"/>
<node TEXT="HashSet" ID="ID_1450981482" CREATED="1631803151995" MODIFIED="1631803151995"/>
<node TEXT="LinkedHashMap" ID="ID_1061995876" CREATED="1631803151998" MODIFIED="1631803151998"/>
<node TEXT="LinkedHashSet" ID="ID_1427789101" CREATED="1631803151998" MODIFIED="1631803151998"/>
</node>
</node>
</map>
