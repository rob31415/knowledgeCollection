<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Stream" FOLDED="false" ID="ID_1775383331" CREATED="1632261455888" MODIFIED="1632262195633" STYLE="oval"><hook NAME="MapStyle" zoom="1.091">
    <conditional_styles>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="styles.connection" LAST="false">
            <node_periodic_level_condition PERIOD="2" REMAINDER="1"/>
        </conditional_style>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="styles.topic" LAST="false">
            <node_level_condition VALUE="2" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="styles.subtopic" LAST="false">
            <node_level_condition VALUE="4" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="styles.subsubtopic" LAST="false">
            <node_level_condition VALUE="6" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
    </conditional_styles>
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false" show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_339742628" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_339742628" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="Arial" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork" MAX_WIDTH="50 cm">
<font NAME="DejaVu Sans Mono" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.connection" COLOR="#606060" STYLE="fork" MAX_WIDTH="50 cm">
<font NAME="Source Code Pro" SIZE="10" BOLD="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="create" POSITION="left" ID="ID_1729493053" CREATED="1632261723881" MODIFIED="1632262934593" HGAP_QUANTITY="9.5 pt" VSHIFT_QUANTITY="-2.25 pt">
<font SIZE="14" BOLD="true"/>
<node TEXT="Stream&lt;T&gt; of(T t)" ID="ID_1095357406" CREATED="1632262665254" MODIFIED="1632262879228">
<font BOLD="false"/>
</node>
<node TEXT="Stream&lt;T&gt; of(T... values)" ID="ID_1616095042" CREATED="1632262665254" MODIFIED="1632262879230">
<font BOLD="false"/>
</node>
<node TEXT="Stream&lt;T&gt; ofNullable(T t)" ID="ID_104573031" CREATED="1632262665255" MODIFIED="1632262879232" VSHIFT_QUANTITY="-6 pt">
<font BOLD="false"/>
</node>
<node TEXT="Stream&lt;T&gt; generate(Supplier&lt;? extends T&gt; s)" ID="ID_1931625250" CREATED="1632262665256" MODIFIED="1632262879232" HGAP_QUANTITY="14.75 pt" VSHIFT_QUANTITY="4.5 pt">
<font BOLD="false"/>
</node>
<node TEXT="Stream&lt;T&gt; empty()" ID="ID_579347121" CREATED="1632262684972" MODIFIED="1632262879233" HGAP_QUANTITY="14 pt" VSHIFT_QUANTITY="-5.25 pt">
<font BOLD="false"/>
</node>
<node TEXT="Stream&lt;T&gt; iterate(T seed, Predicate&lt;? super T&gt; hasNext, UnaryOperator&lt;T&gt; next)" ID="ID_1559697898" CREATED="1632262665257" MODIFIED="1632262879233">
<font BOLD="false"/>
</node>
<node TEXT="Stream&lt;T&gt; iterate(T seed, UnaryOperator&lt;T&gt; f)" ID="ID_1088823099" CREATED="1632262665257" MODIFIED="1632262879234" VSHIFT_QUANTITY="-9 pt">
<font BOLD="false"/>
</node>
<node TEXT="Stream&lt;T&gt; concat(Stream&lt;? extends T&gt; a, Stream&lt;? extends T&gt; b)" ID="ID_1879618695" CREATED="1632262665258" MODIFIED="1632262879234">
<font BOLD="false"/>
</node>
<node TEXT="Object[] toArray()" ID="ID_514070528" CREATED="1632262665258" MODIFIED="1632262879234">
<font BOLD="false"/>
</node>
<node TEXT="A[] toArray(IntFunction&lt;A[]&gt; generator)" ID="ID_599575340" CREATED="1632262665259" MODIFIED="1632262879234">
<font BOLD="false"/>
</node>
</node>
<node TEXT="iterate" POSITION="right" ID="ID_7139167" CREATED="1632261754300" MODIFIED="1632262934594" HGAP_QUANTITY="12.5 pt" VSHIFT_QUANTITY="-57 pt">
<font SIZE="14" BOLD="true"/>
<node TEXT="void forEach​(Consumer&lt;? super T&gt; action)&#xa;void forEachOrdered​(Consumer&lt;? super T&gt; action)&#xa;Stream&lt;T&gt; peek​(Consumer&lt;? super T&gt; action)" ID="ID_918722055" CREATED="1632261779953" MODIFIED="1632262887898">
<font BOLD="false"/>
</node>
</node>
<node TEXT="query" POSITION="right" ID="ID_1789282150" CREATED="1632261790363" MODIFIED="1632262934594" HGAP_QUANTITY="50 pt" VSHIFT_QUANTITY="31.5 pt">
<font SIZE="14" BOLD="true"/>
<node TEXT="boolean allMatch(Predicate&lt;? super T&gt; predicate)" ID="ID_1022418066" CREATED="1632262684966" MODIFIED="1632262887898">
<font BOLD="false"/>
</node>
<node TEXT="boolean anyMatch(Predicate&lt;? super T&gt; predicate)" ID="ID_474085222" CREATED="1632262684966" MODIFIED="1632262887897">
<font BOLD="false"/>
</node>
<node TEXT="boolean noneMatch(Predicate&lt;? super T&gt; predicate)" ID="ID_524024843" CREATED="1632262684968" MODIFIED="1632262887897" VSHIFT_QUANTITY="-6.75 pt">
<font BOLD="false"/>
</node>
<node TEXT="Optional&lt;T&gt; max(Comparator&lt;? super T&gt; comparator)" ID="ID_874766239" CREATED="1632262684968" MODIFIED="1632262887897">
<font BOLD="false"/>
</node>
<node TEXT="Optional&lt;T&gt; min(Comparator&lt;? super T&gt; comparator)" ID="ID_96168383" CREATED="1632262684969" MODIFIED="1632262887896" VSHIFT_QUANTITY="-8.25 pt">
<font BOLD="false"/>
</node>
<node TEXT="Optional&lt;T&gt; findAny()" ID="ID_548058297" CREATED="1632262684969" MODIFIED="1632262887896">
<font BOLD="false"/>
</node>
<node TEXT="Optional&lt;T&gt; findFirst()" ID="ID_1409101599" CREATED="1632262684970" MODIFIED="1632262887895" VSHIFT_QUANTITY="-9 pt">
<font BOLD="false"/>
</node>
<node TEXT="Stream&lt;T&gt; filter(Predicate&lt;? super T&gt; predicate)" ID="ID_816048565" CREATED="1632262684971" MODIFIED="1632262887895" HGAP_QUANTITY="12.5 pt" VSHIFT_QUANTITY="-9 pt">
<font BOLD="false"/>
</node>
<node TEXT="long count()" ID="ID_1562002193" CREATED="1632262684971" MODIFIED="1632262887893">
<font BOLD="false"/>
</node>
</node>
<node TEXT="transform" POSITION="left" ID="ID_122139294" CREATED="1632261807109" MODIFIED="1632262934590" HGAP_QUANTITY="0.5 pt" VSHIFT_QUANTITY="28.5 pt">
<font SIZE="14" BOLD="true"/>
<node TEXT="manipulate" ID="ID_176231169" CREATED="1632261983238" MODIFIED="1632262946189" HGAP_QUANTITY="-42.25 pt" VSHIFT_QUANTITY="-13.5 pt">
<font BOLD="false"/>
<node TEXT="Stream&lt;T&gt; takeWhile(Predicate&lt;? super T&gt; predicate)" ID="ID_1130954930" CREATED="1632262579852" MODIFIED="1632262579852"/>
<node TEXT="Stream&lt;T&gt; dropWhile(Predicate&lt;? super T&gt; predicate)" ID="ID_1234282327" CREATED="1632262579852" MODIFIED="1632262579852"/>
<node TEXT="Stream&lt;T&gt; limit(long maxSize)" ID="ID_706040253" CREATED="1632262579854" MODIFIED="1632262579854"/>
<node TEXT="Stream&lt;T&gt; skip(long n)" ID="ID_1771914463" CREATED="1632262579854" MODIFIED="1632262579854"/>
<node TEXT="Stream&lt;T&gt; distinct()" ID="ID_1170855631" CREATED="1632262579855" MODIFIED="1632262579855"/>
<node TEXT="Optional&lt;T&gt; reduce(BinaryOperator&lt;T&gt; accumulator)" ID="ID_1173431656" CREATED="1632262579856" MODIFIED="1632262579856"/>
<node TEXT="T reduce(T identity, BinaryOperator&lt;T&gt; accumulator)" ID="ID_695852205" CREATED="1632262579857" MODIFIED="1632262579857"/>
<node TEXT="U reduce(U identity, BiFunction&lt;U,? super T,U&gt; accumulator, BinaryOperator&lt;U&gt; combiner)" ID="ID_111110573" CREATED="1632262579857" MODIFIED="1632262579857"/>
<node TEXT="Stream&lt;T&gt; sorted()" ID="ID_1190122858" CREATED="1632262579858" MODIFIED="1632262579858"/>
<node TEXT="Stream&lt;T&gt; sorted(Comparator&lt;? super T&gt; comparator)" ID="ID_190129907" CREATED="1632262579859" MODIFIED="1632262579859"/>
</node>
<node TEXT="map" ID="ID_729638496" CREATED="1632261843393" MODIFIED="1632262946192" HGAP_QUANTITY="16.25 pt" VSHIFT_QUANTITY="-20.25 pt">
<font BOLD="false"/>
<node TEXT="Stream&lt;R&gt; map(Function&lt;? super T,? extends R&gt; mapper)" ID="ID_210236859" CREATED="1632262601204" MODIFIED="1632262609788"/>
<node TEXT="DoubleStream mapToDouble(ToDoubleFunction&lt;? super T&gt; mapper)" ID="ID_295774587" CREATED="1632262601204" MODIFIED="1632262601204"/>
<node TEXT="IntStream    mapToInt(ToIntFunction&lt;? super T&gt; mapper)" ID="ID_198098011" CREATED="1632262601205" MODIFIED="1632262601205"/>
<node TEXT="LongStream   mapToLong(ToLongFunction&lt;? super T&gt; mapper)" ID="ID_145091317" CREATED="1632262601206" MODIFIED="1632262601206"/>
</node>
<node TEXT="flatMap" ID="ID_2884376" CREATED="1632261852033" MODIFIED="1632262946192" HGAP_QUANTITY="17 pt" VSHIFT_QUANTITY="-13.5 pt">
<font BOLD="false"/>
<node TEXT="Stream&lt;R&gt; flatMap(Function&lt;? super T,? extends Stream&lt;? extends R&gt;&gt; mapper)" ID="ID_947341506" CREATED="1632262617953" MODIFIED="1632262624406"/>
<node TEXT="DoubleStream flatMapToDouble(Function&lt;? super T,? extends DoubleStream&gt; mapper)" ID="ID_811818424" CREATED="1632262617953" MODIFIED="1632262617953"/>
<node TEXT="IntStream    flatMapToInt(Function&lt;? super T,? extends IntStream&gt; mapper)" ID="ID_378305345" CREATED="1632262617954" MODIFIED="1632262617954"/>
<node TEXT="LongStream   flatMapToLong(Function&lt;? super T,? extends LongStream&gt; mapper)" ID="ID_1346293765" CREATED="1632262617955" MODIFIED="1632262617955"/>
</node>
<node TEXT="terminate" ID="ID_408770990" CREATED="1632261917732" MODIFIED="1632262946193" HGAP_QUANTITY="-49.75 pt" VSHIFT_QUANTITY="-4.5 pt">
<font BOLD="false"/>
<node TEXT="R collect(Supplier&lt;R&gt; supplier, BiConsumer&lt;R,? super T&gt; accumulator, BiConsumer&lt;R,R&gt; combiner)" ID="ID_43072805" CREATED="1632262633109" MODIFIED="1632262633109"/>
<node TEXT="R collect(Collector&lt;? super T,A,R&gt; collector)" ID="ID_1264765152" CREATED="1632262633109" MODIFIED="1632262633109"/>
</node>
</node>
</node>
</map>
