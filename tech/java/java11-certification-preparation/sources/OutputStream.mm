<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Output&#xa;Stream" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="Freemind_Link_1513112588" CREATED="1153430895318" MODIFIED="1631452481217" VGAP_QUANTITY="3 pt">
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<hook NAME="MapStyle" background="#9c9c9c" zoom="0.468">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_249848891" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#000000" BACKGROUND_COLOR="#fff024" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_249848891" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#09387a" WIDTH="3" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#fff024" BACKGROUND_COLOR="#000000">
<font BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#f6f9a1" TEXT_ALIGN="LEFT">
<icon BUILTIN="clock2"/>
<font SIZE="10" ITALIC="true"/>
<edge COLOR="#000000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#000000" BACKGROUND_COLOR="#f5131f" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#f5131f"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_254323574" STYLE="narrow_hexagon" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#c3131f">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#c3131f" TRANSPARENCY="255" DESTINATION="ID_254323574"/>
<font SIZE="11" BOLD="true"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#000000" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_WIDTH="3.1 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#2c2b29" BORDER_DASH_LIKE_EDGE="true">
<font NAME="Ubuntu" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#ffffff" BACKGROUND_COLOR="#09387a" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR="#2c2b29">
<font NAME="Ubuntu" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#ffffff" BACKGROUND_COLOR="#126cb3" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#000000" BACKGROUND_COLOR="#d4e226" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" MAX_WIDTH="50 cm">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#000000" BACKGROUND_COLOR="#fff024" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" BACKGROUND_COLOR="#fff024" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BACKGROUND_COLOR="#fff024" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BACKGROUND_COLOR="#fff024" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BACKGROUND_COLOR="#fff024" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<font NAME="Source Code Pro"/>
<node TEXT="File" POSITION="left" ID="ID_1159477562" CREATED="1631372453967" MODIFIED="1631452571004" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="-82 pt" VSHIFT_QUANTITY="-88.5 pt">
<font NAME="Source Code Pro"/>
<node TEXT="FileOutputStream​(File file)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1754473145" CREATED="1631394007286" MODIFIED="1631452555381" BACKGROUND_COLOR="#00ff00" HGAP_QUANTITY="214.24999 pt" VSHIFT_QUANTITY="3.75 pt"/>
<node TEXT="FileOutputStream​(File file, boolean append)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_917140453" CREATED="1631394007286" MODIFIED="1631452551117" BACKGROUND_COLOR="#00ff00" HGAP_QUANTITY="59.75 pt" VSHIFT_QUANTITY="-0.75 pt"/>
<node TEXT="FileOutputStream​(String name)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_700975476" CREATED="1631394007287" MODIFIED="1631452544276" BACKGROUND_COLOR="#00ff00" HGAP_QUANTITY="169.25 pt" VSHIFT_QUANTITY="6 pt"/>
<node TEXT="FileOutputStream​(String name, boolean append)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_833661931" CREATED="1631394007290" MODIFIED="1631394853285" BACKGROUND_COLOR="#00ff00"/>
<node TEXT="FileOutputStream​(FileDescriptor fdObj)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_460080763" CREATED="1631394007291" MODIFIED="1631452561628" BACKGROUND_COLOR="#00ff00" HGAP_QUANTITY="104.75 pt" VSHIFT_QUANTITY="-2.25 pt"/>
<node TEXT="void write​(byte[] b)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_900269050" CREATED="1631394007292" MODIFIED="1631452564700" BACKGROUND_COLOR="#00ff00" HGAP_QUANTITY="282.49999 pt" VSHIFT_QUANTITY="-0.75 pt"/>
<node TEXT="void write​(byte[] b, int off, int len)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1876232871" CREATED="1631394007293" MODIFIED="1631452566740" BACKGROUND_COLOR="#00ff00" HGAP_QUANTITY="146.75 pt" VSHIFT_QUANTITY="-2.25 pt"/>
<node TEXT="void write​(int b)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1166288948" CREATED="1631394007293" MODIFIED="1631452571004" BACKGROUND_COLOR="#00ff00" HGAP_QUANTITY="310.24999 pt" VSHIFT_QUANTITY="-0.75 pt"/>
</node>
<node TEXT="Object" POSITION="left" ID="ID_337232760" CREATED="1631373269171" MODIFIED="1631452465150" HGAP_QUANTITY="-59.5 pt" VSHIFT_QUANTITY="-152.25 pt">
<font NAME="Source Code Pro"/>
<node TEXT="ObjectOutputStream​(OutputStream out)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1402164962" CREATED="1631394051215" MODIFIED="1631394075285" TEXT_ALIGN="LEFT"/>
<node TEXT="void write​(byte[] buf)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_421125509" CREATED="1631394051215" MODIFIED="1631394075286" TEXT_ALIGN="LEFT"/>
<node TEXT="void write​(byte[] buf, int off, int len)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1569065420" CREATED="1631394051216" MODIFIED="1631394075286" TEXT_ALIGN="LEFT"/>
<node TEXT="void write​(int val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_824326959" CREATED="1631394051216" MODIFIED="1631394075286" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeBoolean​(boolean val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_672702059" CREATED="1631394051216" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeByte​(int val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1286052707" CREATED="1631394051216" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeBytes​(String str)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1217423200" CREATED="1631394051216" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeChar​(int val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1239975573" CREATED="1631394051216" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeChars​(String str)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_154764495" CREATED="1631394051216" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeDouble​(double val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1650894587" CREATED="1631394051216" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeFloat​(float val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_290698738" CREATED="1631394051217" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeInt​(int val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1345071534" CREATED="1631394051217" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeLong​(long val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1584391720" CREATED="1631394051217" MODIFIED="1631394075287" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeShort​(int val)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_179690438" CREATED="1631394051217" MODIFIED="1631394075288" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeUTF​(String str)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1671265099" CREATED="1631394051217" MODIFIED="1631395629504" BACKGROUND_COLOR="#ff0000" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeObject​(Object obj)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_886375701" CREATED="1631394051217" MODIFIED="1631394075288" TEXT_ALIGN="LEFT"/>
<node TEXT="ObjectOutputStream.PutField putFields()" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1000567626" CREATED="1631394051217" MODIFIED="1631394075288" TEXT_ALIGN="LEFT"/>
<node TEXT="void writeFields()" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1225753207" CREATED="1631394051217" MODIFIED="1631394075288" TEXT_ALIGN="LEFT"/>
</node>
<node TEXT="Print" POSITION="right" ID="ID_1057903348" CREATED="1631376179783" MODIFIED="1631452452028" HGAP_QUANTITY="38.75 pt" VSHIFT_QUANTITY="-95.25 pt">
<font NAME="Source Code Pro"/>
<node TEXT="PrintStream​(File file)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1222894286" CREATED="1631394738820" MODIFIED="1631394889407" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(File file, String csn)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_876908493" CREATED="1631394738820" MODIFIED="1631394889410" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(File file, Charset charset)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1110703583" CREATED="1631394738821" MODIFIED="1631394889410" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(String fileName)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_843173448" CREATED="1631394738821" MODIFIED="1631394889411" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(String fileName, String csn)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1835727454" CREATED="1631394738822" MODIFIED="1631394889411" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(String fileName, Charset charset)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1977126989" CREATED="1631394738823" MODIFIED="1631394889412" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(OutputStream out)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_214920584" CREATED="1631394738823" MODIFIED="1631394889412" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(OutputStream out, boolean autoFlush)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_234640528" CREATED="1631394738824" MODIFIED="1631394889412" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(OutputStream out, boolean autoFlush, String encoding)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1758709418" CREATED="1631394738824" MODIFIED="1631394889412" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream​(OutputStream out, boolean autoFlush, Charset charset)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_158238402" CREATED="1631394738824" MODIFIED="1631394889412" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream append​(char c)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_25973451" CREATED="1631394738824" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream append​(CharSequence csq)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_951190399" CREATED="1631394738825" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream append​(CharSequence csq, int start, int end)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_689166187" CREATED="1631394738825" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream format​(String format, Object... args)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1819106150" CREATED="1631394738825" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream format​(Locale l, String format, Object... args)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_578141384" CREATED="1631394738825" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream printf​(String format, Object... args)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1417152823" CREATED="1631394738826" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="PrintStream printf​(Locale l, String format, Object... args)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1361932602" CREATED="1631394738826" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="void print​() boolean,char,char[],double,float,int,long,Object,String" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1007006144" CREATED="1631394738826" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="void println()" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1260692300" CREATED="1631394738826" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="void println​ boolean,char,char[],double,float,int,long,Object,String" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1878233514" CREATED="1631394738827" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="void write​(byte[] buf, int off, int len)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1429995112" CREATED="1631394738827" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
<node TEXT="void write​(int b)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_385517881" CREATED="1631394738827" MODIFIED="1631394889413" BACKGROUND_COLOR="#ccccff"/>
</node>
<node TEXT="Data" POSITION="right" ID="ID_835774296" CREATED="1631373308484" MODIFIED="1631452481217" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="39.50001 pt" VSHIFT_QUANTITY="-155.25 pt" TEXT_ALIGN="CENTER">
<font NAME="Source Code Pro"/>
<node TEXT="DataOutputStrem(OutputStream)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_281629005" CREATED="1631393984247" MODIFIED="1631394907751" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void write​(byte[] b, int off, int len)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_882907538" CREATED="1631393984247" MODIFIED="1631394907754" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void write​(int b)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1667865878" CREATED="1631393984248" MODIFIED="1631394907755" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeBoolean​(boolean v)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_567068133" CREATED="1631393984249" MODIFIED="1631394907755" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeByte​(int v)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1407575258" CREATED="1631393984250" MODIFIED="1631394907755" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeBytes​(String s)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_870425642" CREATED="1631393984250" MODIFIED="1631394907756" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeChar​(int v)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1912471136" CREATED="1631393984251" MODIFIED="1631394907756" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeChars​(String s)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_661013571" CREATED="1631393984251" MODIFIED="1631394907756" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeDouble​(double v)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1515577368" CREATED="1631393984251" MODIFIED="1631394907756" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeFloat​(float v)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_865439170" CREATED="1631393984251" MODIFIED="1631394907756" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeInt​(int v)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1849828815" CREATED="1631393984251" MODIFIED="1631394907756" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeLong​(long v)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_980422011" CREATED="1631393984251" MODIFIED="1631394907756" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeShort​(int v)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1801959583" CREATED="1631393984251" MODIFIED="1631394907756" BACKGROUND_COLOR="#ff9999"/>
<node TEXT="void writeUTF​(String str)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1924885361" CREATED="1631393984252" MODIFIED="1631395618360" BACKGROUND_COLOR="#ff0000"/>
</node>
<node TEXT="Buffered" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_265742838" CREATED="1631373301132" MODIFIED="1631452606415" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="-155.49999 pt" VSHIFT_QUANTITY="-91.5 pt">
<edge STYLE="sharp_bezier"/>
<font NAME="Source Code Pro"/>
<node TEXT="BufferedOutputStream(OutputStream out)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1557788706" CREATED="1631394459834" MODIFIED="1631452606413" BACKGROUND_COLOR="#99ffff" HGAP_QUANTITY="89.75 pt" VSHIFT_QUANTITY="-2.25 pt"/>
<node TEXT="BufferedOutputStream(OutputStream out, int size)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1865166948" CREATED="1631394459834" MODIFIED="1631394927241" BACKGROUND_COLOR="#99ffff"/>
<node TEXT="void write​(byte[] b, int off, int len)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_462253153" CREATED="1631394459835" MODIFIED="1631452603926" BACKGROUND_COLOR="#99ffff" HGAP_QUANTITY="164.75 pt" VSHIFT_QUANTITY="-3.75 pt"/>
<node TEXT="void write​(int b)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_1619182105" CREATED="1631394459836" MODIFIED="1631452599949" BACKGROUND_COLOR="#99ffff" HGAP_QUANTITY="322.24999 pt" VSHIFT_QUANTITY="-12.75 pt"/>
</node>
</node>
</map>
