<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="AtomicInteger" FOLDED="false" ID="ID_696401721" CREATED="1610381621824" MODIFIED="1633011064520" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork" MAX_WIDTH="50 cm">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="9" RULE="ON_BRANCH_CREATION"/>
<node TEXT="*AndGet" POSITION="right" ID="ID_1096285263" CREATED="1633010781862" MODIFIED="1633011018799" HGAP_QUANTITY="11 pt" VSHIFT_QUANTITY="-27.75 pt">
<edge COLOR="#0000ff"/>
<node TEXT="int accumulateAndGet(int x, IntBinaryOperator accumulatorFunction)" ID="ID_538648996" CREATED="1633010785083" MODIFIED="1633010785083"/>
<node TEXT="int addAndGet(int delta)" ID="ID_1852019407" CREATED="1633010785083" MODIFIED="1633010785083"/>
<node TEXT="int decrementAndGet()" ID="ID_1259894054" CREATED="1633010785086" MODIFIED="1633010785086"/>
<node TEXT="int incrementAndGet()" ID="ID_1555298008" CREATED="1633010785087" MODIFIED="1633010785087"/>
<node TEXT="int updateAndGet(IntUnaryOperator updateFunction)" ID="ID_1060349774" CREATED="1633010785088" MODIFIED="1633010785088"/>
</node>
<node TEXT="cons" POSITION="left" ID="ID_481995726" CREATED="1633010829624" MODIFIED="1633010934195" HGAP_QUANTITY="-115 pt" VSHIFT_QUANTITY="-18 pt">
<edge COLOR="#ff00ff"/>
<node TEXT="AtomicInteger()" ID="ID_868159761" CREATED="1633010836823" MODIFIED="1633010836823"/>
<node TEXT="AtomicInteger(int initialValue)" ID="ID_1375144226" CREATED="1633010836823" MODIFIED="1633010836823"/>
</node>
<node TEXT="getAnd*" POSITION="left" ID="ID_573153944" CREATED="1633010787139" MODIFIED="1633011013245" HGAP_QUANTITY="6.5 pt" VSHIFT_QUANTITY="-22.5 pt">
<edge COLOR="#00ff00"/>
<node TEXT="int getAndAccumulate(int x, IntBinaryOperator accumulatorFunction)" ID="ID_261909302" CREATED="1633010799060" MODIFIED="1633010799060"/>
<node TEXT="int getAndAdd(int delta)" ID="ID_527397623" CREATED="1633010799060" MODIFIED="1633010799060"/>
<node TEXT="int getAndDecrement()" ID="ID_588492922" CREATED="1633010799062" MODIFIED="1633010799062"/>
<node TEXT="int getAndIncrement()" ID="ID_1411288631" CREATED="1633010799062" MODIFIED="1633010799062"/>
<node TEXT="int getAndUpdate(IntUnaryOperator updateFunction)" ID="ID_799812893" CREATED="1633010799062" MODIFIED="1633010799062"/>
</node>
<node TEXT="get" POSITION="right" ID="ID_520347470" CREATED="1633010855075" MODIFIED="1633011032655" HGAP_QUANTITY="28.25 pt" VSHIFT_QUANTITY="-9 pt">
<edge COLOR="#00ffff"/>
<node TEXT="int getAndSet(int newValue)" ID="ID_1708287037" CREATED="1633010860200" MODIFIED="1633010860200"/>
</node>
<node TEXT="CaS" POSITION="right" ID="ID_218609355" CREATED="1633010874506" MODIFIED="1633011039894" HGAP_QUANTITY="18.5 pt" VSHIFT_QUANTITY="12.75 pt">
<edge COLOR="#7c0000"/>
<node TEXT="int compareAndExchange(int expectedValue, int newValue)" ID="ID_109396920" CREATED="1633010876455" MODIFIED="1633010876455"/>
<node TEXT="int compareAndExchangeAcquire(int expectedValue, int newValue)" ID="ID_527809107" CREATED="1633010876455" MODIFIED="1633010876455"/>
<node TEXT="int compareAndExchangeRelease(int expectedValue, int newValue)" ID="ID_585714153" CREATED="1633010876461" MODIFIED="1633010876461"/>
<node TEXT="boolean compareAndSet(int expectedValue, int newValue)" ID="ID_1349671063" CREATED="1633010876467" MODIFIED="1633010876467"/>
</node>
<node TEXT="get mem" POSITION="left" ID="ID_1380941341" CREATED="1633010894756" MODIFIED="1633011028791" HGAP_QUANTITY="17 pt" VSHIFT_QUANTITY="-17.25 pt">
<edge COLOR="#00007c"/>
<node TEXT="int get()" ID="ID_212211170" CREATED="1633010900817" MODIFIED="1633010900817"/>
<node TEXT="double doubleValue()" ID="ID_1558292958" CREATED="1633010900817" MODIFIED="1633010900817"/>
<node TEXT="float floatValue()" ID="ID_799632200" CREATED="1633010900819" MODIFIED="1633010900819"/>
<node TEXT="int intValue()" ID="ID_1918842117" CREATED="1633010900820" MODIFIED="1633010900820"/>
<node TEXT="long longValue()" ID="ID_1825279421" CREATED="1633010900821" MODIFIED="1633010900821"/>
<node TEXT="int getAcquire()" ID="ID_165719327" CREATED="1633010900822" MODIFIED="1633010900822"/>
<node TEXT="int getOpaque()" ID="ID_720438085" CREATED="1633010900823" MODIFIED="1633010900823"/>
<node TEXT="int getPlain()" ID="ID_1794946511" CREATED="1633010900824" MODIFIED="1633010900824"/>
</node>
<node TEXT="set mem" POSITION="right" ID="ID_1663006822" CREATED="1633010906999" MODIFIED="1633011036462" VSHIFT_QUANTITY="18 pt">
<edge COLOR="#007c00"/>
<node TEXT="void set(int newValue)" ID="ID_1930485608" CREATED="1633010909257" MODIFIED="1633010909257"/>
<node TEXT="void lazySet(int newValue)" ID="ID_1383794760" CREATED="1633010909257" MODIFIED="1633010909257"/>
<node TEXT="void setOpaque(int newValue)" ID="ID_1111451071" CREATED="1633010909259" MODIFIED="1633010909259"/>
<node TEXT="void setPlain(int newValue)" ID="ID_99843865" CREATED="1633010909260" MODIFIED="1633010909260"/>
<node TEXT="void setRelease(int newValue)" ID="ID_1096409073" CREATED="1633010909261" MODIFIED="1633010909261"/>
</node>
<node TEXT="weak mem" POSITION="left" ID="ID_1392607499" CREATED="1633010913923" MODIFIED="1633011064518" HGAP_QUANTITY="-57.25 pt" VSHIFT_QUANTITY="-1.5 pt">
<edge COLOR="#7c007c"/>
<node TEXT="boolean weakCompareAndSet(int expectedValue, int newValue)" ID="ID_826154027" CREATED="1633010916566" MODIFIED="1633010916566"/>
<node TEXT="boolean weakCompareAndSetAcquire(int expectedValue, int newValue)" ID="ID_1740855792" CREATED="1633010916566" MODIFIED="1633010916566"/>
<node TEXT="boolean weakCompareAndSetPlain(int expectedValue, int newValue)" ID="ID_1858042670" CREATED="1633010916567" MODIFIED="1633010916567"/>
<node TEXT="boolean weakCompareAndSetRelease(int expectedValue, int newValue)" ID="ID_1534751418" CREATED="1633010916568" MODIFIED="1633010916568"/>
<node TEXT="boolean weakCompareAndSetVolatile(int expectedValue, int newValue)" ID="ID_219443572" CREATED="1633010916569" MODIFIED="1633010916569"/>
</node>
</node>
</map>
