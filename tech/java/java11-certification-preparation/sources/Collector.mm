<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Collector&#xa;&#xa;&lt;T InputElement,&#xa;A Accu,&#xa;R Result&gt;" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1090958577" CREATED="1409300609620" MODIFIED="1632307252851" VGAP_QUANTITY="3 pt">
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<font NAME="Source Code Pro" BOLD="true"/>
<hook NAME="MapStyle" background="#eeeeee" zoom="0.5">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_1082331723" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#222222" BACKGROUND_COLOR="#eeeeee" STYLE="rectangle" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1082331723" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="Source Code Pro" SIZE="11" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="horizontal" COLOR="#444444" WIDTH="3" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#f6f9a1" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#ffffff" BACKGROUND_COLOR="#3333ff" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#3333ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_334768726" COLOR="#443333" BACKGROUND_COLOR="#dddddd" BORDER_WIDTH="3 px" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#cc0033">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#cc0033" TRANSPARENCY="255" DESTINATION="ID_334768726"/>
<font SIZE="12" BOLD="true"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#000000" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#2c2b29" BORDER_DASH_LIKE_EDGE="true">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#ffffff" BACKGROUND_COLOR="#222222" BORDER_COLOR="#2c2b29">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#000000" BACKGROUND_COLOR="#ffffff" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" MAX_WIDTH="70 cm">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#ffffff" BACKGROUND_COLOR="#676767" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#ffffff" BACKGROUND_COLOR="#888888" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" COLOR="#222222" BACKGROUND_COLOR="#aaaaaa" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" COLOR="#222222" BACKGROUND_COLOR="#bbbbbb" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" COLOR="#222222" BACKGROUND_COLOR="#cccccc" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" COLOR="#222222" BACKGROUND_COLOR="#dddddd" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" COLOR="#222222" BACKGROUND_COLOR="#eeeeee" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" COLOR="#2c2b29" BACKGROUND_COLOR="#ffffff" BORDER_COLOR="#f0f0f0">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="query" POSITION="right" ID="ID_225421284" CREATED="1632263798468" MODIFIED="1632307201509" VGAP_QUANTITY="20.25 pt" HGAP_QUANTITY="-22.75 pt" VSHIFT_QUANTITY="-88.5 pt">
<node ID="ID_1869420072" CREATED="1632306949978" MODIFIED="1632306949978"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Double</b>&gt; <b>averagingDouble</b>(ToDoubleFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_791287138" CREATED="1632306949978" MODIFIED="1632306949978"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Double</b>&gt; <b>averagingInt</b>(ToIntFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_14130733" CREATED="1632306949979" MODIFIED="1632306949979"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Double</b>&gt; <b>averagingLong</b>(ToLongFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_834570075" CREATED="1632306949980" MODIFIED="1632306949980"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Long</b>&gt; <b>counting</b>()
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_610574576" CREATED="1632306949981" MODIFIED="1632306949981"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Double</b>&gt; <b>summingDouble</b>(ToDoubleFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1985813162" CREATED="1632306949982" MODIFIED="1632306949982"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Integer</b>&gt; <b>summingInt</b>(ToIntFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_773714644" CREATED="1632306949983" MODIFIED="1632306949983"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Long</b>&gt; <b>summingLong</b>(ToLongFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_179664510" CREATED="1632306949984" MODIFIED="1632306949984"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>DoubleSummaryStatistics</b>&gt; <b>summarizingDouble</b>(ToDoubleFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_113095870" CREATED="1632306949985" MODIFIED="1632306949985"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>IntSummaryStatistics</b>&gt; <b>summarizingInt</b>(ToIntFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1847941474" CREATED="1632306949985" MODIFIED="1632306949985"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>LongSummaryStatistics</b>&gt; <b>summarizingLong</b>(ToLongFunction&lt;? super T&gt; mapper)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1873180340" CREATED="1632306949986" MODIFIED="1632306949986"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Optional&lt;T&gt;</b>&gt; <b>maxBy</b>(Comparator&lt;? super T&gt; comparator)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1113092616" CREATED="1632306949988" MODIFIED="1632306949988"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Optional&lt;T&gt;</b>&gt; <b>minBy</b>(Comparator&lt;? super T&gt; comparator)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="orga" POSITION="left" ID="ID_594928653" CREATED="1632263737051" MODIFIED="1632307885971" VGAP_QUANTITY="36 pt" HGAP_QUANTITY="11 pt" VSHIFT_QUANTITY="-12.75 pt">
<node ID="ID_577983620" CREATED="1632306795305" MODIFIED="1632307846580" HGAP_QUANTITY="528.49998 pt" VSHIFT_QUANTITY="4.5 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Map&lt;K,List&lt;T&gt;&gt;</b>&gt; <b>groupingBy</b>(Function&lt;? super T,? extends K&gt; classifier)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_550869472" CREATED="1632306795308" MODIFIED="1632307838764" HGAP_QUANTITY="198.49999 pt" VSHIFT_QUANTITY="-3 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Map&lt;K,D&gt;</b>&gt; <b>groupingBy</b>(Function&lt;? super T,? extends K&gt; classifier, Collector&lt;? super T,A,D&gt; downstream)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1705479715" CREATED="1632306795305" MODIFIED="1632306795305"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>M</b>&gt; <b>groupingBy</b>(Function&lt;? super T,? extends K&gt; classifier, Supplier&lt;M&gt; mapFactory, Collector&lt;? super T,A,D&gt; downstream)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_553831802" CREATED="1632306795309" MODIFIED="1632307858114" HGAP_QUANTITY="409.99999 pt" VSHIFT_QUANTITY="13.5 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>ConcurrentMap&lt;K,List&lt;T&gt;&gt;</b>&gt; <b>groupingByConcurrent</b>(Function&lt;? super T,? extends K&gt; classifier)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_263891534" CREATED="1632306795312" MODIFIED="1632307868298" HGAP_QUANTITY="83 pt" VSHIFT_QUANTITY="-3 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>ConcurrentMap&lt;K,D&gt;</b>&gt; <b>groupingByConcurrent</b>(Function&lt;? super T,? extends K&gt; classifier, Collector&lt;? super T,A,D&gt; downstream)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_880556213" CREATED="1632306795310" MODIFIED="1632306795310"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>M</b>&gt; <b>groupingByConcurrent</b>(Function&lt;? super T,? extends K&gt; classifier, Supplier&lt;M&gt; mapFactory, Collector&lt;? super T,A,D&gt; downstream)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1809741998" CREATED="1632306795312" MODIFIED="1632307885969" HGAP_QUANTITY="345.49999 pt" VSHIFT_QUANTITY="13.5 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Map&lt;Boolean,List&lt;T&gt;&gt;</b>&gt; <b>partitioningBy</b>(Predicate&lt;? super T&gt; predicate)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_499827230" CREATED="1632306795313" MODIFIED="1632306795313"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Map&lt;Boolean,D&gt;</b>&gt; <b>partitioningBy</b>(Predicate&lt;? super T&gt; predicate, Collector&lt;? super T,A,D&gt; downstream)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="trans" POSITION="left" ID="ID_1906865344" CREATED="1632263672376" MODIFIED="1632307059467" VGAP_QUANTITY="23.25 pt" HGAP_QUANTITY="7.25 pt" VSHIFT_QUANTITY="72 pt">
<node TEXT="Collector&lt;T,?,C&gt; toCollection(Supplier&lt;C&gt; collectionFactory)" ID="ID_231835016" CREATED="1632264148689" MODIFIED="1632306400407"/>
<node TEXT="Collector&lt;T,?,List&lt;T&gt;&gt; toList()" ID="ID_481631092" CREATED="1632264148689" MODIFIED="1632264148689"/>
<node TEXT="Collector&lt;T,?,Set&lt;T&gt;&gt; toSet()" ID="ID_315109432" CREATED="1632264148690" MODIFIED="1632264148690"/>
<node TEXT="Collector&lt;T,?,Map&lt;K,U&gt;&gt; toMap(Function&lt;? super T,? extends K&gt; keyMapper, Function&lt;? super T,? extends U&gt; valueMapper)" ID="ID_1462510806" CREATED="1632264148690" MODIFIED="1632264148690"/>
<node TEXT="Collector&lt;T,?,Map&lt;K,U&gt;&gt; toMap(Function&lt;? super T,? extends K&gt; keyMapper, Function&lt;? super T,? extends U&gt; valueMapper, BinaryOperator&lt;U&gt; mergeFunction)" ID="ID_680873423" CREATED="1632264148691" MODIFIED="1632264148691"/>
<node TEXT="Collector&lt;T,?,M&gt; toMap(Func&lt;? super T,? extends K&gt; keyMapper, Func&lt;? super T,? extends U&gt; valueMapper, BinaryOperator&lt;U&gt; mergeFunction, Supplier&lt;M&gt; mapFactory)" ID="ID_1618085432" CREATED="1632264148692" MODIFIED="1632264564958"/>
<node TEXT="Collector&lt;T,?,List&lt;T&gt;&gt; toUnmodifiableList()" ID="ID_588625904" CREATED="1632264148692" MODIFIED="1632264148692"/>
<node TEXT="Collector&lt;T,?,Set&lt;T&gt;&gt; toUnmodifiableSet()" ID="ID_358189394" CREATED="1632264148693" MODIFIED="1632264148693"/>
<node TEXT="Collector&lt;T,?,Map&lt;K,U&gt;&gt; toUnmodifiableMap(Function&lt;? super T,? extends K&gt; keyMapper, Function&lt;? super T,? extends U&gt; valueMapper)" ID="ID_1715895083" CREATED="1632264148693" MODIFIED="1632264148693"/>
<node TEXT="Collector&lt;T,?,Map&lt;K,U&gt;&gt; toUnmodifiableMap(Function&lt;? super T,? extends K&gt; keyMapper, Function&lt;? super T,? extends U&gt; valueMapper, BinaryOperator&lt;U&gt; mergeFunction)" ID="ID_91678931" CREATED="1632264148693" MODIFIED="1632264148693"/>
<node TEXT="Collector&lt;T,?,ConcurrentMap&lt;K,U&gt;&gt; toConcurrentMap(Function&lt;? super T,? extends K&gt; keyMapper, Function&lt;? super T,? extends U&gt; valueMapper)" ID="ID_830171137" CREATED="1632264148693" MODIFIED="1632264148693"/>
<node TEXT="Collector&lt;T,?,ConcurrentMap&lt;K,U&gt;&gt; toConcurrentMap(Func&lt;? super T,? extends K&gt; keyMapper, Func&lt;? super T,? extends U&gt; valueMapper, BinaryOperator&lt;U&gt; mergeFunc)" ID="ID_1452186025" CREATED="1632264148693" MODIFIED="1632264586333"/>
<node TEXT="Collector&lt;T,?,M&gt; toConcurrentMap(Func&lt;? super T,? extends K&gt; keyMapper, Func&lt;? super T,? extends U&gt; valueMapper, BinaryOp&lt;U&gt; mergeFunc, Supplier&lt;M&gt; mapFactory)" ID="ID_618284665" CREATED="1632264148693" MODIFIED="1632264610172"/>
<node TEXT="Collector&lt;T,A,RR&gt; collectingAndThen(Collector&lt;T,A,R&gt; downstream, Function&lt;R,RR&gt; finisher)" ID="ID_1454345528" CREATED="1632264148694" MODIFIED="1632264148694"/>
</node>
<node TEXT="manipulate" POSITION="right" ID="ID_732194253" CREATED="1632263936868" MODIFIED="1632307252849" VGAP_QUANTITY="25.5 pt" HGAP_QUANTITY="-133 pt" VSHIFT_QUANTITY="124.5 pt">
<node ID="ID_1713130443" CREATED="1632307031630" MODIFIED="1632307031630"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>R</b>&gt; <b>filtering</b>(Predicate&lt;? super T&gt; predicate, Collector&lt;? super T,A,R&gt; downstream)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_624878474" CREATED="1632307031630" MODIFIED="1632307031630"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;CharSequence,?,<b>String</b>&gt; <b>joining</b>()
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1130702446" CREATED="1632307031631" MODIFIED="1632307031631"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;CharSequence,?,<b>String</b>&gt; <b>joining</b>(CharSequence delimiter)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1832764922" CREATED="1632307031632" MODIFIED="1632307031632"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;CharSequence,?,<b>String</b>&gt; <b>joining</b>(CharSequence delimiter, CharSequence prefix, CharSequence suffix)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_511660094" CREATED="1632307031633" MODIFIED="1632307031633"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>R</b>&gt; <b>mapping</b>(Function&lt;? super T,? extends U&gt; mapper, Collector&lt;? super U,A,R&gt; downstream)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_274402488" CREATED="1632307031633" MODIFIED="1632307234004"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>R</b>&gt; <b>flatMapping</b>(Function&lt;? super T,? extends Stream&lt;? extends U&gt;&gt; mapper, Coll'&lt;? super U,A,R&gt; dwnstrm)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_294362570" CREATED="1632307031634" MODIFIED="1632307031634"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>Optional&lt;T&gt;</b>&gt; <b>reducing</b>(BinaryOperator&lt;T&gt; op)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_382131320" CREATED="1632307031635" MODIFIED="1632307031635"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>T</b>&gt; <b>reducing</b>(T identity, BinaryOperator&lt;T&gt; op)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1169336796" CREATED="1632307031636" MODIFIED="1632307031636"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Collector&lt;T,?,<b>U</b>&gt; <b>reducing</b>(U identity, Function&lt;? super T,? extends U&gt; mapper, BinaryOperator&lt;U&gt; op)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</map>
