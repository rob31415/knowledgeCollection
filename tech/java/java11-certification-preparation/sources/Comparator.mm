<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Comparator&#xa;&lt;T input element&gt;" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="Freemind_Link_1513112588" CREATED="1153430895318" MODIFIED="1632314982577" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" VGAP_QUANTITY="3 pt">
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<hook NAME="MapStyle" background="#ffffff" zoom="0.827">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_1102549087" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#2b0053" BACKGROUND_COLOR="#ff4fdb" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#585858" WIDTH="2" TRANSPARENCY="255" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1102549087" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="11" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#8001dd" WIDTH="3" DASH="SOLID"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#ff4fdb" BACKGROUND_COLOR="#2b0053">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffff9e" TEXT_ALIGN="LEFT">
<icon BUILTIN="clock2"/>
<font SIZE="10" ITALIC="true"/>
<edge COLOR="#ffec56"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#2b0053" BACKGROUND_COLOR="#ffffff" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#fd004c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" COLOR="#ffffff" BACKGROUND_COLOR="#fd004c">
<icon BUILTIN="yes"/>
<font SIZE="12" BOLD="true"/>
<edge COLOR="#fd004c"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#8001dd" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_WIDTH="3.1 px" BORDER_DASH_LIKE_EDGE="true">
<font SIZE="18"/>
<edge COLOR="#ff4fdb"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#ffffff" BACKGROUND_COLOR="#a120f2" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#ffffff" BACKGROUND_COLOR="#c100fe" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" MAX_WIDTH="50 cm">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#282828" BACKGROUND_COLOR="#dd00fd" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#282828" BACKGROUND_COLOR="#ff4fdb">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" BACKGROUND_COLOR="#ff4fdb">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BACKGROUND_COLOR="#ff4fdb">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BACKGROUND_COLOR="#ff4fdb">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BACKGROUND_COLOR="#ff4fdb">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="comp" POSITION="right" ID="ID_1813793149" CREATED="1632299312731" MODIFIED="1632315781072" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="20.75 pt" VSHIFT_QUANTITY="-50.25 pt">
<node ID="ID_771468642" CREATED="1632315769782" MODIFIED="1632315781071" VSHIFT_QUANTITY="-10.5 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      int <b>compare</b>(T o1, T o2)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_225599327" CREATED="1632315769782" MODIFIED="1632315769782"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T,<b>U extends Comparable</b>&lt;? super U&gt;&gt; Comparator&lt;T&gt; <b>comparing</b>(Function&lt;? super T,? <b>extends U</b>&gt; keyExtractor)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1996917466" CREATED="1632315769783" MODIFIED="1632315778211" VSHIFT_QUANTITY="-14.25 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T,U&gt; Comparator&lt;T&gt; <b>comparing</b>(Function&lt;? super T,? extends U&gt; keyExtractor, Comparator&lt;? super U&gt; keyComparator)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1159557013" CREATED="1632315769785" MODIFIED="1632315769785"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      Comparator&lt;T&gt; <b>thenComparing</b>(Comparator&lt;? super T&gt; other)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1204995326" CREATED="1632315769787" MODIFIED="1632315769787"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;U extends Comparable&lt;? super U&gt;&gt; Comparator&lt;T&gt; <b>thenComparing</b>(Function&lt;? super T,? extends U&gt; keyExtractor)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1481611578" CREATED="1632315769788" MODIFIED="1632315769788"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;U&gt; Comparator&lt;T&gt; <b>thenComparing</b>(Function&lt;? super T,? extends U&gt; keyExtractor, Comparator&lt;? super U&gt; keyComparator)
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="primitives" POSITION="left" ID="ID_1390646942" CREATED="1632299330696" MODIFIED="1632315866294" VGAP_QUANTITY="3 pt" HGAP_QUANTITY="15.5 pt" VSHIFT_QUANTITY="-50.25 pt">
<node ID="ID_1601855646" CREATED="1632315706206" MODIFIED="1632315706206"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T&gt; Comparator&lt;T&gt; <b>comparingDouble</b>(ToDoubleFunction&lt;? super T&gt; keyExtractor)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_823000351" CREATED="1632315706207" MODIFIED="1632315706207"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T&gt; Comparator&lt;T&gt; <b>comparingInt</b>(ToIntFunction&lt;? super T&gt; keyExtractor)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1657734757" CREATED="1632315706208" MODIFIED="1632315866288" VSHIFT_QUANTITY="-12 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T&gt; Comparator&lt;T&gt; <b>comparingLong</b>(ToLongFunction&lt;? super T&gt; keyExtractor)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_930353141" CREATED="1632315706210" MODIFIED="1632315706210"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      Comparator&lt;T&gt; thenComparingDouble(ToDoubleFunction&lt;? super T&gt; keyExtractor)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_873059249" CREATED="1632315706211" MODIFIED="1632315706211"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      Comparator&lt;T&gt; thenComparingInt(ToIntFunction&lt;? super T&gt; keyExtractor)
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_1121035196" CREATED="1632315706212" MODIFIED="1632315706212"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      Comparator&lt;T&gt; thenComparingLong(ToLongFunction&lt;? super T&gt; keyExtractor)
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="order" POSITION="right" ID="ID_771462310" CREATED="1632299378317" MODIFIED="1632299379763">
<node ID="ID_1240986147" CREATED="1632315792935" MODIFIED="1632315792935"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T extends Comparable&lt;? super T&gt;&gt; Comparator&lt;T&gt; naturalOrder()
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_358304434" CREATED="1632315792935" MODIFIED="1632315792935"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T extends Comparable&lt;? super T&gt;&gt; Comparator&lt;T&gt; reverseOrder()
    </p>
  </body>
</html>

</richcontent>
</node>
<node ID="ID_425927922" CREATED="1632315792937" MODIFIED="1632315792937"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      Comparator&lt;T&gt; reversed()
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="nulls" POSITION="left" ID="ID_1192928875" CREATED="1632299390999" MODIFIED="1632299488975" VGAP_QUANTITY="3 pt">
<node ID="ID_816911653" CREATED="1632315727629" MODIFIED="1632315727629"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T&gt; Comparator&lt;T&gt; nullsFirst(Comparator&lt;? super T&gt; comparator)
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="null&lt;non-null" ID="ID_305842721" CREATED="1632299393507" MODIFIED="1632299488975" HGAP_QUANTITY="14.75 pt" VSHIFT_QUANTITY="0.75 pt"/>
<node ID="ID_613282019" CREATED="1632315727629" MODIFIED="1632315727629"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="margin-bottom: 0in; line-height: 100%">
      &lt;T&gt; Comparator&lt;T&gt; nullsLast(Comparator&lt;? super T&gt; comparator)
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="null&gt;non-null" ID="ID_1544635624" CREATED="1632299393510" MODIFIED="1632299393510"/>
</node>
</node>
</map>
