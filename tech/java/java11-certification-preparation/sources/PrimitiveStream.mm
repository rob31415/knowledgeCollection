<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="IntStream" FOLDED="false" ID="ID_1775383331" CREATED="1632261455888" MODIFIED="1632999576807" STYLE="oval"><hook NAME="MapStyle" zoom="0.902">
    <conditional_styles>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="styles.connection" LAST="false">
            <node_periodic_level_condition PERIOD="2" REMAINDER="1"/>
        </conditional_style>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="styles.topic" LAST="false">
            <node_level_condition VALUE="2" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="styles.subtopic" LAST="false">
            <node_level_condition VALUE="4" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
        <conditional_style ACTIVE="true" LOCALIZED_STYLE_REF="styles.subsubtopic" LAST="false">
            <node_level_condition VALUE="6" COMPARATION_RESULT="0" SUCCEED="true"/>
        </conditional_style>
    </conditional_styles>
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_339742628" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_339742628" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="Arial" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork" MAX_WIDTH="50 cm">
<font NAME="DejaVu Sans Mono" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.connection" COLOR="#606060" STYLE="fork" MAX_WIDTH="50 cm">
<font NAME="Source Code Pro" SIZE="10" BOLD="false"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="create" POSITION="left" ID="ID_1729493053" CREATED="1632261723881" MODIFIED="1632999033283" HGAP_QUANTITY="9.5 pt" VSHIFT_QUANTITY="-2.25 pt">
<font SIZE="14" BOLD="true"/>
<node TEXT="static IntStream of(int t)" ID="ID_587001981" CREATED="1632998761325" MODIFIED="1632998761325"/>
<node TEXT="static IntStream of(int... values)" ID="ID_956621680" CREATED="1632998761325" MODIFIED="1632999002015" HGAP_QUANTITY="13.25 pt" VSHIFT_QUANTITY="-14.25 pt"/>
<node TEXT="static IntStream generate(IntSupplier s)" ID="ID_53875250" CREATED="1632998761326" MODIFIED="1632998761326"/>
<node TEXT="static IntStream empty()" ID="ID_699598899" CREATED="1632998761327" MODIFIED="1632999006849" HGAP_QUANTITY="15.5 pt" VSHIFT_QUANTITY="-14.25 pt"/>
<node TEXT="static IntStream iterate(int seed, IntPredicate hasNext, IntUnaryOperator next)" ID="ID_1225795582" CREATED="1632998761328" MODIFIED="1632998761328"/>
<node TEXT="static IntStream iterate(int seed, IntUnaryOperator f)" ID="ID_1357716596" CREATED="1632998761328" MODIFIED="1632999016906" VSHIFT_QUANTITY="-12.75 pt"/>
<node TEXT="static IntStream concat(IntStream a, IntStream b)" ID="ID_1816465332" CREATED="1632998761329" MODIFIED="1632998761329"/>
<node TEXT="int[] toArray()" ID="ID_1382559397" CREATED="1632998761329" MODIFIED="1632999024975" HGAP_QUANTITY="15.5 pt" VSHIFT_QUANTITY="-10.5 pt"/>
<node TEXT="static IntStream range(int startInclusive, int endExclusive)" ID="ID_921110068" CREATED="1632998761330" MODIFIED="1632999041244" COLOR="#cc0033"/>
<node TEXT="static IntStream rangeClosed(int startInclusive, int endInclusive)" ID="ID_697053909" CREATED="1632998761330" MODIFIED="1632999041243" HGAP_QUANTITY="14.75 pt" VSHIFT_QUANTITY="-8.25 pt" COLOR="#cc0033"/>
<node TEXT="DoubleStream asDoubleStream()" ID="ID_36131275" CREATED="1632998761331" MODIFIED="1632999041243" COLOR="#cc0033"/>
<node TEXT="LongStream asLongStream()" ID="ID_1737330358" CREATED="1632998761331" MODIFIED="1632999041242" COLOR="#cc0033"/>
<node TEXT="Stream&lt;Integer&gt; boxed()" ID="ID_404598640" CREATED="1632998761332" MODIFIED="1632999041242" VSHIFT_QUANTITY="-8.25 pt" COLOR="#cc0033"/>
<node TEXT="static IntStream.Builder builder()" ID="ID_76365316" CREATED="1632998761332" MODIFIED="1632999041239" COLOR="#cc0033"/>
</node>
<node TEXT="iterate" POSITION="right" ID="ID_7139167" CREATED="1632261754300" MODIFIED="1632999576805" HGAP_QUANTITY="42.5 pt" VSHIFT_QUANTITY="-77.25 pt">
<font SIZE="14" BOLD="true"/>
<node TEXT="void forEach(IntConsumer action)" ID="ID_1086083565" CREATED="1632998776656" MODIFIED="1632998776656"/>
<node TEXT="void forEachOrdered(IntConsumer action)" ID="ID_970302504" CREATED="1632998776656" MODIFIED="1632998776656"/>
<node TEXT="IntStream peek(IntConsumer action)" ID="ID_1470676856" CREATED="1632998776656" MODIFIED="1632998776656"/>
</node>
<node TEXT="query" POSITION="right" ID="ID_1789282150" CREATED="1632261790363" MODIFIED="1632998866725" HGAP_QUANTITY="50 pt" VSHIFT_QUANTITY="31.5 pt">
<font SIZE="14" BOLD="true"/>
<node TEXT="boolean allMatch(IntPredicate predicate)" ID="ID_1193381714" CREATED="1632998792957" MODIFIED="1632998792957"/>
<node TEXT="boolean anyMatch(IntPredicate predicate)" ID="ID_1685040966" CREATED="1632998792957" MODIFIED="1632998792957"/>
<node TEXT="boolean noneMatch(IntPredicate predicate)" ID="ID_856702296" CREATED="1632998792959" MODIFIED="1632998853909" VSHIFT_QUANTITY="-11.25 pt"/>
<node TEXT="OptionalInt max()" ID="ID_1972944466" CREATED="1632998792960" MODIFIED="1632998792960"/>
<node TEXT="OptionalInt min()" ID="ID_1377420882" CREATED="1632998792960" MODIFIED="1632998856009" VSHIFT_QUANTITY="-9.75 pt"/>
<node TEXT="OptionalInt findAny()" ID="ID_190920500" CREATED="1632998792961" MODIFIED="1632998792961"/>
<node TEXT="OptionalInt findFirst()" ID="ID_852968741" CREATED="1632998792961" MODIFIED="1632998860665" HGAP_QUANTITY="13.25 pt" VSHIFT_QUANTITY="-13.5 pt"/>
<node TEXT="IntStream filter(IntPredicate predicate)" ID="ID_497837447" CREATED="1632998792961" MODIFIED="1632998863299" VSHIFT_QUANTITY="-8.25 pt"/>
<node TEXT="long count()" ID="ID_1832822297" CREATED="1632998792962" MODIFIED="1632998866723" VSHIFT_QUANTITY="-15 pt"/>
<node TEXT="OptionalDouble average()" ID="ID_214829848" CREATED="1632998792962" MODIFIED="1632998886857" COLOR="#cc0033"/>
<node TEXT="int sum()" ID="ID_1039818974" CREATED="1632998792962" MODIFIED="1632998886860" COLOR="#cc0033"/>
<node TEXT="IntSummaryStatistics summaryStatistics()" ID="ID_856552327" CREATED="1632998792963" MODIFIED="1632998886861" COLOR="#cc0033"/>
</node>
<node TEXT="transform" POSITION="left" ID="ID_122139294" CREATED="1632261807109" MODIFIED="1632999570584" HGAP_QUANTITY="0.5 pt" VSHIFT_QUANTITY="28.5 pt">
<font SIZE="14" BOLD="true"/>
<node TEXT="manipulate" ID="ID_176231169" CREATED="1632261983238" MODIFIED="1632262946189" HGAP_QUANTITY="-42.25 pt" VSHIFT_QUANTITY="-13.5 pt">
<font BOLD="false"/>
<node TEXT="default IntStream takeWhile(IntPredicate predicate)" ID="ID_831968613" CREATED="1632998832568" MODIFIED="1632998832568"/>
<node TEXT="default IntStream dropWhile(IntPredicate predicate)" ID="ID_1130107445" CREATED="1632998832568" MODIFIED="1632998832568"/>
<node TEXT="IntStream limit(long maxSize)" ID="ID_1980894137" CREATED="1632998832570" MODIFIED="1632998832570"/>
<node TEXT="IntStream skip(long n)" ID="ID_1311588058" CREATED="1632998832570" MODIFIED="1632998832570"/>
<node TEXT="IntStream distinct()" ID="ID_834260174" CREATED="1632998832571" MODIFIED="1632998832571"/>
<node TEXT="" ID="ID_1544772256" CREATED="1632999503582" MODIFIED="1632999503583">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="int reduce(int identity, IntBinaryOperator op)" ID="ID_298960467" CREATED="1632998832572" MODIFIED="1632998832572"/>
<node TEXT="OptionalInt reduce(IntBinaryOperator op)" ID="ID_46157092" CREATED="1632998832574" MODIFIED="1632998832574"/>
<node TEXT="" ID="ID_1749414102" CREATED="1632999503580" MODIFIED="1632999503582">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="not 3; no accu" ID="ID_1275156145" CREATED="1632999503584" MODIFIED="1632999516054"/>
</node>
<node TEXT="" ID="ID_784158244" CREATED="1632999402712" MODIFIED="1632999402712">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="IntStream sorted()" ID="ID_1884156775" CREATED="1632998832575" MODIFIED="1632998832575"/>
<node TEXT="" ID="ID_778924879" CREATED="1632999402703" MODIFIED="1632999428352">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="only w/o Comparator" ID="ID_1081681960" CREATED="1632999402713" MODIFIED="1632999428350" HGAP_QUANTITY="184.24999 pt" VSHIFT_QUANTITY="0.75 pt"/>
</node>
</node>
<node TEXT="map" ID="ID_729638496" CREATED="1632261843393" MODIFIED="1632262946192" HGAP_QUANTITY="16.25 pt" VSHIFT_QUANTITY="-20.25 pt">
<font BOLD="false"/>
<node TEXT="IntStream map(IntUnaryOperator mapper)" ID="ID_1956359637" CREATED="1632998820682" MODIFIED="1632998820682"/>
<node TEXT="DoubleStream mapToDouble(IntToDoubleFunction mapper)" ID="ID_1186235397" CREATED="1632998820682" MODIFIED="1632998820682"/>
<node TEXT="LongStream mapToLong(IntToLongFunction mapper)" ID="ID_1392929203" CREATED="1632998820682" MODIFIED="1632998820682"/>
<node TEXT="&lt;U&gt; Stream&lt;U&gt; mapToObj(IntFunction&lt;? extends U&gt; mapper)" ID="ID_1047236808" CREATED="1632998820682" MODIFIED="1632998820682"/>
</node>
<node TEXT="flatMap" ID="ID_2884376" CREATED="1632261852033" MODIFIED="1632262946192" HGAP_QUANTITY="17 pt" VSHIFT_QUANTITY="-13.5 pt">
<font BOLD="false"/>
<node TEXT="IntStream flatMap(IntFunction&lt;? extends IntStream&gt; mapper)" ID="ID_1040491088" CREATED="1632998812526" MODIFIED="1632998812526"/>
</node>
<node TEXT="terminate" ID="ID_408770990" CREATED="1632261917732" MODIFIED="1632999570584" HGAP_QUANTITY="-143.5 pt" VSHIFT_QUANTITY="-8.25 pt">
<font BOLD="false"/>
<node TEXT="&lt;R&gt; R collect(Supplier&lt;R&gt; supplier, ObjIntConsumer&lt;R&gt; accumulator, BiConsumer&lt;R,R&gt; combiner)" ID="ID_79235859" CREATED="1632998803573" MODIFIED="1632998803573"/>
</node>
</node>
</node>
</map>
