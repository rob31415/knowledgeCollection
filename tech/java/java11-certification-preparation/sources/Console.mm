<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Console" FOLDED="false" ID="ID_696401721" CREATED="1610381621824" MODIFIED="1632142119059" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="9" RULE="ON_BRANCH_CREATION"/>
<node TEXT="void flush()" POSITION="right" ID="ID_994585424" CREATED="1632142078117" MODIFIED="1632142099610" HGAP_QUANTITY="12.5 pt" VSHIFT_QUANTITY="-24.75 pt">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="Console format​(String fmt, Object... args)" POSITION="right" ID="ID_1072973994" CREATED="1632142078117" MODIFIED="1632142078122">
<edge COLOR="#0000ff"/>
</node>
<node TEXT="Console printf​(String format, Object... args)" POSITION="right" ID="ID_129033525" CREATED="1632142078121" MODIFIED="1632142078123">
<edge COLOR="#00ff00"/>
</node>
<node TEXT="String readLine()" POSITION="right" ID="ID_1823294700" CREATED="1632142078123" MODIFIED="1632142105018" HGAP_QUANTITY="14.75 pt" VSHIFT_QUANTITY="20.25 pt">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="String readLine​(String fmt, Object... args)" POSITION="right" ID="ID_627280649" CREATED="1632142078123" MODIFIED="1632142078124">
<edge COLOR="#7c0000"/>
</node>
<node TEXT="char[] readPassword()" POSITION="right" ID="ID_1646068075" CREATED="1632142078123" MODIFIED="1632142119057" VSHIFT_QUANTITY="16.5 pt">
<edge COLOR="#00007c"/>
</node>
<node TEXT="char[] readPassword​(String fmt, Object... args)" POSITION="right" ID="ID_155009752" CREATED="1632142078124" MODIFIED="1632142078125">
<edge COLOR="#007c00"/>
</node>
<node TEXT="Reader reader()" POSITION="left" ID="ID_532053444" CREATED="1632142078122" MODIFIED="1632142109137" HGAP_QUANTITY="11 pt" VSHIFT_QUANTITY="-36 pt">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="PrintWriter writer()" POSITION="left" ID="ID_157189907" CREATED="1632142078124" MODIFIED="1632142113057" HGAP_QUANTITY="11 pt" VSHIFT_QUANTITY="23.25 pt">
<edge COLOR="#7c007c"/>
</node>
</node>
</map>
