<map version="freeplane 1.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="ArrayList" FOLDED="false" ID="ID_696401721" CREATED="1610381621824" MODIFIED="1632992292151" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="1.2">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="DEFAULT" ENDARROW="NONE"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
<richcontent CONTENT-TYPE="plain/auto" TYPE="DETAILS"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" STYLE="bubble" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#003399" TRANSPARENCY="255" DESTINATION="ID_67550811"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="9" RULE="ON_BRANCH_CREATION"/>
<node TEXT="create" POSITION="left" ID="ID_435060985" CREATED="1632991902043" MODIFIED="1632992138705" HGAP_QUANTITY="17.75 pt" VSHIFT_QUANTITY="-20.25 pt">
<edge COLOR="#0000ff"/>
<node TEXT="ArrayList()" ID="ID_1189489008" CREATED="1632991915001" MODIFIED="1632991915001"/>
<node TEXT="ArrayList​(int initialCapacity)" ID="ID_1026954378" CREATED="1632991915001" MODIFIED="1632991915001"/>
<node TEXT="ArrayList​(Collection&lt;? extends E&gt; c)" ID="ID_1579120857" CREATED="1632991915002" MODIFIED="1632991915002"/>
<node TEXT="Object clone()" ID="ID_1648500134" CREATED="1632991915003" MODIFIED="1632991915003"/>
<node TEXT="Object[] toArray()" ID="ID_1834174372" CREATED="1632991915004" MODIFIED="1632991915004"/>
<node TEXT="&lt;T&gt; T[] toArray​(T[] a)" ID="ID_577314381" CREATED="1632991915004" MODIFIED="1632991915004"/>
</node>
<node TEXT="delete" POSITION="left" ID="ID_672924512" CREATED="1632992044563" MODIFIED="1632992292149" HGAP_QUANTITY="18.5 pt" VSHIFT_QUANTITY="-24.75 pt">
<edge COLOR="#7c007c"/>
<node TEXT="E remove​(int index)" ID="ID_1795479975" CREATED="1632992048123" MODIFIED="1632992048123"/>
<node TEXT="boolean remove​(Object o)" ID="ID_402508735" CREATED="1632992048123" MODIFIED="1632992048123"/>
<node TEXT="boolean removeAll​(Collection&lt;?&gt; c)" ID="ID_964866089" CREATED="1632992048124" MODIFIED="1632992048124"/>
<node TEXT="boolean removeIf​(Predicate&lt;? super E&gt; filter)" ID="ID_1535125125" CREATED="1632992048125" MODIFIED="1632992048125"/>
<node TEXT="protected void removeRange​(int fromIndex, int toIndex)" ID="ID_1070768484" CREATED="1632992048126" MODIFIED="1632992048126"/>
<node TEXT="boolean retainAll​(Collection&lt;?&gt; c)" ID="ID_1729683114" CREATED="1632992048128" MODIFIED="1632992048128"/>
</node>
<node TEXT="get" POSITION="right" ID="ID_646191287" CREATED="1632991928355" MODIFIED="1632992186013" HGAP_QUANTITY="5 pt" VSHIFT_QUANTITY="-24 pt">
<edge COLOR="#00ff00"/>
<node TEXT="E get​(int index)" ID="ID_856176965" CREATED="1632991933933" MODIFIED="1632991933933"/>
<node TEXT="List&lt;E&gt; subList​(int fromIndex, int toIndex)" ID="ID_1239072652" CREATED="1632991933933" MODIFIED="1632992156868" HGAP_QUANTITY="17 pt" VSHIFT_QUANTITY="-14.25 pt"/>
</node>
<node TEXT="query" POSITION="right" ID="ID_187544825" CREATED="1632991958924" MODIFIED="1632992181664" HGAP_QUANTITY="21.5 pt" VSHIFT_QUANTITY="-7.5 pt">
<edge COLOR="#00ffff"/>
<node TEXT="boolean contains​(Object o)" ID="ID_1593959296" CREATED="1632991960609" MODIFIED="1632991960609"/>
<node TEXT="int indexOf​(Object o)" ID="ID_77028448" CREATED="1632991960609" MODIFIED="1632991960609"/>
<node TEXT="int lastIndexOf​(Object o)" ID="ID_908752519" CREATED="1632991960610" MODIFIED="1632991960610"/>
<node TEXT="boolean isEmpty()" ID="ID_537384222" CREATED="1632991960610" MODIFIED="1632991960610"/>
<node TEXT="int size()" ID="ID_1674243951" CREATED="1632991960611" MODIFIED="1632991960611"/>
</node>
<node TEXT="iterate" POSITION="right" ID="ID_1029625462" CREATED="1632991940319" MODIFIED="1632992180110" HGAP_QUANTITY="11 pt" VSHIFT_QUANTITY="9 pt">
<edge COLOR="#ff00ff"/>
<node TEXT="void forEach​(Consumer&lt;? super E&gt; action)" ID="ID_545430481" CREATED="1632991942344" MODIFIED="1632991942344"/>
<node TEXT="Iterator&lt;E&gt; iterator()" ID="ID_1693351198" CREATED="1632991942344" MODIFIED="1632991942344"/>
<node TEXT="ListIterator&lt;E&gt; listIterator()" ID="ID_1149857450" CREATED="1632991942346" MODIFIED="1632991942346"/>
<node TEXT="ListIterator&lt;E&gt; listIterator​(int index)" ID="ID_1636245735" CREATED="1632991942347" MODIFIED="1632991942347"/>
<node TEXT="Spliterator&lt;E&gt; spliterator()" ID="ID_1168188744" CREATED="1632991942347" MODIFIED="1632991942347"/>
</node>
<node TEXT="set" POSITION="left" ID="ID_134342410" CREATED="1632992021844" MODIFIED="1632992289133" HGAP_QUANTITY="65.75 pt" VSHIFT_QUANTITY="-7.5 pt">
<edge COLOR="#00007c"/>
<node TEXT="E set​(int index, E element)" ID="ID_543956559" CREATED="1632992037188" MODIFIED="1632992037188"/>
</node>
<node TEXT="mod" POSITION="left" ID="ID_1269480948" CREATED="1632992010682" MODIFIED="1632992282862" HGAP_QUANTITY="21.5 pt" VSHIFT_QUANTITY="15.75 pt">
<edge COLOR="#7c0000"/>
<node TEXT="void trimToSize()" ID="ID_1590103714" CREATED="1632992016183" MODIFIED="1632992016183"/>
<node TEXT="void ensureCapacity​(int minCapacity)" ID="ID_1039319068" CREATED="1632992016183" MODIFIED="1632992016183"/>
<node TEXT="void clear()" ID="ID_549259609" CREATED="1632992016185" MODIFIED="1632992016185"/>
</node>
<node TEXT="add" POSITION="right" ID="ID_1817219934" CREATED="1632992025257" MODIFIED="1632992178341" HGAP_QUANTITY="13.25 pt" VSHIFT_QUANTITY="25.5 pt">
<edge COLOR="#007c00"/>
<node TEXT="void add​(int index, E element)" ID="ID_829447713" CREATED="1632992031651" MODIFIED="1632992031651"/>
<node TEXT="boolean add​(E e)" ID="ID_471301839" CREATED="1632992031651" MODIFIED="1632992031651"/>
<node TEXT="boolean addAll​(int index, Collection&lt;? extends E&gt; c)" ID="ID_497772202" CREATED="1632992031653" MODIFIED="1632992031653"/>
<node TEXT="boolean addAll​(Collection&lt;? extends E&gt; c)" ID="ID_1169649454" CREATED="1632992031654" MODIFIED="1632992031654"/>
</node>
</node>
</map>
