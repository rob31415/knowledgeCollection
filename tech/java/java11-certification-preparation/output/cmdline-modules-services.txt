COMMANDLINE

    general
        javac, jvm, javadoc, jar, jdeps - all part of JDK
        kein JRE für 11

    javac
        classic: javac -d classDir pack1/myfile.java pack2/another.java
        module:  
            all files single
                javac -d classOutputDir -p/--module-path modDir pack1/myfile.java pack2/another.java
            finds/compiles all source files for moduleA:
                javac --module-source-path src -d outdir --module moduleA
            -d is required for --module-source-path

        --module-source-path    root dir for module source .java files
        --module                compile all the source files of a particular module (tandem w/ sourcepath)
        --module-path           for dependency (on .jar files)

    java
        classic:    java -cp/--class-path/-classpath classDir myPack.myClass
        singlefile: java SingleFileZoo.java Cleveland (can't import non-java libs)
        module:     java -p/--module-path modDir -m/--module mod.name/myPack.myClass
        "single-file source-code" since java11: java Blabla.java
        --add-exports is discouraged

        --list-modules (list all available modules)
        java.base@11.0.2
        java.compiler@11.0.2 ...

    jar
        jar -cvf myNewFile.jar .
        jar --create --verbose --file myNewFile.jar .
        jar --create --verbose --file myNewFile.jar -C dirWithFilesToBeUsed .
        jar is the same for classic and module based apps

        this is not in the examn (but really important in real life):
            jar cfe some.jar maPack.EntryCLass *.class   put manifest in jar
            jar uf  to update
            jar cfm  to add specific manifest file

    jdeps
        jdeps --module-path -s/-summary  (only one -)
        for classes & packages
        lists dependecy modules and packages (with "-s" no packages)
        unlike describing a module, it looks at the code to tell what dependencies are actually used rather than simply declared.
        jdk.unsupported
            people ignored deprecation
            suggestion what to do: jdeps --jdk-internals some.jar  (or -jdkinternals)

    info about modules via cmd line:
        java -d/--describe-module --list-modules --show-module-resolution
        jar -d/--describe-module
        jdeps

    jmod
        is for jmod files (contains anything that cant go into a jar)
        options: list extract create hash describe (lechd)

    jlink
        creates executable, containing specified parts that would have been in the JRE
        create with jlink & jar; run with java

    options in general:
        --module -m             (for running a module with java)
        --module-path -p
        --describe-module -d    (except for javac, -d is compiletarget dir)
        --summary -s
        show-module-resolution  has no one-letter (for debugging via "java" command)
        list-modules            has no one-letter


JPMS

    hierarchy:
        module (module-info.java)
            package
                .class file

    generally:
        if there's "package myPack;" in .java file, it has to be in "myPack/" dir
        package methods access modifiers: private, package-private, protected, public
        module access modifiers: protected & public only if exported via module-info.java
        packages can be cyclic; mods can't be cyclic
        modules
            java.base
                module, available w/o import
                = "fundamental APIs" = "core packages"
            some more modules: java.desktop (for swing), logging, sql, xml
        a module exports packages, not single classes
        a module depends on modules, neither packages nor classes
        a modular jar can still be used like a regular jar (just put it in classpath)
        3 phases: compiletime, linktime, runtime
        internal platform implementation not accessible from outside
        --add-exports can export modules not in modules-info.java, but discouraged

    encapsulation on package level:
        make only one top level class public (and maybe nested classes), all other top levels package-private.
        this way that on class is the package's entry/API.

    module-info.java:

        module zoo.animal.care {
            exports zoo.animal.care.medical;                    // anyone can use this package
            exports zoo.animal.talks.content to zoo.staff;      // whitelist modules who can use this package
            requires transitive zoo.animal.feeding;             // if someone uses this module (care), they implicitly/automatically/transitively get feeding module
            provides zoo.staff.ZooApi with zoo.staff.ZooImpl;   // expose service (interface & concrete implementation) service provider does this.
            uses zoo.staff.ZooApi;                              // declare usage of a service (the service locator does this!)
            opens zoo.animal.talks.schedule;                    // allow package reflection to anyone
            opens zoo.animal.talks.media to zoo.staff;          // allow package reflection with whitelist modules
        }
        kurz: exports=package, requires=modules

        can also be emtpy, 0 bytes
        module no.exports.at.all {}     // main can still be run on this. "exports" is only for making packages available.
        must be in museum/staff for module named "module.staff" - always at the root of the module
        module-info.java is compiled into module-info.class by the compiler
        provides & with: error if specify same service more than once pro module-info.jar
            eg "provides someService" twice or "with SomeClass" twice

    3 different module types:
        unnamed module: on classpath (any module-info.java is ignored)
            do not export any packages to named or automatic modules
            can read from any JARs on the classpath or module path
        named module: on module path, has module-info.java
        automatic module: on module path, no module-info.java
            all packages are exported
            name is based off the filename of the JAR file, e.g. commons2‐x‐1.0.0‐SNAPSHOT.jar -> commons2.x
                version info goes away
                _ und $ und spezial chars wird zu .
                mehrere dots werden gemerged
                leading/trailing dots are removed
                nummern gehen weg (king‐arthur4.jar -> king.arthur)
            can't reference unnamed module

        access:
            CP        MOD
            | --yep->  |        except. java -cp ..  auto can access:
            | <-no--   |                    unnamed,other autos, named
        unnamed     auto 
                    named

    migration:
        jdeps
        determine order
        or top down migration (all at once from CP to MOD, adding module-info.jar 1-by-one)
        do bottom up (one-by-one from CP to MOD, adding module-info.jar) 
        use third party jars as automatic modules (don't convert to named yourself)

    naming rules for package and module names:
        "mod.name/myPack.myClass"
        same as for identifieres/variables (see Program Flow/identifiers)
        "." separates identifiers and rules apply to each identifier separately
            e.g. "com.amazing.1movierentals" is illegal
        
SERVICES

                req
    INTERFACE A ◄──── CONSUMER D
    ▲        ▲      /
    │   class \    / 
    │    ▲     \  /req
    req  │      \/
    │    │      /\
    │    uses  /  \
    │    │    /    req
    │        ▼      \
    LOCATOR B        PROVIDER C
    exports          provides "api" with "impl"

    service = interface + locator  (A + B)

    A service provider interface (you write this, just a plain old interface or abstract class)
    B service locator (you write this, using java's ServiceLoader class)
        ServiceLoader<My> sl = ServiceLoader.load(My.class)
        load() returns another ServiceLoader, typed and it has:
            Optional findFirst(), iterator (implements Iterable), stream()
        Stream<ServiceLoader.Provider<S>> services = sl.stream();
    C service provider (you write this, can be more than 1)
        someclass implements your Interface
        or has "public static provider()" returning an instance
        and put "provides with" into module-info.java
        this is a "break out" kind of thing - loosly coupled - just put them into your project, they'll be found by the locator
    D invoke from customer (you write this of course)
        using locator
    
    - if a service provider references other classes, they're considered part of the service (?provider)
    - changing interface doesn't always require recompilation of the provider, only sometimes
    - ServiceLoader has findFirst (just like Stream/PrimitiveStreams)
