meta
    phrasing? "legal" = runtime; "valid" = compile time

    "invariant": a certain condition that constrains the possible states
        an object can have. e.g. area has to be height*width at all times.

Data types
    numbers are not printed with suffix (eg. f, d, etc)
    System.out.println(null);           geht nicht
    System.out.println(iter.next());    geht schon, auch wenn iter.next()==null

    literal denotions can be lower and upper case (e.g. f F l L)
    Number is supertype of Integer
    char is unsigned, 2 bytes
    new Short(1)  doesn't work (1 is implicitly int, no int cons in Short)
        short=1 works though
    numeric literals non-10 bases:
        integer literals: 0b1100, O0123, 0xff
        double literal: 43e1

    compound operators apply casting automatically
    post++ verändert var direkt, aber "returnt" aktuellen wert, also ein ausdruck arbeitet an der stelle noch mit orignal-wert
    ++pre verändert direkt, returnt neuen wert
    ternaries can be nested w/o parens
    == for identity, "public boolean equals(Object o)" for equality
    'a' + 1  will print 98  (promo to int; 97+1)
    5/4  does integer division (=1)
    Double == Integer -> compiler error  vs.  Double == int -> ok
    there is Void (capital V !) which is an uninstantiable Class

    "var" is a reserved type, not a reserved word, and may be used for variable names

    Integer.*Value (byte/short/double...)
    boxing: int cannot be boxed with a Double

    string and numeric interactions
        assign numeric (int etc) to string doesn't work. yet, someInt+1+"" does
        printing string+int and so on works
        int+string and string+int is possible
        the + operator performs a string operation only when one or both its operands is/are strings
            + starts left, so 1+2+"3" = "33"
        parseInt(string), (string,radix), (charseq,begin,end,radix)
        Integer Integer.valueOf(string/int);  int Integer.parseInt(string)  both  NumberFormatException (runtime)
        .parseInt(12.3) NumberFormatException
        var x = new Integer("42"); works too (deprecated)
        Integer.toHexString()

    string
        string & stringbuilder have
             charAt
             substring(begin 2, end 1) StringIndexOutOfBoundsException
        only string has
            isBlank=only whitspaces?  isEmpty=length 0?  strip=whitespaces
            replace(char old,new) (target,withthis)  (regex, replace)
            concat, trim
        stringbuilder has
            new StringBuilder(string or capacity or CharSequence)
            .substring(start,optional end)    CharSequence .subSequence(s,e)
            indexof(str) (str,startFromIndex)
            append(CharSequence s, int startInCharSeq, int endICS)
            insert(int dstOffset, CharSequence s, int startInCharSeq, int endICS)
            replace(int start, int end, String withthis)
            delete(start, optinal end)
            .capacity  .length()
        can't init string w' char (String s = 'a'; doesnt work)
        StringBuffer is synchronized, StringBuilder is not
            don't override Object.equals()  (WTF!)

    Float.parseFloat
        string(""+Float.NEGATIVE_INFINITY) -> Float.NEGATIVE_INFINITY
        "-Infinity" -> Float.NEGATIVE_INFINITY
        "NaN" -> Float.NaN
        "bla" -> NumberFormatException

    Arithmetic Operator Rules:
        Operator Precedence Rule
            MUPSREBLTZ
            MUDLREBLTA: method invocation (. operator), unary (post,pre,other), punkt/dot, strich/line, relational, equal, (bit), logical (non sc., shortcirtuit), ternary, assignment
            (note: the B is irrelevant for examn and in wrong place, please ignore it)
            when all have equal precedence -> from left to right
        Operand Promotion Rule: at least to int (byte->short->int, char->int)
        Same Type Operand Rule: trivial (eg float+float = float)
        Mix Type Operand Rule: to wider (eg long + double = double)

    primitive types = int boolean; Wrapper = Integer, Boolean
    Wrappers are immutable & passed byvalue
    Wrappers have no is-a relationship between them
        int is-a long, but Integer is-not-a Long

    Math.round() double-long, float-int
    mod % works with float and double (0.5%0.3=0.2)

    Collection.forEach() vs Collection.stream().forEach(e->{modify e})
        you can never modify the references that make up the array
        but if they're mutable non-primitives/Objects, they can be changed via the ref (e.g "e.x+=1")

    conversions:

        i I d D
        i      y n
        I      y n
        d  y n
        D  n n

    no I to D.



Generics

    no primitive types allowed as generic types
    untyped generics are ??? TODO

    syntax:
        class X<T> {
            public <T> void someMethod() {} }
        method's T is different than class's T
        
    rule of covariant returns:  overriding generics
        given: T <- S   <<< means "subtype of"
        A<S>    <<<    A<? extends S>    <<<    A<? extends T>
        A<T>    <<<    A<? super T>      <<<    A<? super S>
        yet, List<S> is not a subtype of List<T>

        see also -- GenericOver.java --

    bounds:
        List<?> a = new ArrayList<String>();
        List<? extends Exception> a = new ArrayList<RuntimeException>(); 
        List<? super Exception> a = new ArrayList<Object>(); 
    generic bounded type params (not ?, but letters):
        only extends is valid, super isnt
        class P <T extends String> works, class P <T super String> doesn't

    collections:
        List<? extends T>   only add(null), only "T get()"      compiler can't know exact type
        List<? super T>     only add(T), only "Object get()"    compiler knows only T but not exactly which supertype
        List<?>             only add(null), only "Object get()" compiler can't know exact
        List (rawtype)      add anything, only "Object get()"

    inheritance: Type erasure of generic method parameters
        unlike arrays, generic collections are not reified, meaning all generic information is removed
            eg:  void m(Set<CharSequence> cs), void m(Set<String> s) are the same
        this causes illegal overload (in same class) or illegal override (when in subclass)
        exception to this rule: overriding method is allowed to erase the generic type specification
            eg: Set<Integer> can become Set

    static variables can't be generic, static methods and non-static methods can

Program Flow

    imports haben precedence, konkret>wildcard!

    import static java.lang.System.*;    out.println("Hello");

    identifieres (aka variables):
        begin with a letter $ or _
        not start with numbers
        _ alone is not allowed
        no reserved word
            "var" and classnames (eg "String") is not reserved, so ok as variable name!
        _ in nummern, nicht: am anfang, ende, neben .

    LVTI:
        var can NOT be initialized with a null
        var can NOT be used in a multiple variable assignment statement
        using localvar before definition = compiler error
        var bla = new ArrayList<>();  works, it's Object
        var as method parameter not allowed
        var k = System.out::println  INVALID - var not usable with method refs

    a method same name as constructor is allowed in Java, not in C++
    GC might not run, even at program end
    unreached code = compilererror

    switch
        jumps to the mark and executes right side(s) until "break;" or end }
        switch: integral type (byte,short,char too), string or enum. long,float,double,bool not! var is allowed.
        case must be compile-time constant (e.g. final int x=3;)
        case for enums "Element", not "MyEnum.Element"
        switch(null) -> NullPointerException

    for
        for(;;) condition evaluated before entering body
        for(int i=0, j=3; ...) works
        for(var k=0; k<x; k++){ } works
        for(var x:y) - y primitive array or Iterable (Set, Collection), not String*, Map
        break SOMELABEL;  only valid within the scope of the label SOMELABEL; (line or {})
    
    a label has to be before a codeblock. multiple lables are valid: "BLA: BLAA: {}"

    can't use just {1,2,3} with varargs; must be "new Integer[] {1,2,3}"

    "final" is allowed for method and lambda arguments

    assert some-bool-expr and optional ": some-string"
        java [ -enableassertions | -ea  ] [:<package name>"..." | :<class name> ]
    assert is a keyword

    "dangling else":
        if(false) if(false) {x} else {y}  -> will not execute x nor y - associated w innermost if


OO

    -- oo.txt --

Exceptions

    -- exceptions.txt --

    always check: checked exception handeled or declared?
    a method can declare ("throws") checked or unchecked and have empty implementation
    try-with-resources is guaranteed to call the close() (resource leak possible if exception occurs in close)
    multicatch:
        one var only  catch(Exception1 | Exception2 | Exception3 e)
        only unrelated classes (not in inheritance line) otherwise compiler error
    Unchecked exceptions can be thrown in any method w/o declaring or handling
    always check: unreachable code compiler error
        catch blocks are evaluated in order, so catching superclass before subclass
        catching a checked exception which can't potentially be thrown (TODO?)
    For checked, overridden (or impl from interface) method can declare same, subclass-exception or no exception at all.
    Exceptions can get lost, eg: catch(Exception e) {throw e;} finally {throw Y;}, e is lost.
        it's not added to the suppressed exceptions list of finally.
    finally > return; aber System.exit() > finally
    you can create custom checked and unchecked exceptions
    a method can throw both, checked and unchecked

    must impl 3 constructors: noarg, String, Throwable

    catch/finally
        try: catch and/or finally
        try-w-res: neither (+ effectively final)

    try: 
        t+c+f throw: t is caught, f wins, c ist lost
    try-w-res:
        t+close() throw: t wins, the other is suppressed
        t+close()+finally throw: f wins
    note - t = try block OR opening a resource

    always check: try-w-res  AutoCloseable <- Closeable - one of them implemented
        close() throws IOException
    Resources are closed at the end of the try block and before any catch or finally block.
    if there's "catch(someCheckedEx)" the code in try{} must potentially be able to throw someCheckedEx, otherwise compile error

    exceptions in init-blocks
        static init block
            only runtimeex'
            is wrapped in ExceptionInInitializerError
        instance init block
            runtimex
            checked ONLY when delclared in ALL constructors



Arrays and Collections

    arrays
        fixed size
        int array.length, int string.length()
        sqarebraces before and/or after name
        array of Objects inits all elements with null (String too)
        array initializer syntax
            boolean[] b2 = {true , false};
            var a = new int[]{ 1, 2, 3, 4 };
            int i[ ][ ] = { { 1, 2 }, new int[ 2 ] } ;
            with array initializer, specifying size [2] is invalid
        multidim
            each dim can have different length (array isn't a matrix)
            only 1st brace needs size (except if inited on declaration via initializer-list literal)
            LVTI can't use var on multidim arrays
        LVTI for anonymous array int[] x = {1,2,3};
        LVTI "int ids[], bla;"  is 2 different types;  "int[] ids,bla;" is one
        .equals() is not "deep", looks only at reference, not at elements
            is equivalent to ==
        cast to Object[] then to any-type might lead to Exception
        .clone()
        Array access precedence: getArray()[getIndex()], getArray first, then getIndex
             a [ (a = b)[3] ]  outer looks in original a[3], not in b[3], a = b happens afterwards
             iA[i] = i = 30 ;  original i will be used as well
             -> left hand operand first, operands before operation

    Arrays class
        myArray.toString()  "Ljava.lang..."
            not for other collections, they say "[1,2,3]"
        Arrays.toString(myArray)  "[1,2,3]"
        sort() is inplace
        .binarySearch(): negative nr; +1 and *-1 yields insert pos.   
        .mismatch(): first idx of diff or -1 (wenn match)
        .compare(a,b): 
                complete common prefix? len1-len2 : (string? char1-char2 : 1 or -1)
                ccp:  A={12}, B={1234}  -> -2
                not a ccp:  A={12}, B={147}  -> -1 (because 2<4)

    ArrayList is like StringBuilder for arrays
        equals compares all elements&order
        To be able to put a value at index n, the ArrayList must have atleast n values already or IndexOutOfBoundsException

    Array and list conversions:

                            modify  add/remove  view/copy  null?  literals
Arrays.asList(array)        yes     no          view       ?      no
*.toArray(
  opt. storageArray)        yes     no          view       ?      ?
  List,Set,Deque,Vector...
*.of(1..n)                  no      no          init       no     yes
    dates,numbers,coll's
    ...lots of stuff
*.copyOf(Collection)        no      no          copy       no     no
    Arrays,List,Set,Map
Collections
    .unmodifiableXXX()      no      no          view       ?      ?
    List,Set,Map,etc.


    Collection:
        addAll(Collection), new ..(other Collection)
        Collections.reverse(List)

    Map:
        .forEach((key, value)->... BiConsumer);  for (Map.Entry<K, V> entry : map.entrySet()) {}
        entrySet, values, keySet, put, putAll, putIfAbsetnt, remove, replace, containsKey, containsValue
        merge(k,v,fct(oldV,newV)) replace v via fct if k exists, set v otherwise or remove entry if fct returns null
        Interface Map.Entry  getKey, getValue, setValue, Comparator comparingByKey/Value(optional Comparator)
        HashMap supports null key & value; ConcurrentHashMap throws NullPointerException
        v = HashMap.put(k,v) if k exists, returns that existing v; otherwise null (after putting k,v) (or there was already a null key!)
        someMap = myMap.tailMap(fromKey, boolean inclusive)  someMap is backed by the original map myMap

    TreeMap:
        keys must be mutually comparable (putting Integer and Double doesn't work)

    List/Deque:
        head: push, pop, remove, poll, peek, addFirst, getFirst, offerFirst, peekFirst, pollFirst, removeFirst
        tail: add, offer, addLast, getLast, OfferLast, peekLast, pollLast, removeLast
        - just remember add & offer, the rest has either "Last" in it's name or else it's head

        add*/remove*/get* = throws   NoSuchElementException, IndexOutOfBoundsException or null/false
        offer/poll/peek = special value
        peek makes a copy of element, get "removes" element from stream

        Queue.remove() goes by object, not index
            removes 1st occurence; true=occured;false=not modified
        deque can act as a Queue (offer/add tail) as well as a Stack (push/pop head)
        List has sort(), deques doesn have sort
        List.foreach(SomeCLass::someMethod) works for "void someMethod()"
            some magic creates a Consumer, which calls w/o args, like {e -> someMethod();}
        List.add(el) , add(insertIdx,el)
    
    BlockingQueue
        put/take = blocks
        offer/poll = InterruptedException (checked)

    TreeSet:
        new TreeSet<Sorted>(Comparator)
        sets have .size()   (not count or length)
        .subSet(from,fromInclusive,to,toInclusive) backed by original. subset.add() allowed only within range.

    these are sorted!
        TreeSet, TreeMap
        ConcurrentSkipListSet, ConcurrentSkipListMap

    sort:
        numbers,uppercase,lowercase (ASCII)
        lang.Comparable/compareTo(other)  vs  util.Comparator/compare(a,b)
            static Comparator.comparing()
                class Employee {String name; getter;}  just a POJO
                Comparator<Employee> employeeNameComparator = Comparator.comparing(Employee::getName);
                Arrays.sort(employees, employeeNameComparator)
            Comparator.reversed()  everywhere else it's reverse()
        aufsteigende sortierung default
        Collections.sort(list); sort(list,Comparator) = List.sort(Comparator)
            if Comparator=null, Comparable.compareTo() is used
            sorting Object array w/ different types leads to ClassCastException,
              because e.g. String can't be compared to Double
        Bsp: a aa b bb c

    modifiable in extended for-loop: CopyOnWriteArrayList, Concurrent collections
    CopyOnWriteArrayList modification is atomic

functional interfaces

    SAM rule (single abstract method)
    .andThen(): Function, Comsumer (Bi* variants too)
    Function<T, R>  vs.  UnaryOperator<T>
        one converts type, unary doesn't

lambdas
    only allowed to reference effectively final variables
        just like inner classes! (lambdas become inner classes during compilation)
        like forEach(), if ref to mutable Object, it can be changed from inside the lambda
    parameters are not allowed to use the same name as another variable in the same scope
        aka lambdas cant shadow localvar
    declare same name vars afterwards is ok
    var for parameters is allowed
    a method reference is convenience syntax for a lambda
        will become a method body (Integer::toHexString)
        but not in a lambda: a -> Integer::toHexString
    a method reference cannot have an operator next to it
    a method reference cannot have arguments (e.g. bla::some(42))
    new X()::someMethod  is legal syntax
        it's a Reference to an instance method of a particular object

Streams

    elements are auto-iterated and fed through the pipeline, element by element

    always identify source, intermediate and terminal
        and be sure what they're doing

    there are numerous stream-bearing methods in the JDK
        collections: parallelStream() and stream()
        Stream.of(Object[]), IntStream.range(int, int)
        Stream.generate(Math::random)  vs.   Stream.iterate(1, n -> n + 2);
        files and lines in files, Random.ints(), etc..
    aggregate operations possible on stream:
        intermediate (return yet another stream, always lazy):
            filter, Stream map(Function) (lambda can return one value), 
            Stream flatMap(Function) (lambda can return multiple values as stream which is merged into the output stream), 
                List<List<String>> x = List.of( List.of("a"));
                x.stream().flatMap(e->e.stream());
                used only when each element of a given stream can itself generate a Stream of objects
                The objective of flatMap is to take each element of the current stream and replace that element with elements contained in the stream returned by the Function that is passed as an argument to flatMap.
            mapToObj, distinct, sorted (must impl Comparable, otherwise runtime ClassCastException), peek, limit, skip
        terminal (produce value- or side-effects):
            toArray, collect, long count, Stream forEach, forEachOrdered, min(Comparator), 
            max(Comparator -1,0,1), Optional findAny(), findFirst()  (ServiceLoader is the other class having findFirst)
            work only with Predicate & return boolean: anyMatch, allMatch, noneMatch (latter 2 are exhaustive)
            optional reduce(acc), T reduce(identity,acc), T reduce(id,acc,combiner)

    primitive stream: 
        contain primitives
        DoubleStream/IntStream/LongStream nothing else
        all have *Supplier functional interface
            DoubleSupplier = native  vs.  Supplier<Double> = generic
            *Supplier has getAsDouble() etc
            BooleanSupplier exists (yet no stream)
        DoubleStream.mapToObj() creates object value stream
        IntStream.mapToInt(ToIntFunction) creates a primitive stream
        .boxed() only exists for primitive streams (obviously)
            .groupingBy() needs non-primitives/Objects (ie .boxed())
        DoubleStream.average() returns OptionalDouble, sum() a primitive  (both exist for primitive streams only)
        OptionalDouble etc. have .getAsLong() etc;
        OptionalDouble - no elements = OptionalDouble.empty, not 0.0
        DoubleUnaryOperator exists
        double Collectors.averagingInt(toIntFct)/Long/Double

    primitive stream statistics
        IntStream ints;
        IntSummaryStatistics stat = ints.summaryStatistics();
        stat.getMin, getMax, getCount, getSum, getAverage


    Stream.of(1) containins single element, Stream.of(1,2,3) multiple, Collection.stream() converts into stream of elements
    not on examn: there's an ordering constraint on a stream pipeline (.unordered() lifts it)
    reduce(identity, accumulator, combiner)

    thenComparing:
        returns another Comparator
        3 variants: .thenComparing(comparator), 
            .thenComparing(Function keyExtractor) (keyEx must be Comparable)
            .thenComparing(Function keyExtractor, comparator) (
                the value returned by keyEx is compared using a given Comparator, useful if keyEx doesnt impl Comparable
        if there is a tie on species, sort those by weight.
        Comparator.comparing(Squirrel::getSpecies).thenComparingInt(Squirrel::getWeight)

    Collector<T,​A,​R> interface:
        it represents a "mutable reduction operation"
        T type of input elements
        what does it do?
            R creation of a new result container (supplier())
            A incorporating a new data element into a result container (accumulator())
            combining two result containers into one (combiner())
            performing an optional final transform on the container (finisher())

    collect:
        knows (can infer) what type to return based on given Collector (TODO: how?)

        collect​(Collector c)
            List<String> asList = stringStream.collect(Collectors.toList());

        collect​(supplier, BiConsumer accumulator, BiConsumer combiner)
            List<String> asList = stringStream.collect(ArrayList::new, ArrayList::add,

    groupingBy:
        Collector groupingBy(function);
        Collector groupingBy(function,collector);
        Collector groupingBy(function,supplier,collector);
        function=wonach, collector=value of retval-map (list,set), supplier=konkrete end-collection des retvals
        retval & supplier: ist immer eine map
        groupingBy expects all keys to be non-null

        TreeMap<Integer, Set<String>> map = ohMy.collect(
            Collectors.groupingBy( String::length,  TreeMap::new,  Collectors.toSet()));
        
    Collectors: toList, toSet, toMap, toCollection (toConcurrent*, toUnmodifiable* variants)
        minBy, maxBy
        mapping​(Function mapper, Collector downstream)   mapper changes type of things (in=objects,out=Collector)
        they all return Collector
        .joining(optional delim)  (delim,prefix,postfix) pre&post globally

    collect+groupingBy+mapping:
        accumulate the set of last names in each city:
        Map<City, Set<String>> lastNamesByCity
            = people.stream().collect(
                groupingBy(Person::getCity,
                            mapping(Person::getLastName,
                                    toSet())));

                        
Parallel streams
    Stream forEachOrdered() - process in the order they are stored in the stream
    calling parallel() on an already parallel stream is allowed
    groupingByConcurrent hat selbe args wie groupingBy
    findFirst() on parallel stream == on ordered serial stream
    parallel-stream reduction requires Collector.Characteristics  concurrent & unordered
    accumulator in a serial or parallel reduction should be associative and stateless
    .parallelStream().reduce() any thread can pick any element yet final output respects order

Commandline, Modules, Services

    -- cmdline-modules-services.txt --

Concurrency

    -- concurrency-notes.txt --

I/O APIs

    -- file-io.txt --

Secure Coding

    -- security.txt --

DB

    Driver: Establishes a connection to the database
        "jdbc:subProtocol:db-specific"
        has at least three components separated by two colons
        eg. "jdbc:postres://localhost:1234/myDb"
    Connection: Sends commands to a database
        no "new Connection()"
        Connection conn = DriverManager.getConnection(url);
            getConnection(url,properties), (url,user,passwd)
            jdbc driver classes are loaded automatically - no code necessary
        DataSource is preferred over DriverManager
            DataSource is not in the examn (?)
        .commit() or .rollback(optional safepoint)  or .setAutoCommit()
        auto-commit is enabled by default
    PreparedStatement: Executes a SQL query
        conn.preparestatement(SQL) SQL mandatory
    CallableStatement: Executes commands stored in the database
        CallableStatement.registerOutParameter() bei OUT
        retval im SQL omittable; kein set*, keine deklaration in SP nötig; ? ignoriert
    ResultSet: Reads results of a query


    a JDBC driver impl must include all these above interfaces: 
        Driver, Connection, Statement, ResultSet

    hierarchy:
        Statement (con.createStatement(sql))   <- PreparedStatement
                                                (con.prepareStatement(sql))
                                            <- CallableStatement
                                                (con.prepareCall(sql))
        - nur Statement hat executeQuery(string)
        - PreparedStatement kann BLOB, CLOB, Statement nicht
        - sqls kommen nur in statements, nicht in connection.

    int rowsAffected = ps.executeUpdate(): INSERT,UPDATE,DELETE
    resultSet = ps.executeQuery(): SELECT
    bool isResultSet = ps.execute(): for all
        getResultSet() -> select
        getUpdateCount() -> update

    rs.getString(x) always works on anything, since Object has toString()
    indexing starts with 1
    setNull() uses java.sql.Types, not JDBCType; doesn't have "STRING" but VARCHAR

Localization

    -- datetime-and-l10n.txt --

Annotations

    5: primitive, String, Class, enum, another annotation, or an array of these types
        inside an anno-declaration, another anno can be used as a type
    variables & elements implicitly public
    args/elements are named, not positional
    unnamed always needs "value()"
    unnamed und named don't mix!
    for 1 element array, you can omit {}
    "default" opts out of required
    marker-anno: no elements but can have constant variables
    target default = missing = can be used anywhere
    no = anywhere in annotations
    annotations definitions are not extendable
    null is invalid for values/arrays

    repeatable
        public @interface ContainerForA {
            A[] value();
        }
        @Repeatable(ContainerForA.class)    # refer to container
        public @interface A {
            String bla();
        }
        you can use the container (ContainerForA) and the repeatable (A) simultaneously

    common annos
        for annotations
            @Target
                TYPE 	    Classes, interfaces, enums, annotations
                TYPE_USE 	Java type declaration (like TYPE plus more) or usage (like cast, new) but only on methods w/ retval
                TYPE_PARAMETER  Parameterized types (eg List<String>), generic declarations
                ANNOTATION_TYPE
            @Documented  if anno used e.g. on a class, that class's docu will contain annotation information
            @Inherited   annotations of superclass are not inherited
            @Retention   (SOURCE,CLASS,RUNTIME), CLASS = default
        for other targets:
            @Override (for methods; compiler error)
            @FunctionalInterface   (compiler reports error when not really a fct-if)
            @Deprecated(since, forRemoval)  (for lots of targets)
            @SuppressWarnings("deprecation")  (for lots of targets)
                "unchecked" for typecasts (also for raw types?)
                    varargs with a generic type always generate "unchecked warning
                geht nicht ohne argument, nicht repeatable
                none, rawtypes, serial (serial version ID missing), and varargs
                unsupported values are ignored by compiler
                when applied to a scope (method), doesn't affect nested statements (fct calls) (q2.3395)
                    but applied to class, affects all methods
            @SafeVarargs (for methods & constructors); suppress compiler warning
                method has to have varargs, be private, static, or final

Enums

    static Enum<T> valueOf​(Class<T> enumType, String name) throws IllegalArgumentException
    T[] values() array (not in docu; added by compiler)
    .name() name of enum constant
    can have constructors (only private, package-private), fields, and methods (also abstract)
    needs ; after value list when there's more
    list of values always be declared first
    elements
        equals() and == are the same
        have .ordinal()
        are like nested classes, inheriting from the containing enum (overriding possible)
    inherit from Object
    can't extend, because Enum is final class
    can implement interfaces
    implements Comparable (for sorted collection goes by order of enum values)
        can be added to sorted collections such as SortedSet, TreeSet, and TreeMap
    .java file may contain just public enum and no public class

Optionals
    throws: get() & orElseThrow() & orElseThrow(suppl)
    orElse(other)    orElseGet(suppl)
    isPresent()      ifPresent(consumer)
    empty()         ofNullable(v) - the value is allowed to be null; empty in that case

    primitive-Optionals:
        OptionalDouble, OptionalInt, OptionalLong 
            getAsDouble etc.
            orElseGet(supplier) wants DoubleSupplier etc.
            OptionalDouble max()/min()
            double/int/long sum()
            OptionalDouble average() (also for Int/Long)


Service

    -- cmdline-modules-services.txt --

Random
    new Random(optional long seed)
    double nextDouble(); DoubleStream doubles()
    doubles(dbl origin(incl),bound(excl)); (long streamsize), (strmsize,origin,bound)
