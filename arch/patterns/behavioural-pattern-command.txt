// Define Command classes, which represent one operation/request and which they execute on a given "Receiver".
// A so called "Invoker" can be set up dynamically (meaning: at run-time) with command(s) and it executes the commands.

class ICommand {
    overwriteable execute()
} 

class CommandA : public ICommand {
    Receiver r
    execute() {
        r.doThis()
    }
}

class CommandB : public ICommand {
    Receiver r
    execute() {
        r.doThat()
    }
}

class Invoker {
    ICommand command
    execute() {
        command.execute()
    }
}

class InvokerForMultipleCommands {
    list of ICommand
    run() {
        for each command in list of ICommand {
            command.execute()
        }
    }
}

class Receiver {    // provides/implements set of things that can be done
    doThis()
    doThat()
}

main {
    Receiver r
    cmd_a = new CommandA(r)
    cmd_b = new CommandB(r)

    Invoker i;
    i.command = cmd_a
    i.execute()
    i.command = cmd_b
    i.execute()

    InvokerForMultipleCommands im;
    im."list of ICommand" append cmd_a and cmd_b
    im.execute()
}