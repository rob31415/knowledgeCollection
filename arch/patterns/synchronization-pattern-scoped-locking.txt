aka synchronized block (in Java), RAII guard, execute around object

class Guard {
   Guard(l) {acqure lock l}
   ~Guard() {release lock l}
}

class GuardedExample {
private:
    lock l
public:
    doSomething() {
        Guard(l)
        ... implementation ...
        // ~Guard() is automatically called
    }
}

Notes:
relies on a C++ language feature.
recursion leads to self-deadlock.
getting out via longjmp() doesn't call destructor.