// this is inversion of control (hollywood principle, "don't call us, we call you") without dependency injection,
// but rather done with inheritance.

class Abstract {
    overwriteable op1()
    overwriteable op2()

    template() {    // specifies order of single operations
        op2()
        op1()
    }
}

class IConcrete1 : public Abstract {    // specifies operation itself (one variant)
    op1() {... do op1 some way ...}
    op2() {... do op1 some way ...}
}

class IConcrete2 : public Abstract {    // specifies operation itself (another variant)
    op1() {... do op1 another way ...}
    op2() {... do op1 another way ...}
}

void main {
    Abstract a
    a.template()
}