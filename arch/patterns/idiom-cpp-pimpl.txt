Assume the following:
You've got a class X with a bunch of implemented functions - some public, some private - in a X.h and a X.cpp file.
The X.h is included a lot elsewhere.
Now you change the prototype of a private function in the X.h file (because you want to modify internal workings).

It follows that every compilation unit including the X.h file gets recompiled - unneccessarily.

To avoid that, you can do this:

struct X {
private:
    struct Y;   // has all private stuff from X. X keeps all it's public stuff.
    Y* pimpl;
}

pimpl is an implementation of an opaque pointer and a.k.a. d-pointer, compiler firewall, cheshire cat
