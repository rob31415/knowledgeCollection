// intent: convert interface of a class into another interface that is expected by a client

class ThingToAdapt {
    someSpecificOperation()
}


class IAdapter {    // aka "Target"
    operation()
}

class ObjectAdapter : public IAdapter {
    ThingToAdapt a;
    operation() {
        a.someSpecificOperation()       // by delegation
    }
}

class ClassAdapter : public IAdapter, public ThingToAdapt {     // by inheritance
    operation() {
        super.someSpecificOperation()
    }
}
