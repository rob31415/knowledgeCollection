class IFlyweight {
    overwriteable operation()
}

class Flyweight1 : IFlyweight {
    operation() {...}
}

class Flyweight2 : IFlyweight {
    operation() {...}
}

// factory with cache
class FlyweightFactory {
    HashMap h
    IFlyweight getFlyweight(key) {
        f = h.get(key)
        if (not f) {
            switch key {
                case "A" : f = new Flyweight1()
                case "B" : f = new Flyweight2()
            }
            h.put(f)            
        }
        return f
    }
}